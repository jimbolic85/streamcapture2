// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          downloadunpack
//          Copyright (C) 2020 - 2022 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef DOWNLOADUNPACK_H
#define DOWNLOADUNPACK_H
#include <qglobal.h>
#if !(defined Q_OS_WIN && defined QT_STATIC)
#include "downloadunpack_global.h"
#endif

#include <QMainWindow>
#include <QNetworkReply>

#include "ui_downloadunpack.h"

#ifdef Q_OS_WIN
#define CURRENT_YEAR __DATE__
#endif

#define EXECUTABLE_DISPLAY_NAME "streamCapture2"
#define EXECUTABLE_NAME "streamcapture2"
#define LIBRARY_NAME "downloadunpack"

#define RED 218
#define GREEN 255
#define BLUE 221

#ifdef Q_OS_LINUX
#define DOWNLOADUNPACK_FONT_SIZE 12
#endif
#ifdef Q_OS_WIN
#define DOWNLOADUNPACK_FONT_SIZE 11
#endif

//QT_BEGIN_NAMESPACE
namespace Ui
{
class DownloadUnpack;
}
//QT_END_NAMESPACE

#if (defined Q_OS_WIN && defined QT_STATIC)
class DownloadUnpack : public QMainWindow
#else
class DOWNLOADUNPACK_EXPORT DownloadUnpack : public QMainWindow
#endif
{
    Q_OBJECT


private:
    int stable_or_snapshot;
    Ui::DownloadUnpack *ui;
    QString selectedsvtplaydl;
    QTimer *timer;
    QString networkErrorMessages(QNetworkReply::NetworkError error);
    void startConfig();
    QString currentOs();

    void getLatest(const QString version);

    void download(bool downloadlatest, const QString &latest, const QString &path);
    void openFolder();
    void zoom();
    void pteDisplayTextChanged();
    void zoomDefault();
    void zoomMinus();
    void zoomPlus();
    QString getStablebetapath(QString stable_bleeding);
    void directoryListing(QString version);
    void downloadLatest(QString *version);
    void downloadLatest(QString *version, QString &path);




public:
    DownloadUnpack(QWidget *parent = nullptr);
    ~DownloadUnpack();
    void endConfig();
    void closeEvent(QCloseEvent *e);



private slots:



    void klocka();
    QString fileDialog();
    void takeAScreenshot();
    void setAllEnabled();


signals:


};
#endif // DOWNLOADUNPACK_H
