#!/bin/bash

# Backup of png,html,css,.htaccess (bin, lib)
HOST="`sed -n 1p ../../secret`"
USER="`sed -n 2p ../../secret`"
PASSWORD="`sed -n 3p ../../secret`"

wget --ftp-user=${USER} --ftp-password=${PASSWORD} -np -r -A png,html,css,htaccess  "ftp://${HOST}//bin.ceicer.com/public_html"
date_now=$(date "+%FT%H-%M-%S")
fil=ftpcluster.loopia.se_${date_now}_all.zip
zip -r "$fil" ftpcluster.loopia.se

rm -r -f ftpcluster.loopia.se

#wget --ftp-user=${USER} --ftp-password=${PASSWORD} -np -r -A png,html,css,htaccess  "ftp://${HOST}//bin.ceicer.com/public_html/streamcapture2/bin"
#date_now=$(date "+%FT%H-%M-%S")
##tar -czf ftpcluster.loopia.se_${date_now}_bin.tar.gz ftpcluster.loopia.se
#zip -r ftpcluster.loopia.se_${date_now}_bin.zip ftpcluster.loopia.se
#rm -r -f ftpcluster.loopia.se

#wget --ftp-user=${USER} --ftp-password=${PASSWORD} -np -r -A png,html,css,htaccess  "ftp://${HOST}//bin.ceicer.com/public_html/streamcapture2/lib"
#date_now=$(date "+%FT%H-%M-%S")
##tar -czf ftpcluster.loopia.se_${date_now}_lib.tar.gz ftpcluster.loopia.se
#zip -r ftpcluster.loopia.se_${date_now}_lib.zip ftpcluster.loopia.se
#rm -r -f ftpcluster.loopia.se



ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
cd "/bin.ceicer.com/public_html/backup"
put $fil
bye
EOF

rm $fil