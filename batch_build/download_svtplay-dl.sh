#!/bin/bash

HOST="`sed -n 1p ../../secret`"
USER="`sed -n 2p ../../secret`"
PASSWORD="`sed -n 3p ../../secret`"
echo "Download the Linux version of svtplay-dl from bin.ceicer.com"
echo "and copy it to \"build-executable5\", \"build-executable6\" and \"AppDir\"."
PS3='Please enter your choice: '
options=("Continue" "Quit")
echo ------------------------------------------------------------------------------
select opt in "${options[@]}"
do
    case $opt in
        "Continue")
echo "You chose choice $REPLY which is $opt"
wget --ftp-user=${USER} --ftp-password=${PASSWORD} -np   "ftp://${HOST}//bin.ceicer.com/public_html/svtplay-dl/Linux/version.txt"
ver=`cat version.txt`
svtplaydlver=svtplay-dl-${ver}.tar.gz
wget --ftp-user=${USER} --ftp-password=${PASSWORD} -np   "ftp://${HOST}//bin.ceicer.com/public_html/svtplay-dl/Linux/${svtplaydlver}"
tar -xf ${svtplaydlver}
cp -f svtplay-dl-${ver}/svtplay-dl ../AppDir/usr/bin/bleedingedge
cp -f svtplay-dl-${ver}/svtplay-dl ../build-executable5/bleedingedge
cp -f svtplay-dl-${ver}/svtplay-dl ../build-executable6/bleedingedge
rm version.txt
rm -r svtplay-dl-${ver}
rm ${svtplaydlver}

wget --ftp-user=${USER} --ftp-password=${PASSWORD} -np   "ftp://${HOST}//bin.ceicer.com/public_html/svtplay-dl/Linux/version_stable.txt"
ver=`cat version_stable.txt`
svtplaydlver=svtplay-dl-${ver}.tar.gz
wget --ftp-user=${USER} --ftp-password=${PASSWORD} -np   "ftp://${HOST}//bin.ceicer.com/public_html/svtplay-dl/Linux/${svtplaydlver}"
tar -xf ${svtplaydlver}
cp -f svtplay-dl-${ver}/svtplay-dl ../AppDir/usr/bin/stable
cp -f svtplay-dl-${ver}/svtplay-dl ../build-executable5/stable
cp -f svtplay-dl-${ver}/svtplay-dl ../build-executable6/stable
rm version_stable.txt
rm -r svtplay-dl-${ver}
rm ${svtplaydlver}
            break;
            ;;
        "Quit")
            exit 0
            break;
            ;;
        *) echo "invalid option $REPLY";;
    esac
done 