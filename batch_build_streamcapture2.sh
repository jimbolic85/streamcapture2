#!/bin/bash

		echo -----------------------------------------------------------
		PS3='Please enter your choice: '
		options=("Qt 5.15.2 64-bit" "Qt 5.15.7 64-bit" "Qt 6.4.1 64-bit" "Qt 6.4.1 64-bit static" "Quit")
		select opt in "${options[@]}"
do
    case $opt in
        "Qt 5.15.2 64-bit")
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/5.15.2/gcc_64/bin/qmake"
            build_executable="build-executable5"
            break;
            ;;
        "Qt 5.15.7 64-bit")
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/Qt5.15.7/bin/qmake"
            build_executable="build-executable5"
            export LD_LIBRARY_PATH=/opt/Qt/Qt5.15.7/lib:$LD_LIBRARY_PATH
            export PATH=/opt/Qt/Qt5.15.7/bin:$PATH
            break;
            ;;
        "Qt 6.4.1 64-bit")
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/6.4.1/gcc_64/bin/qmake"
            build_executable="build-executable6"
            export LD_LIBRARY_PATH=/opt/Qt/6.4.1/gcc_64/lib:$LD_LIBRARY_PATH
            export PATH=/opt/Qt/6.4.1/gcc_64/bin:$PATH
            break;
            ;;
        "Qt 6.4.1 64-bit static")
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/Qt6.4.1_static/bin/qmake"
            build_executable="build-executable6"
            export LD_LIBRARY_PATH=/opt/Qt/Qt6.4.1_static/lib:$LD_LIBRARY_PATH
            export PATH=/opt/Qt/Qt6.4.1_static/bin:$PATH
            break;
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

        echo -----------------------------------------------------------
		PS3='Please enter your choice: '
		options=("Debug" "Release" "Quit")
		select opt in "${options[@]}"
do
    case $opt in
        "Debug")
            echo "You chose choice $REPLY which is $opt"
			mkdir build
			cd build
			$qmakePath -project ../code/newprg.pro
			$qmakePath ../code/newprg.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make qmake_all
			make -j$(nproc)
			make clean -j$(nproc)
			cd ..
			rm -r build
            break;
            ;;
        "Release")
            echo "You chose choice $REPLY which is $opt"
			mkdir build
			cd build
			$qmakePath -project ../code/newprg.pro
			if [ "$bit32" > 0 ] ;
			then 
				$qmakePath ../code/newprg.pro -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
			else
				$qmakePath ../code/newprg.pro -spec linux-g++ CONFIG+=release CONFIG+=qml_release && /usr/bin/make qmake_all
			fi
			
			                          
			/usr/bin/make -j$(nproc)
			/usr/bin/make clean -j$(nproc)
			cd ..
			rm -r build
			#
						    echo -----------------------------------------------------------
							PS3='Please enter your choice: '
							options=("Copy to AppDir" "Copy to AppDir_ffmpeg" "Copy to AppDir2" "Copy to AppDir2_ffmpeg" "Quit")
							select opt in "${options[@]}"
					do
						case $opt in
							"Copy to AppDir")
								sokvag=`pwd`
								cp -f $build_executable/streamcapture2 AppDir/usr/bin/
								break;
								;;
                            "Copy to AppDir2")
								sokvag=`pwd`
								cp -f $build_executable/streamcapture2 AppDir2/usr/bin/
								break;
								;;
							"Copy to AppDir_ffmpeg")
							    sokvag=`pwd`
								cp -f $build_executable/streamcapture2 AppDir_ffmpeg/usr/bin/
								break;
								;;
							"Copy to AppDir2_ffmpeg")
							    sokvag=`pwd`
								cp -f $build_executable/streamcapture2 AppDir2_ffmpeg/usr/bin/
								break;
								;;
							"Quit")
								break
								;;
							*) echo "invalid option $REPLY";;
						esac
					done
		#
            break;
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done



# AppImage
        echo -----------------------------------------------------------
		PS3='Please enter your choice: '
		options=("Run this script again" "Build AppImage" "Do not build AppImage" "Quit")
		select opt in "${options[@]}"
do
    case $opt in
            "Run this script again")
            echo "You chose choice $REPLY which is $opt"
            ./`basename "$0"`

            break;
            ;;
        "Build AppImage")
            echo "You chose choice $REPLY which is $opt"
            ./batch_build_appimage.sh
            break;
            ;;
        "Do not build AppImage")
            echo "You chose choice $REPLY which is $opt"
            echo "Goodbye"
	        exit 0
            break;
            ;;
        "Quit")
            break
            exit 0;
            ;;
        *) echo "invalid option $REPLY";;
    esac
done


