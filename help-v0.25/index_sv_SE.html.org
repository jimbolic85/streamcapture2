<!DOCTYPE html>
<html lang="sv-SE">

<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="stilmall.css">
  <title>streamCapture2 Manual</title>

</head>

<body>
  <div id="container">
    <div class="grazie">
    <div class="language">
      <img src="images/it/italian.png" alt="Italiano">
      <a href="index_it_IT.html">In italiano, grazie!</a>
    </div>
      <span class="kursiv">Många tack till bovarius för den italienska översättningen.</span>
    </div>

    <div class="language">
      <img src="images/eng/english.png" alt="English">
      <a href="index_en_US.html">In English, thanks!</a>
    </div>
    <p>Besök gärna
      | <a href="http://ceicer.org/streamcapture2/index.php" target="_blank">Hemsida</a> |
      <a href="https://gitlab.com/posktomten/streamcapture2/wikis/home" target="_blank">Gitlab</a> |
    </p>
    <h2>streamCapture2 Version 0.25 Manual</h2>
    <p class="bold">Använd dra och släpp</p>
    <img src="images/sv/draganddrop.png" alt="Drag and drop"><br>
    <span class="kursiv">Dra från webbläsarens adressfält till streamCapture2 och släpp för att kopiera adressen.</span>
    <h3>Ladda ner en fil</h3>
    <ol>
      <li>Leta upp en webbsida som streamar video</li>
      <li>Kopiera sidans adress</li>
      <li>Klistra in i programmet</li>
      <li>Klicka på &quot;Sök&quot;</li>
      <li>Välj kvalitet</li>
      <li>Klicka på &quot;Ladda ner&quot;</li>
    </ol>
    <br>
    <h3>Ladda ner flera videoströmmar</h3>
    <ol>
      <li>Leta upp en webbsida som strömmar video</li>
      <li>Kopiera sidans adress</li>
      <li>Klistra in i programmet</li>
      <li>Klicka på &quot;Sök&quot;</li>
      <li>Välj kvalitet</li>
      <li>Klicka på &quot;Lägg till i nedladdningslistan&quot; </li>
      <li>Klicka på &quot;Ladda ner alla&quot; när du vill ladda ner </li>
    </ol>
    <br>
    <h3>Ladda ner alla avsnitt i en serie</h3>
    <ol>
      <li>Leta upp en webbsida som strömmar video</li>
      <li>Kopiera sidans adress</li>
      <li>Klistra in i programmet</li>
      <li>Klicka på &quot;Flera avsnitt&quot;, &quot;Lista alla videoströmmar&quot;</li>
      <li>Klicka på &quot;Flera avsnitt&quot;, &quot;Direktnedladdning av alla videoströmmar i nuvarande serie (inte från nedladdningslistan)&quot;</li>
      <li>Programmet väljer automatiskt videoströmmen med högst kvalitet</li>
    </ol>
    <br>
    <h3>Menyerna</h3>
    <br>
    <!-- file.png -->
    <img src="images/sv/file.png" alt="Fil">

    <ol>
      <li>Klistra in <span class="kursiv">Klistra in (url) från Urklipp.</span></li>
      <li>Sök <span class="kursiv">Leta efter videoströmmar att ladda ner.</span></li>
      <li>Ladda ner <span class="kursiv">Ladda ner en videoström med den metod och kvalitet som du valt.</span></li>
      <li>Inkludera undertexten <span class="kursiv">Bocka i om du vill att undertexten till videon du laddar ner också ska laddas ner. </span></li>
      <li>Stoppa alla nedladdningar <span class="kursiv">Stänger ner svtplay-dl. (Endast Windows.) </span></li>
      <li>Stäng <span class="kursiv">Avslutar programmet. </span></li>
    </ol>

    <br>
    <br>
    <br>
    <!-- recent.png -->
    <img src="images/sv/recent.png" alt="Senaste">
    <ol>
      <li>Om du klickar på någon av dina tidigare sökningar hamnar adressen i sökrutan.</li>
      <li>Antalet tidigare sökningar som ska sparas... <span class="kursiv">Du väljer mellan 0 och 50. Överstiger antalet sökningar ditt val försvinner den äldsta sökningen.</span></li>
      <li>Ta bort alla tidigare sökningar <span class="kursiv">Alla sparade sökningar tas bort.</span></li>
    </ol>

    <br>
    <br>
    <br>
    <!-- downloadlist.png -->
    <img src="images/sv/downloadlist.png" alt="Nedladdningslista">
    <ol>
      <li>Titta på nedladdningslistan <span class="kursiv">Visar listan med videoströmmar som du lagt till för nedladdning.</span></li>
      <li>Lägg till i nedladdningslistan <span class="kursiv">Lägger till en videoström för senare nedladdning. Du måste välja metod och kvalitet samt om du vill att undertexterna ska laddas ner innan du klickar. Listan sparas automatiskt.</span>
      </li>
      <li>Ladda ner alla <span class="kursiv">Laddar ner alla videoströmmarna du har i listan. Med de inställningar du valde när du la till videoströmmen. (Kvalitet, metod och undertext eller inte undertext.) Om samma videoström med olika kvaliteter
          laddas ner skapas mappar automatiskt.</span></li>
      <li>Redigera nedladdningslistan (avancerat) <span class="kursiv">Du kan till exempel ta bort en videoström, ändra till att ladda ner undertexter m.m. Redigera bara om du är säker på hur det fungerar. </span></li>
      <li>Spara nedladdningslistan (avancerat) <span class="kursiv">Sparar dina ändringar i nedladdningslistan.</span></li>
      <li>Raddera nedladdningslistan <span class="kursiv">Raderar hela nedladdningslistan.</span></li>
    </ol>

    <br>
    <br>
    <br>
    <!-- downloadlist_example.png -->
    <img src="images/sv/downloadlist_example.png" alt="Download list example">
    <br>
    <span class="kursiv">Nedladdningslistan är en kommaseparerad lista på egenskaper, som används vid nedladdning av videoströmmarna som sparats i nedladdningslistan.</span>

    <ol>
      <li>Adress <span class="kursiv">https://www.svtplay.se/video/30215264/sheriffen,5519,DASH,nopassword,nsub</span></li>
      <li>Kvalitet (bithastighet) <span class="kursiv">5519</span></li>
      <li>Metod <span class="kursiv">DASH</span></li>
      <li>Lösenord (namnet på leverantören av videoströmmen om lösenord används, "nopassword" om lösenord inte används) <span class="kursiv">nopassword</span></li>
      <li>Ladda ner undertexten eller ladda inte ner undertexten <span class="kursiv">ysub eller nsub</span></li>
    </ol>


    <br>
    <br>
    <br>
    <!-- language.png -->
    <img src="images/sv/language.png" alt="Språk">
    <ol>
      <li>Välj gränssnittets språk</li>
      <li>Ladda extern språkfil... <span class="kursiv">Kolla <a class="kursiv" href="https://gitlab.com/posktomten/streamcapture2/-/wikis/Translate" target="_blank">instruktionerna</a> hur du översätter streamCapture2.</span></li>
    </ol>
    <br>
    <br>
    <br>
    <!-- tools.png -->
    <img src="images/sv/tools.png" alt="Verktyg">
    <ol>
      <li>Sök efter uppdateringar när programmet startar <span class="kursiv">Om du har bockat i kryssrutan kollar programmet vid varje start om det kommit en nyare version och meddelar dig detta. Kräver internetanslutning. </span></li>
      <li>Skapa folder &quot;metod_kvalitet&quot; <span class="kursiv">Om du har bockat i kryssrutan skapas en folder som har namn efter filnamn och undermappar valda efter metod och kvalitet. Till exempel
          &quot;bolibompa-baby-visor-sasong-6-sma-grodorna/dash_3039&quot;. Detta sker automatiskt om du laddar ner samma video i olika kvaliteer. </span></li>
      <li>Visa mer <span class="kursiv">Du får mer information från svtplay-dl om vad som händer. </span></li>
      <li>Välj standard nedladdningsplats... <span class="kursiv">Du kan välja en standardplats dit videoströmmarna alltid laddas ner. </span></li>
      <li>Ladda ner till standardplatsen <span class="kursiv">Nedladdningen startar direkt och videoströmmarna sparas i din standardplats. </span></li>
      <li>Välj kopieringsplats... <span class="kursiv">Den kompletta mediafilen kan kopieras automatiskt till din valda folder. Efter att alla filer, ljud, eventuell undertext och video har laddats ner och sammanfogats kopieras den färdiga
          mp4-filen.</span></li>
      <li>Kopiera <span class="kursiv">Kopiera automatiskt till din förvalda folder. </span></li>
      <li>Använd svtplay-dl från systemsökvägen <span class="kursiv">Använder svtplay-dl som hittas i ditt systems sökväg. </span></li>
      <li>Använd senaste stabila svtplay-dl <span class="kursiv">Använd senaste stabila versionen av svtplay-dl. </span></li>
      <li>Använd senaste svtplay-dl snapshot <span class="kursiv">Använd senaste snapshot av svtplay-dl. </span></li>
      <li>Välj svtplay-dl... <span class="kursiv">Leta upp svtplay-dl i ditt filsystem. </span></li>
      <li>Använd vald svtplay-dl <span class="kursiv">Använd svtplay-dl som du valt i ditt filsystem. </span></li>
      <li>Visa inte aviseringar <span class="kursiv">Du kan få ett meddelande på skärmen när videoströmmen eller videoströmmarna är nedladdade. </span></li>
      <li>Skrivbordsgenväg <span class="kursiv"> Skapar en genväg på skrivbordet till streamCapture2. </span></li>
      <li>Genväg i programmenyn <span class="kursiv">Skapar genväg på startmenyn till streamCapture2. Saknas om du använder den installerade Windows-versionen. Om du installerat skapar installationsprogrammet en genväg.</span></li>
      <li>Välj teckensnitt... <span class="kursiv">Välj teckensnitt, teckenstorlek och typ. </span></li>
      <li>
           <ul>
             <li> Underhållsverktyg...<span class="kursiv"> (Windows) Lägg till eller ta bort komponenter, uppdatera komponenter eller ta bort alla komponenter (Avinstallera). Gäller endast om du har installerat streamCapture2.</span></li>
             <li> Uppdatera...<span class="kursiv"> (Linux) Uppdatera Linux AppImage.</span></li>
           </ul>
      </li>
      <li>Ta bort alla inställningar och avsluta <span class="kursiv">Ta bort alla konfigurationsfiler, sparade sökningar och nedladdningslistan. </span></li>
    </ol>

    <br>
    <br>
    <br>

    <img src="images/sv/maintenancetool.png" alt="Underhållsverktyg">
    <br>
    <span class="kursiv">Underhållsverktyg</span>
    <br>
    <br>
    <br>
    <br>
    <!-- paytv.png -->
    <img src="images/sv/paytv.png" alt="Betal-TV">
    <ol>
      <li>Lösenord <span class="kursiv">Ange ett lösenord för att ladda ner från en streamingtjänst som kräver inloggning.</span></li>
      <li>Skapa ny <span class="kursiv">Du kan lägga till en streamingtjänst som kräver inloggning. Ange leverantörens namn och ditt användarnamn. Du kan låta programmet spara ditt lösenord eller välja att ange det varje gång du laddar ner.</span></li>
      <li>Klicka på en befintlig streamingtjänst <span class="kursiv">Du väljer om du vill ta bort streamingtjänsten eller redigera användarnamn och lösenord. </span></li>
    </ol>

    <br>
    <br>
    <br>
    <!-- paytv.png -->
    <img src="images/sv/severalepisodes.png" alt="Flera avsnitt">
    <ol>
      <li>Lista alla videoströmmar <span class="kursiv">Listar alla videoströmmar som hör till samma TV-serie.</span>
      <li>Lägg till alla videoströmmar i nedladdningslistan <span class="kursiv">Lägg till alla videoströmmar som hör till aktuell TV-serie i nedladdningslistan.</span></li>
      <li>Direktnedladdning av alla strömmar i aktuell serie (inte från nedladdningslistan) <span class="kursiv">Försöker hitta och ladda ner alla videoströmmar som hör till aktuell TV-serie utan att lägga till dem på nedladdningslistan. </span></li>
    </ol>

    <br>
    <br>
    <br>
    <!-- help.png -->
    <img src="images/sv/help.png" alt="Hjälp">
    <ol>
      <li>Hjälp... <span class="kursiv">Visar den här hjälpen.</span></li>
      <li>Sök efter uppdateringar... <span class="kursiv">Kollar om det kommit någon ny version av programmet och meddelar om så är fallet. Kräver internetuppkoppling.</span></li>
      <li>Om... <span class="kursiv">Teknisk information och information om version.</span></li>
      <li>Om svtplay-dl... <span class="kursiv">Information om version och konfigurationsalternativ för svtplay-dl. svtplay-dl väljer du i menyn &quot;Verktyg&quot;. Har du inte gjort något val är &quot;svtplay-dl Senaste stabila versionen&quot;
          förvalt.</span></li>
      <li>About FFmpeg... <span class="kursiv">Version and configuration options for FFmpeg.</span></li>
      <li>Besök svtplay-dl forum... <span class="kursiv">Länk till svtplay-dl forum. Öppnas i din webbläsare.</span></li>
      <li>Licens streamCapture2... <span class="kursiv">Visar licensinformation för streamCapture2.</span></li>
      <li>Licens svtplay-dl... <span class="kursiv">Visar licensinformation för svtplay-dl.</span></li>
      <li>Licens FFmpeg... <span class="kursiv">Visar licensinformation för FFmpeg.</span></li>

    </ol>

    <br>

    <br>
    <!-- uninstall.png -->
    <img src="images/sv/listboxkvalitet.png" alt="Listbox kvalité">
    <ol>
      <li>Välj kvalitet <span class="kursiv">Klicka på listboxen för att välja kvalitet. Här är &quot;2877 DASH&quot; valt. Högre bitrate ger bättre kvalitet och en större fil att ladda ner. </span></li>
      <li>Lösenord <span class="kursiv">Klicka på listboxen för att välja &quot;- Ingen lösenord&quot; eller välj en streamingtjänst som kräver lösenord. Har du låtit programmet spara lösenorden kan du ha flera streamingtjänster i din
          nedladdningslista. Programmet väljer automatiskt rätt lösenord. </span></li>
    </ol>

<div class="ruta">
    <p class="bold">Om FFmpeg</p>
    <p>streamCapture2 använder i första hand FFmpeg som medföljer streamCapture2. Saknas FFmpeg letar streamCapture2 i systemsökvägen efter FFmpeg. Du kan alltså använda FFmpeg som du tidigare installerat.</p>
</div>

<br>
  </div>
  <!-- container -->
</body>

</html>
