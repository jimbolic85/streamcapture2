file "bin\7za.exe"
file "bin\all_files.bat"
file "bin\checkupdate.dll"
file "bin\D3Dcompiler_47.dll"
file "bin\downloadunpack.dll"
file "bin\ffmpeg.exe"
file "bin\libgcc_s_seh-1.dll"
file "bin\libstdc++-6.dll"
file "bin\libwinpthread-1.dll"
file "bin\opengl32sw.dll"
file "bin\Qt6Core.dll"
file "bin\Qt6Gui.dll"
file "bin\Qt6Network.dll"
file "bin\Qt6Svg.dll"
file "bin\Qt6Widgets.dll"
file "bin\selectfont.dll"
file "bin\streamcapture2.exe"

file "bin\iconengines\qsvgicon.dll"
file "bin\imageformats\qgif.dll"
file "bin\imageformats\qicns.dll"
file "bin\imageformats\qico.dll"
file "bin\imageformats\qjpeg.dll"
file "bin\imageformats\qsvg.dll"
file "bin\imageformats\qtga.dll"
file "bin\imageformats\qtiff.dll"
file "bin\imageformats\qwbmp.dll"
file "bin\imageformats\qwebp.dll"

file "bin\platforms\qwindows.dll"

file "bin\styles\qwindowsvistastyle.dll"

file "bin\tls\qcertonlybackend.dll"
file "bin\tls\qopensslbackend.dll"
file "bin\tls\qschannelbackend.dll"

file "Licenses\license_7zip.txt"
file "Licenses\license_ffmpeg.txt"
file "Licenses\license_qt.txt"
file "Licenses\license_streamCapture2.txt"
file "Licenses\license_svtplay-dl.txt"
