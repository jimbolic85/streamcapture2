; Turn off old selected section
; 12 06 2005: Luis Wong
; Template to generate an installer
; Especially for the generation of EasyPlayer installers
; Trimedia Interactive Projects
 
; -------------------------------
; Start
 
  !define MUI_PRODUCT "SIG Beta Ver. 1.0"
  !define MUI_FILE "savefile"
  !define MUI_VERSION ""
  !define MUI_BRANDINGTEXT "SIG Beta Ver. 1.0"
  CRCCheck On
 
  ; We should test if we must use an absolute path 
  !include "${NSISDIR}\Contrib\Modern UI\System.nsh"
 
 
;---------------------------------
;General
 
  OutFile "installsig.exe"
  ShowInstDetails "nevershow"
  ShowUninstDetails "nevershow"
  ;SetCompressor "bzip2"
 
  !define MUI_ICON "icon.ico"
  !define MUI_UNICON "icon.ico"
 ; !define MUI_SPECIALBITMAP "Bitmap.bmp"
 
 
;--------------------------------
;Folder selection page
 
  InstallDir "$PROGRAMFILES\${MUI_PRODUCT}"
 
 
;--------------------------------
;Modern UI Configuration
 
  !define MUI_WELCOMEPAGE  
  !define MUI_LICENSEPAGE
  !define MUI_DIRECTORYPAGE
  !define MUI_ABORTWARNING
  !define MUI_UNINSTALLER
  !define MUI_UNCONFIRMPAGE
  !define MUI_FINISHPAGE  
 
 
;--------------------------------
;Language
 
  !insertmacro MUI_LANGUAGE "English"
 
 
;-------------------------------- 
;Modern UI System
 
  !insertmacro MUI_UI 
 
 
;--------------------------------
;Data
 
  LicenseData "Licenses\license_streamCapture2.txt"
 
 
;-------------------------------- 
;Installer Sections     
Section "install" Installation info
 
;Add files
  SetOutPath "$INSTDIR"
   File "icon.ico"
  ;File "${MUI_FILE}.exe"
  ;File "${MUI_FILE}.ini"
  ;File "Read_me.txt"
  ;SetOutPath "$INSTDIR\playlists"
  ;file "playlists\${MUI_FILE}.epp"
  ;SetOutPath "$INSTDIR\data"
  ;file "data\*.cst"
  ;file "data\errorlog.txt"
  ; Here follow the files that will be in the playlist
  ;SetOutPath "$INSTDIR"  
  ;file /r mpg
  ;SetOutPath "$INSTDIR"  
  ;file /r xtras  
SetOutPath "$INSTDIR\bin"
file "bin\7za.exe"
file "bin\all_files.bat"
file "bin\checkupdate.dll"
file "bin\D3Dcompiler_47.dll"
file "bin\downloadunpack.dll"
file "bin\ffmpeg.exe"
file "bin\libgcc_s_seh-1.dll"
file "bin\libstdc++-6.dll"
file "bin\libwinpthread-1.dll"
file "bin\opengl32sw.dll"
file "bin\Qt6Core.dll"
file "bin\Qt6Gui.dll"
file "bin\Qt6Network.dll"
file "bin\Qt6Svg.dll"
file "bin\Qt6Widgets.dll"
file "bin\selectfont.dll"
file "bin\streamcapture2.exe"
SetOutPath "$INSTDIR\bin\iconengines"
file "bin\iconengines\qsvgicon.dll"
SetOutPath "$INSTDIR\bin\imageformats"
file "bin\imageformats\qgif.dll"
file "bin\imageformats\qicns.dll"
file "bin\imageformats\qico.dll"
file "bin\imageformats\qjpeg.dll"
file "bin\imageformats\qsvg.dll"
file "bin\imageformats\qtga.dll"
file "bin\imageformats\qtiff.dll"
file "bin\imageformats\qwbmp.dll"
file "bin\imageformats\qwebp.dll"
SetOutPath "$INSTDIR\bin\platforms"
file "bin\platforms\qwindows.dll"
SetOutPath "$INSTDIR\bin\styles"
file "bin\styles\qwindowsvistastyle.dll"
SetOutPath "$INSTDIR\bin\tls"
file "bin\tls\qcertonlybackend.dll"
file "bin\tls\qopensslbackend.dll"
file "bin\tls\qschannelbackend.dll"
SetOutPath "$INSTDIR\Licenses"
file "Licenses\license_7zip.txt"
file "Licenses\license_ffmpeg.txt"
file "Licenses\license_qt.txt"
file "Licenses\license_streamCapture2.txt"
file "Licenses\license_svtplay-dl.txt"

 
;create desktop shortcut
  CreateShortCut "$DESKTOP\${MUI_PRODUCT}.lnk" "$INSTDIR\${MUI_FILE}.exe" ""
 
;create start-menu items
  CreateDirectory "$SMPROGRAMS\${MUI_PRODUCT}"
  CreateShortCut "$SMPROGRAMS\${MUI_PRODUCT}\Uninstall.lnk" "$INSTDIR\Uninstall.exe" "" "$INSTDIR\Uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\${MUI_PRODUCT}\${MUI_PRODUCT}.lnk" "$INSTDIR\${MUI_FILE}.exe" "" "$INSTDIR\${MUI_FILE}.exe" 0
 
;write uninstall information to the registry
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "DisplayName" "${MUI_PRODUCT} (remove only)"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "UninstallString" "$INSTDIR\Uninstall.exe"
 
  WriteUninstaller "$INSTDIR\Uninstall.exe"
 
SectionEnd
 
 
;--------------------------------    
;Uninstaller Section  
Section "Uninstall"
 
;Delete Files 
  RMDir /r "$INSTDIR\*.*"    
 
;Remove the installation directory
  RMDir "$INSTDIR"
 
;Delete Start Menu Shortcuts
  Delete "$DESKTOP\${MUI_PRODUCT}.lnk"
  Delete "$SMPROGRAMS\${MUI_PRODUCT}\*.*"
  RmDir  "$SMPROGRAMS\${MUI_PRODUCT}"
 
;Delete Uninstaller And Unistall Registry Entries
  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\${MUI_PRODUCT}"
  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}"  
 
SectionEnd
 
 
;--------------------------------    
;MessageBox Section
 
 
;Function that calls a messagebox when installation finished correctly
Function .onInstSuccess
  MessageBox MB_OK "You have successfully installed ${MUI_PRODUCT}. Use the desktop icon to start the program."
FunctionEnd
 
Function un.onUninstSuccess
  MessageBox MB_OK "You have successfully uninstalled ${MUI_PRODUCT}."
FunctionEnd
 
 
;eof