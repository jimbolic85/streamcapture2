<?php
$tr = array(
"Download latest offline installer" => "Download installer offline aggiornato",
"Latest offline installer, please download!" => "Installer offline aggiornato - scaricalo!",
"Read" => "Leggi",
"Size" => "Dimensione");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 <link rel="stylesheet" href="../../../.stilmall.css">
<title><?php echo $tr['Download latest offline installer'] ?></title>
<style>
body {
	display:flex;
	justify-content:center;
}
.center {
	display-flex;
	margin:auto;
	margin-top:12px;
	padding:20px;
	border-style:solid;
	border-width:2px;
	background-color:white;
}
</style>
</head>
<body>
<div class="center">
<h2><?php echo $tr['Latest offline installer, please download!'] ?></h2>
<?php
foreach (glob("streamCapture2-*.*.*_installer.exe") as $filename) {
    echo "<a href=\"https://bin.ceicer.com/streamcapture2/bin/" . $filename . "\"><span class=\"fet\">" . $filename  . "</span></a><span class=\"fet\">" . $tr['Size'] . " " . round(filesize($filename)/1048576,2) . " MB<br><br>MD5: " . md5_file($filename) . "</span><br>";
}
?>
<p class="fet"><?php echo $tr['Read'] ?> <a href="https://gitlab.com/posktomten/streamcapture2/-/blob/Qt6/CHANGELOG" target="_blank">CANGELOG</a></p>
</div>
</body>
</html>
