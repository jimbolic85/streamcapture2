; Language ID
; Italian (Italy) 1040 - Italian (Switzerland) 2064

LangString UNINST_EXCLUDE_ERROR 1040 "Errore durante la creazione di un elenco di esclusione."
LangString UNINST_DATA_ERROR 1040 "Errore durante la creazione dei dati del programma di disinstallazione: $\r$\nImpossibile trovare un elenco di esclusione."
LangString UNINST_DAT_NOT_FOUND 1040 "$UNINST_DAT non trovato.$\r$\nImpossibile eseguire la disinstallazione.$\r$\nElimina manualmente i file."
LangString UNINST_DAT_MISSING 1040 "$UNINST_DAT non trovato.$\r$\nNon è stato possibile rimuovere alcuni elementi.$\r$\nQuesti elementi possono essere rimossi manualmente."
LangString UNINST_DEL_FILE 1040 "Elimina file"
LangString Uninstall 1040 "Disinstalla"
LangString NextCD 1040 "Inserisci il disco etichettato"
LangString CDNotFound 1040 "Impossibile trovare il file $0.$\r$\nInserisci il disco corretto."
LangString Extract 1040 "Decompressione:"
LangString REMOVEALL 1040 "Vuoi rimuovere tutti i file e le cartelle da '$INSTDIR'?"
LangString UNCOMPLATE 1040 "Disinstallazione di $(^Name) completata."

LangString DESC_Sec1 1040 "Installa streamCapture2 e tutti i file libreria, nonché 7za.exe.$\r$\nstreamCapture2 è un programma per salvare video in streaming sul tuo computer."
LangString DESC_Sec2 1040 "FFmpeg è un progetto software gratuito e open source costituito da una suite di librerie e programmi per la gestione di file e flussi video, audio e altri multimediali."
LangString DESC_Sec3 1040 "Qt è un software multi piattaforma per la creazione di interfacce utente grafiche e applicazioni."

LangString INST_TYPE_FULL 1040 "Completa"
LangString INST_TYPE_MINIMAL 1040 "Minima"
LangString INST_TYPE_CUSTOM 1040 "Personalizzata"

LangString WELCOME_TITLE 1040 "Installazione di$\r$\nstreamCapture2 2.8.1"