#!/bin/bash

echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Qt 5.15.2 64-bit" "Qt 5.15.7" "Qt 6.4.1 64-bit" "Qt 6.4.1 64-bit static" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Qt 5.15.2 64-bit")
            LD_LIBRARY_PATH="/opt/Qt/5.15.2/gcc_64/lib${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}"
            PATH="/opt/Qt/5.15.2/gcc_64/bin${PATH:+:${PATH}}"
            APPDIR="AppDir"
            APPDIR_FFMPEG="AppDir_ffmpeg"
            echo "You chose $REPLY which is $opt"
            qmakeexecutable="/opt/Qt/5.15.2/gcc_64/bin/qmake"
            HIGHGLIBC=0
            break;
            ;;
        "Qt 5.15.7")
            LD_LIBRARY_PATH="/opt/Qt/Qt5.15.7/lib${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}"
            PATH="/opt/Qt/Qt5.15.7/bin${PATH:+:${PATH}}"
            APPDIR="AppDir"
            APPDIR_FFMPEG="AppDir_ffmpeg"
            echo "You chose $REPLY which is $opt"
            qmakeexecutable="/opt/Qt/Qt5.15.7/bin/qmake"
            HIGHGLIBC=0
            unsuported_ffmpeg=""
            unsuported=""
            HIGHGLIBC=0
            break;
            ;;
        "Qt 6.4.1 64-bit")
            LD_LIBRARY_PATH="/opt/Qt/6.4.1/gcc_64/lib${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}"
            PATH="/opt/Qt/6.4.1/gcc_64/bin${PATH:+:${PATH}}"
            APPDIR="AppDir"
            APPDIR_FFMPEG="AppDir_ffmpeg"
            echo "You chose $REPLY which is $opt"
            qmakeexecutable="/opt/Qt/6.4.1/gcc_64/bin/qmake"-no-copy-copyright-files
            unsuported="-extra-plugins=${APPDIR}/usr/plugins/platforms/libqxcb.so,${APPDIR}/usr/plugins/tls/libqopensslbackend.so -executable=${APPDIR}/usr/bin/zsync -unsupported-allow-new-glibc"
            unsuported_ffmpeg="-extra-plugins=${APPDIR_FFMPEG}/usr/plugins/platforms/libqxcb.so,${APPDIR_FFMPEG}/usr/plugins/tls/libqopensslbackend.so -executable=${APPDIR_FFMPEG}/usr/bin/zsync -unsupported-allow-new-glibc"            
            HIGHGLIBC=1
            break;
            ;;
        "Qt 6.4.1 64-bit static")
            LD_LIBRARY_PATH="/opt/Qt/Qt6.4.1_static/lib${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}"
            PATH="/opt/Qt/Qt6.4.1_static/bin${PATH:+:${PATH}}"
            APPDIR="AppDir"
            APPDIR_FFMPEG="AppDir_ffmpeg"
            echo "You chose $REPLY which is $opt"
            qmakeexecutable="/opt/Qt/Qt6.4.1_static/bin/qmake"-no-copy-copyright-files
            unsuported="-extra-plugins=${APPDIR}/usr/plugins/platforms/libqxcb.so,${APPDIR}/usr/plugins/tls/libqopensslbackend.so -executable=${APPDIR}/usr/bin/zsync -unsupported-allow-new-glibc"
            unsuported_ffmpeg="-extra-plugins=${APPDIR_FFMPEG}/usr/plugins/platforms/libqxcb.so,${APPDIR_FFMPEG}/usr/plugins/tls/libqopensslbackend.so -executable=${APPDIR_FFMPEG}/usr/bin/zsync -unsupported-allow-new-glibc"            
            HIGHGLIBC=10
            break;
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Sign AppImage" "Do not sign AppImage" "Quit")
select opt in "${options[@]}"
do


	case $opt in
		"Sign AppImage")
			echo "You chose $REPLY which is $opt"
			signera=1
			break;
			;;
		"Do not sign AppImage")
			echo "You chose $REPLY which is $opt"
			signera=0  
			break;
			;;
		"Quit")
			break
			;;
		*) echo "invalid option $REPLY";;
	esac
done
echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("AppImage" "AppImage ffmpeg" "AppImage and AppImage ffmpeg" "Quit")
select opt in "${options[@]}"
do

    case $opt in
        "AppImage")
            echo "You chose $REPLY which is $opt"
            linuxdeployqt ${APPDIR}/usr/bin/streamcapture2 -qmake=$qmakeexecutable -bundle-non-qt-libs -verbose=2 -no-translations -no-copy-copyright-files -always-overwrite -executable=${APPDIR}/usr/bin/zsync $unsuported
            
            if [ "$signera" -gt 0 ] ;
            then
				appimagetool --sign ${APPDIR}
			else
				appimagetool ${APPDIR}
			fi
           
            break;
            ;;
        "AppImage ffmpeg")
            echo "You chose $REPLY which is $opt"
            linuxdeployqt ${APPDIR_FFMPEG}/usr/bin/streamcapture2 -qmake=$qmakeexecutable -bundle-non-qt-libs -verbose=2 -no-translations -no-copy-copyright-files -always-overwrite -executable=${APPDIR_FFMPEG}/usr/bin/zsync $unsuported_ffmpeg
            
            if [ "$signera" -gt 0 ] ;
            then
				appimagetool --sign ${APPDIR_FFMPEG}
			else
				appimagetool ${APPDIR_FFMPEG}
			fi
            
            break;
            ;;
        "AppImage and AppImage ffmpeg")
            echo "You chose $REPLY which is $opt"
            linuxdeployqt ${APPDIR}/usr/bin/streamcapture2 -qmake=$qmakeexecutable -bundle-non-qt-libs -verbose=2 -no-translations -no-copy-copyright-files -always-overwrite -executable=${APPDIR}/usr/bin/zsync $unsuported
            
            linuxdeployqt ${APPDIR_FFMPEG}/usr/bin/streamcapture2 -qmake=$qmakeexecutable -bundle-non-qt-libs -verbose=2 -no-translations -no-copy-copyright-files -always-overwrite -executable=${APPDIR_FFMPEG}/usr/bin/zsync $unsuported_ffmpeg

            if [ "$signera" -gt 0 ] ;
            then
                appimagetool --sign ${APPDIR}
				appimagetool --sign ${APPDIR_FFMPEG}
			else
			    appimagetool ${APPDIR}
				appimagetool ${APPDIR_FFMPEG}
			fi
            break;
            ;;
            
        "Quit")
        echo "Goodbye"
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Upload AppImage" "Upload AppImage later" "Quit")
select opt in "${options[@]}"
do


case $opt in
    "Upload AppImage")
        echo "You chose $REPLY which is $opt"

if [ "$HIGHGLIBC" -eq 10 ] ;
 then
   cp -f *.AppImage streamcapture2-zsyncmake/
else 
   cp -f *.AppImage streamcapture2-zsyncmake/
fi

if [ "$HIGHGLIBC" -eq 1 ] ;
 then
   
	ZSYNC_ADDRESS="http://bin.ceicer.com/zsync/HIGH_GLIBC"
	ZSYNC_DIR="/bin.ceicer.com/public_html/zsync/HIGH_GLIBC"
	BIN_DIR="/bin.ceicer.com/public_html/streamcapture2/bin/HIGH_GLIBC"
	
   
 elif [ "$HIGHGLIBC" -eq 0 ] ;
  then
   
   	ZSYNC_ADDRESS="http://bin.ceicer.com/zsync"
   	ZSYNC_DIR="/bin.ceicer.com/public_html/zsync"
	BIN_DIR="/bin.ceicer.com/public_html/streamcapture2/bin"

 
 else 
    # static
   	ZSYNC_ADDRESS="http://bin.ceicer.com/zsync/HIGH_GLIBC/static"
	ZSYNC_DIR="/bin.ceicer.com/public_html/zsync/HIGH_GLIBC/static"
	BIN_DIR="/bin.ceicer.com/public_html/streamcapture2/bin/HIGH_GLIBC/static"
  
fi


if [ "$HIGHGLIBC" -eq 10 ] ;
 then
    cd streamcapture2-zsyncmake
else 
    cd streamcapture2-zsyncmake
fi


date_now=$(date "+%FT%H-%M-%S")

for i in *.AppImage; do # Whitespace-safe but not recursive.

   
    ./zsyncmake "$i" -u "${ZSYNC_ADDRESS}/$i" 
    echo "--------------------------------------------------" > "$i-MD5.txt"
	echo "MD5 HASH SUM" >> "$i-MD5.txt"
	md5sum "$i" >> "$i-MD5.txt"
	filesize=$(stat -c%s "$i")
	mb=$(bc <<< "scale=6; $filesize / 1048576")
	mb2=$(echo $mb | awk '{printf "%f", $1 + $2}')
	echo "Uploaded: $date_now" >> "$i-MD5.txt"
	echo "Size $mb MB." >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "HOW TO CHECK THE HASH SUM" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "LINUX" >> "$i-MD5.txt"
	echo "\"md5sum\" is included in most Linus distributions." >> "$i-MD5.txt"
	echo "md5sum $i" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "WINDOWS" >> "$i-MD5.txt"
	echo "\"certutil\" is included in Windows." >> "$i-MD5.txt"
	echo "certutil -hashfile $i md5" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "READ MORE" >> "$i-MD5.txt"
	echo "https://gitlab.com/posktomten" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
    
    
done



HOST="`sed -n 1p ../../secret`"
USER="`sed -n 2p ../../secret`"
PASSWORD="`sed -n 3p ../../secret`"

echo "$BIN_DIR"

echo "$ZSYNC_DIR"



for fil in *.AppImage; do # Whitespace-safe but not recursive.

ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
passive
cd "$BIN_DIR"
put $fil
cd "$ZSYNC_DIR"
put $fil
bye
EOF

done



for fil in *.zsync; do # Whitespace-safe but not recursive.

ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
passive
cd "$ZSYNC_DIR"
put $fil
bye
EOF

done

for fil in *-MD5.txt; do # Whitespace-safe but not recursive.

ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
passive
cd "$BIN_DIR"
put $fil
bye
EOF

done
break
;;
			
  "Upload AppImage later")
		##
echo "You chose $REPLY which is $opt"

if [ "$HIGHGLIBC" -eq 10 ] ;
 then
   cp -f *.AppImage streamcapture2-zsyncmake/
else 
   cp -f *.AppImage streamcapture2-zsyncmake/
fi

#HIGHGLIBC=0

if [ "$HIGHGLIBC" -eq 1 ] ;
 then
   
	ZSYNC_ADDRESS="http://bin.ceicer.com/zsync/HIGH_GLIBC"
	ZSYNC_DIR="/bin.ceicer.com/public_html/zsync/HIGH_GLIBC"
	BIN_DIR="/bin.ceicer.com/public_html/streamcapture2/bin/HIGH_GLIBC"
	
   
 elif [ "$HIGHGLIBC" -eq 0 ] ;
  then
   
   	ZSYNC_ADDRESS="http://bin.ceicer.com/zsync"
   	ZSYNC_DIR="/bin.ceicer.com/public_html/zsync"
	BIN_DIR="/bin.ceicer.com/public_html/streamcapture2/bin"

 
 else 
                          
    # static
   	ZSYNC_ADDRESS="http://bin.ceicer.com/zsync/HIGH_GLIBC/static"
	ZSYNC_DIR="/bin.ceicer.com/public_html/zsync/HIGH_GLIBC/static"
	BIN_DIR="/bin.ceicer.com/public_html/streamcapture2/bin/HIGH_GLIBC/static"
  
fi

if [ "$HIGHGLIBC" -eq 10 ] ;
 then
    cd streamcapture2-zsyncmake
else 
    cd streamcapture2-zsyncmake
fi


date_now=$(date "+%FT%H-%M-%S")

for i in *.AppImage; do # Whitespace-safe but not recursive.

    ./zsyncmake "$i" -u "${ZSYNC_ADDRESS}/$i" 
    
    
    echo "--------------------------------------------------" > "$i-MD5.txt"
	echo "MD5 HASH SUM" >> "$i-MD5.txt"
	md5sum "$i" >> "$i-MD5.txt"
	filesize=$(stat -c%s "$i")
	mb=$(bc <<< "scale=6; $filesize / 1048576")
	mb2=$(echo $mb | awk '{printf "%f", $1 + $2}')
	echo "Uploaded: $date_now" >> "$i-MD5.txt"
	echo "Size $mb MB." >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "HOW TO CHECK THE HASH SUM" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "LINUX" >> "$i-MD5.txt"
	echo "\"md5sum\" is included in most Linus distributions." >> "$i-MD5.txt"
	echo "md5sum $i" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "WINDOWS" >> "$i-MD5.txt"
	echo "\"certutil\" is included in Windows." >> "$i-MD5.txt"
	echo "certutil -hashfile $i md5" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "READ MORE" >> "$i-MD5.txt"
	echo "https://gitlab.com/posktomten" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
    
    
done
		##
			break
			;;

		"Quit")
		echo "Goodbye"
			break
			;;
		*) echo "invalid option $REPLY";;
	esac
done



