#!/bin/bash

# Backup of streamCapture lib 32-bit
HOST="`sed -n 1p ../secret`"
USER="`sed -n 2p ../secret`"
PASSWORD="`sed -n 3p ../secret`"

date_now=$(date "+%FT%H-%M-%S")
fil=ftpcluster.loopia.se_${date_now}_all.zip
zip -r "$fil" ftpcluster.loopia.se
fil=lib_Qt5_32_MinGW-8.1.0_${date_now}_all.zip
rm -r -f ftpcluster.loopia.se

ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
passive
cd "/bin.ceicer.com/public_html/streamcapture2/lib"
put $fil
bye
EOF

rm $fil