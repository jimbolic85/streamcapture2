ECHO OFF
CLS
:MENU
ECHO.
ECHO   PRESS 1 OR 2 to select your task, or 3 to EXIT.
ECHO.
ECHO   1 - Build Qt5.15.4
ECHO   2 - Build Qt5.15.2
ECHO   3 - EXIT
ECHO.
SET /P M=Type 1, 2 or 3 then press ENTER: 
IF %M%==1 GOTO LATEST
IF %M%==2 GOTO OFFICIAL
IF %M%==3 GOTO EOF
:LATEST
del /S /Q lib5\*.*
xcopy lib_Qt5.15.4\*.* lib5\

mkdir build
cd build
    C:\Qt\Qt5.15.4_mingw_shared\bin\qmake.exe -project ../code/newprg.pro
	C:\Qt\Qt5.15.4_mingw_shared\bin\qmake.exe ../code/newprg.pro -spec win32-g++ "CONFIG+=qtquickcompiler" && C:\Qt\Qt\Tools\mingw810_32\bin\mingw32-make.exe qmake_all
  
C:\Qt\Qt\Tools\mingw810_32\bin\mingw32-make.exe -j4

cd ..
rmdir /S /Q build



GOTO EOF
:OFFICIAL
del /S /Q lib5\*.*
xcopy lib_Qt5.15.2\*.* lib5\

mkdir build
cd build
    C:\Qt\Qt\5.15.2\mingw81_32\bin\qmake.exe -project ../code/newprg.pro
	C:\Qt\Qt\5.15.2\mingw81_32\bin\qmake.exe ../code/newprg.pro -spec win32-g++ "CONFIG+=qtquickcompiler" && C:\Qt\Qt\Tools\mingw810_32\bin\mingw32-make.exe qmake_all
  
C:\Qt\Qt\Tools\mingw810_32\bin\mingw32-make.exe -j4

cd ..
rmdir /S /Q build



GOTO EOF


:EOF


