
//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          libselectfont
//          Copyright 2021 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef SELECTFONT_H
#define SELECTFONT_H

#include "selectfont_global.h"
#include <QDebug>
#include <QDialog>
#include <QSettings>
//#include <QWhatsThis>
#define RED 218
#define GREEN 255
#define BLUE 221

#ifdef Q_OS_LINUX
#define FONT_SIZE 12
#endif
#ifdef Q_OS_WIN
#define FONT_SIZE 11
#endif

QT_BEGIN_NAMESPACE
namespace Ui
{
class SelectFont;
}
QT_END_NAMESPACE
#ifdef Q_OS_WINDOWS
#ifndef QT_STATIC
class SELECTFONT_EXPORT SelectFont : public QDialog
#endif

#ifdef QT_STATIC
    class SelectFont : public QDialog
#endif
#endif
#ifdef Q_OS_LINUX
        class SelectFont : public QDialog
#endif

        {
            Q_OBJECT

        private:
            Ui::SelectFont *ui;
            void triggerSignal();
            QString display_name;
            QString executable_name;


        public:
            SelectFont(QWidget *parent = nullptr);
            ~SelectFont();
            void setFont(QString display_name, QString executable_name);
            void setFont(QString display_name, QString executable_name, QIcon icon);

        signals:
            void valueChanged(QFont);
        };
#endif // SELECTFONT_H
