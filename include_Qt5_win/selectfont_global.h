//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          libselectfont
//          Copyright 2021 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef QT_STATIC
#include <QtCore/QtGlobal>

#ifndef SELECTFONT_GLOBAL_H
#define SELECTFONT_GLOBAL_H

#if defined(SELECTFONT_LIBRARY)
#define SELECTFONT_EXPORT Q_DECL_EXPORT
#else
#define SELECTFONT_EXPORT Q_DECL_IMPORT
#endif

#endif // SELECTFONT_GLOBAL_H
#endif
