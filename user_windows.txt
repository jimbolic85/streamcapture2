Message published 2022-06-09<br><br>

English<br>
svtplay-dl writes on its download page that the latest stable version is 4.13<br>
If you ask svtplay-dl, you get the answer 4.13-1-g487c7fe<br>
(svtplay-dl --version)<br>
That's why it looks different in streamCapture2.<br><br>

Svenska<br>
svtplay-dl skriver på sin nedladdningssida att den senaste stabila versionen är 4.13<br>
Frågar du svtplay-dl får du svaret 4.13-1-g487c7fe<br>
(svtplay-dl --version)<br>
Det är därför det ser annorlunda ut i streamCapture2.<br>