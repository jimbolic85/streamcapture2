23:00 December 11, 2022
Version 2.8.3 BETA
Improved user interface.
Uses Qt 6.4.1, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.7, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.1, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31)

23:00 December 10, 2022
Version 2.8.2 BETA
Improved user interface.
Uses Qt 6.4.1, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.7, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.1, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

22:00 December 4, 2022
Version 2.8.1
Bugfix: Incorrect prompt to update svtplay-dl. (Windows only).
Använder Qt 6.4.1, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.7, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.4.1, Kompilerad med GCC 11.3.0 (Linux, GLIBC >= 2.35).

01:00 October 30, 2022
Version 2.8.0
Video resolution 480p
Uses Qt 6.4.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.7, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.0, Compiled with GCC 11.3.0 (Linux, GLIBC >= 2.35).

23:00 October 27, 2022
Version 2.7.3 BETA
Video resolution 480p
Uses Qt 6.4.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.6, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.0, Compiled with GCC 11.3.0 (Linux, GLIBC >= 2.35).

23:00 October 26, 2022
Version 2.7.2
Bugfix: streamCapture2 could not find svtplay-dl version 4.14.
Uses Qt 6.4.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.6, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.0, Compiled with GCC 11.3.0 (Linux, GLIBC >= 2.35).

22:00 October 15, 2022
Version 2.7.1
Improved "Select Font" dialog.
Uses Qt 6.4.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.6, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.0, Compiled with GCC 11.3.0 (Linux, GLIBC >= 2.35).

20:00 October 1, 2022
Version 2.7.0
Bugfix: Login to a streaming service. If you create a user without a username, the program may crash if you remove the streaming service.
Ability to choose the file name of the downloaded file. Individual files or files from the download list.
7zip updated to version 22.01 (Windows only).
FFmpeg updated to version 5.1.1.
Qt 6.4.0 updated to stable version.
Uses Qt 6.4.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.6, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.0, Compiled with GCC 11.3.0 (Linux, GLIBC >= 2.35).

23:59 September 30, 2022
Version 2.6.4 BETA
7zip updated to version 22.01 (Windows only).
Uses Qt 6.4.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.6, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.0, Compiled with GCC 11.3.0 (Linux, GLIBC >= 2.35).

10:00 September 30, 2022
Version 2.6.3 BETA
Qt 6.4.0 stable version.
Uses Qt 6.4.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.6, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.0, Compiled with GCC 11.3.0 (Linux, GLIBC >= 2.35).

23:00 September 25, 2022
Version 2.6.2 BETA
Bugfix: Login to a streaming service. If you create a user without a username, the program may crash if you remove the streaming service.
Uses Qt 6.4.0 RC, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.6, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.0 RC, Compiled with GCC 11.3.0 (Linux, GLIBC >= 2.35).

15:00 September 25, 2022
Version 2.6.1 BETA
Ability to choose the file name of the downloaded file. Individual files or files from the download list.
Uses Qt 6.4.0 RC, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.6, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.0 RC, Compiled with GCC 11.3.0 (Linux, GLIBC >= 2.35).

14:00 September 25, 2022
Version 2.6.0 BETA
Ability to choose the file name of the downloaded file.
FFmpeg updated to version 5.1.1.
Uses Qt 6.4.0 RC, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.6, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.0 RC, Compiled with GCC 11.3.0 (Linux, GLIBC >= 2.35).

17:00 September 15, 2022
Version 2.5.10
Uses Qt 6.4.0 RC, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.6, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.0 RC, Compiled with GCC 11.3.0 (Linux, GLIBC >= 2.35).

19:00 September 9, 2022
Version 2.5.10
Uses Qt 6.4.0 beta 4, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.6, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.0 beta 4, Compiled with GCC 11.3.0 (Linux, GLIBC >= 2.35).

22:00 August 4, 2022
Version 2.5.10
FFmpeg updated to version 5.0.1.
Alternative download for Linux. AppImage with Qt 6.4.0.
Uses Qt 6.3.1, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.5, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.4.0 beta 4, Compiled with GCC 11.3.0 (Linux, GLIBC >= 2.35).

22:00 August 1, 2022
Version 2.5.9
Downgraded from Qt6 to Q5. For better reliability.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.5, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 5.15.5, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

07:00 August 1, 2022
Version 2.5.8
Bugfixes: Several nasty bugs. The program did not save its settings. Linux only.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.5, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.1, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

10:00 June 11, 2022
Version 2.5.6
Bug fix: "download svtplay-dl" crashes if you click recklessly.
More informative messages for network problems.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

00:30 June 11, 2022
Version 2.5.5 BETA
Bug fix: "download svtplay-dl" crashes if you click recklessly.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

22:00 June 10, 2022
Version 2.5.4
Possible to select which toolbar to display when "download svtplay-dl" starts.
Possible to display important messages from the developer when the program starts.
Complete Italian and Swedish translation.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

20:00 June 10, 2022
Version 2.5.3 BETA
Possible to select which toolbar to display when "download svtplay-dl" starts.
Possible to display important messages from the developer when the program starts.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

02:00 June 5, 2022
Version 2.5.2
Bug fix: The icon is back on the desktop and in the application menu. (Linux)
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

23:30 May 31, 2022
Version 2.5.1
Bug fix: "Search for svtplay-dl" could cause the program to crash if there was no internet connection.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

20:00 May 28, 2022
Version 2.5.0
The interface has been simplified. Made more logical and easy to understand.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

16:00 May 28, 2022
Version 2.4.4 BETA
Minor changes to the interface.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

23:00 May 27, 2022
Version 2.4.3 BETA
Small update of the Italian translation.
Minor changes to the interface.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

11:00 May 27, 2022
Version 2.4.2 BETA
Updated Italian and Swedish translation.
Minor changes to the interface.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

01:00 May 27, 2022
Version 2.4.1 BETA
The interface has been simplified. Made more logical and easy to understand.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

19:00 May 23, 2022
Version 2.4.0
Information about the latest svtplay-dl snapshot is only displayed if it is newer than the latest stable release.
Simplified and clearer interface on "Download svtplay-dl".
Minor improvements and clarifications.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

16:00 May 21, 2022
Version 2.3.3 BETA
Simplified and clearer interface on "Download svtplay-dl".
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

02:00 May 20, 2022
Version 2.3.2 BETA
Information about the latest svtplay-dl snapshot is only displayed if it is newer than the latest stable release.
Test of new offline installer for Windows.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

10:00 May 19, 2022
Version 2.3.1
Bugfix: "Download to Default Location" had stopped working.
Minor improvements and clarifications.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

12:00 May 18, 2022
Version 2.3.0
New update from svtplay-dl, version 4.12 has been implemented. Enter the desired resolution.
Possibility to select resolution when you click on "Add all episodes to the download list".
"Download svtplay-dl" has a smaller program window and smaller icons.
Bugfix: "Download svtplay-dl". If the path to bleedingedge and stable no longer exists, this will be handled better.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

00:00 May 18, 2022
Version 2.2.5 BETA
Bug fix: "Download svtplay-dl". If the path to bleedingedge and stable no longer exists, this will be handled better.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows 64-bit).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

23:00 May 17, 2022
Version 2.2.4 BETA
Possibility to select resolution when clicking "Add all Episodes to Download List".
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows 64-bit).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

23:00 May 16, 2022
Version 2.2.3 BETA
"Download svtplay-dl" has a smaller program window.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows 64-bit).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

21:00 May 15, 2022
Version 2.2.2 BETA
Smaller icons on "download svtplay-dl".
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows 64-bit).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

14:00 May 14, 2022
Version 2.2.1 BETA
New update from svtplay-dl, version 4.12 has been implemented. Enter the desired resolution.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows 64-bit).
Uses Qt 5.15.4, Compiled with MinGW GCC 8.1.0 (Windows 32-bit).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

23:30 May 13, 2022
Version 2.2.0
Right-click the taskbar to take a screenshot. streamCapture2 and "download svtplay-dl"
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows 64-bit).
Uses Qt 5.15.4, Compiled with MinGW GCC 8.1.0 (Windows 32-bit).
Uses Qt 5.15.4, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

23:00 May 10, 2022
Version 2.1.4
Downloads NFO information. From, for example, svtplay.se.
Improved display of license information and "About" ffmpeg and svtplay-dl.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows 64-bit).
Uses Qt 5.15.3, Compiled with MinGW GCC 8.1.0 (Windows 32-bit).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

20:30 May 9, 2022
Version 2.1.3 BETA
Even better display of NFO information. And improved display of license information and "About" ffmpeg and svtplay-dl.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

00:30 May 9, 2022
Version 2.1.2 BETA
Improved display of NFO information.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

23:00 May 6, 2022
Version 2.1.1 BETA
Bug fix: Downloads NFO information. From, for example, svtplay.se.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

18:00 May 6, 2022
Version 2.1.0 BETA
Downloads NFO information. From, for example, svtplay.se.
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

22:30 April, 30 2022
Version 2.0.12
Updated Italian translation.
Updated Swedish translation.
Uses Qt 6.3.0 (Windows).
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

22:00 April, 8 2022
Version 2.0.11
Bug fix: Works better when the user chooses method and quality.
Uses Qt 5.15.3, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

18:00 April, 8 2022
Version 2.0.10
Displays a message unless streamCapture2 is allowed to open the folder where streamCapture2 is located.
Bug fix: Error message when clicking "Check for latest svtplay-dl from bin.ceicer.com..." For the very first time.
Qt updated to version 5.15.3 Open source code. Built on Windows 10. (Windows)
Uses Qt 5.15.3, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

12:00 April, 7 2022
Version 2.0.9 BETA
Swedish translation.
Uses Qt 5.15.3, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

16:00 April, 3 2022
Version 2.0.8 BETA
Bug fix: Error message when clicking "Check for latest svtplay-dl from bin.ceicer.com..." For the very first time.
Uses Qt 5.15.3, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

23:30 March, 27 2022
Version 2.0.7 BETA
Qt updated to version 5.15.3 Open source code. Built on Windows 10. (Windows)
Uses Qt 5.15.3, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

04:00 March, 26 2022
Version 2.0.6 BETA
Displays a message unless streamCapture is allowed to
open the folder where streamCapture2 is located.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

23:00 March, 22 2022
Version 2.0.5
Bug fix: Quality (bit rate) was not displayed correctly.
Bug fix: Misspelling in "Download svtplay-dl...".
Bug fix: You must select the folder twice to save the
downloaded file if the folder is newly created.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

21:00 March, 20 2022
Version 2.0.4
Bug fix: streamCapture2 did not always find svtplay-dl.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

17:00 March, 11 2022
Version 2.0.2
Incorrect information about which operating system was used when compiling the program.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

17:00 March, 6 2022
Version 2.0.1
Incorrect message when streamCapture2 needs to be updated. Only the linux version.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

22:00 March, 4 2022
Version 2.0.0
New function: download directly to "stable" and "bleedingedge".
New function: "Check for new versions of svtplay-dl at start."
New function: Search for latest stable.
New function: Editable "Download svtplay-dl" settings.
"Download svtplay-dl..." 
-Follows the font selection made in "Select font...".
-Possible to make the text larger or smaller.
New "About" dialog.
Updated FFmpeg to version 5.0
Bug fix: Desktop shortcut and menu shortcut.
Bug fix: Linux version, "Check for updates at program start".
Bug fix: "Edit settings" severe bug.
Bug fix: "readyupdate"
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

21:00 February, 21 2022
Version 1.3.22 BETA
Bug fix: "Check for the latest svtplay-dl from bin.ceicer.com..."
Larger desktop icon (Windows)
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

08:30 February, 20 2022
Version 1.3.21 BETA
Updated Swedish translation.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

21:00 February, 19 2022
Version 1.3.20 BETA
Bug fix: "readyupdate"
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

16:00 February, 17 2022
Version 1.3.19 BETA
New "About" dialog.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux).

09:00 February, 16 2022
Version 1.3.18 BETA
Change location on "Find latest" and "Find latest stable".
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

20:00 February, 15 2022
Version 1.3.17 BETA
Improved: download directly to "stable" and "bleedingedge".
Nicer icons.
Possible bug fixed: download directly to "stable" and "bleedingedge".
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:00 February, 14 2022
Version 1.3.16 BETA
"Check for new versions of svtplay-dl at start".
-Improved
Editable "Download svtplay-dl" settings.
Minor bug fixes and improvements.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:00 February, 13 2022
Version 1.3.15 BETA
New function "Check for new versions of svtplay-dl at start."
Minor bug fixes and improvements.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

10:00 February, 12 2022
Version 1.3.14 BETA
Bug fix, "Edit settings" severe bug. 
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

08:00 February, 12 2022
Version 1.3.13 BETA
Updated FFmpeg to version 5.0
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

21:30 February, 9 2022
Version 1.3.12 BETA
Updated "Check for the latest svtplay-dl from bin.ceicer.com..."
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

21:30 February, 8 2022
Version 1.3.11 BETA
Bug fix, Linux version, "Check for updates at program start".
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

22:30 February, 7 2022
Version 1.3.10 BETA
Updated Italian translation.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

00:00 February, 7 2022
Version 1.3.9 BETA
"Download svtplay-dl..."
-Updated
-Swedish translation.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

20:00 February, 5 2022
Version 1.3.8 BETA
"Download svtplay-dl..."
-Updated
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:30 February, 4 2022
Version 1.3.7 BETA
Search for latest stable.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

14:00 February, 4 2022
Version 1.3.6 BETA
Bug fix: Desktop shortcut and menu shortcut.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux)

12:00 February, 4 2022
Version 1.3.5 BETA
Minor bug fixes and improvements.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

11:00 February, 3 2022
Version 1.3.4 BETA
Translation.
Linux version.
Minor bug fixes and improvements.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

02:00 February, 3 2022
Version 1.3.3 BETA
"Download svtplay-dl..."
-Improved possibility to download directly to "stable" and "bleedingedge".
Minor bug fixes and improvements.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

02:00 February, 1 2022 
Version 1.3.2 BETA
"Download svtplay-dl..." 
-Follows the font selection made in "Select font".
-Possible to make the text larger or smaller.
-Possibility to download directly to "stable" and "bleedingedge".
Minor bug fixes and improvements.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

02:00 January, 23 2022 
Version 1.3.0
Updated "Select font dialog".
Updated "Check for the latest svtplay-dl from bin.ceicer.com..."
Right-click on the status bar.
Minor bug fixes and improvements.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

02:00 December, 31 2021 
Version 1.2.0
Updated "Download svtplay-dl".
Updated "Select font dialog".
Upgraded method to update Linux AppImage.
Updated svtplay-dl stable version 4.10
Updated svtplay-dl latest snapshot 4.10
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

18:00 December, 18 2021 
Version 1.1.7
Default to decompress and delete compressed file when downloading new version of svtplay-dl.
Updated svtplay-dl latest snapshot 4.9-14-g6966876
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

22:00 November, 25 2021 
Version 1.1.6
Bug fix, did not always find the latest stable svtplay-dl when searching at bin.ceicer.com.
Updated svtplay-dl latest snapshot 4.9-7-gd9209f8
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

17:00 November, 14 2021 
Version 1.1.5
Bug fix, An icon that was not visible, is visible again.
Updated svtplay-dl latest snapshot 4.9-5-g77b43ea
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

17:30 November, 13 2021
Version 1.1.4
Bugfix, "Delete all settings and Exit" works again.
Download the "Microsoft Visual C++ Redistributable" runtime file from bin.ceicer.com. Windows version only.
Open the folder you downloaded svtplay-dl to directly from the program.
Updated svtplay-dl latest snapshot 4.9-3-g4f2fa55
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

11:00 November 4, 2021
Version 1.1.3
Buggfix, a message box was displayed twice.
Better information when searching for svtplay-dl to download from bin.ceicer.com.
Minor improvements, a little faster code.
Updated svtplay-dl stable version 4.9
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

22:00 October 26, 2021 
Version 1.1.2
Bug fix, now it works to update AppImage.
Updated svtplay-dl stable version 4.7
Updated svtplay-dl latest snapshot 4.7-4-gb3fd51d
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

15:00 October 10, 2021 
Version 1.1.1
Clearer messages when downloading svtplay-dl.
Updated svtplay-dl stable version 4.5
Updated svtplay-dl latest snapshot 4.5-1-gadcaa73
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

14:00 September 5, 2021 
Version 1.1.0
When you search for the latest svtplay-dl, the latest version is displayed in the combo box.
Automatically searches when you change operating system.
Updated svtplay-dl stable version 4.2
Updated svtplay-dl latest snapshot 4.2-5-g404373e
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

13:38 July 26, 2021 
Version 1.0.1
Updated svtplay-dl stable version 4.2
Updated svtplay-dl latest snapshot 4.2
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:00 July 23, 2021 
Version 1.0.0
After testing and testing and testing again, I hope everything works.
If it does not, report all bugs, thanks!
Updated help.
Updated svtplay-dl latest snapshot 4.1-5-gc05a736
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

13:30 July 23, 2021 
Version 0.99.9
Coming soon version 1.0.0!
Italian translation.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:00 July 21, 2021 
Version 0.99.8
Coming soon version 1.0.0!
Minor bug fixes.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

13:30 July 15, 2021 
Version 0.99.7
Coming soon version 1.0.0!
Bug fix: Error when using 'st' cookie.
More and clearer print from svtplay-dl when "Show more" is checked.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

01:00 July 15, 2021 
Version 0.99.6
Coming soon version 1.0.0!
The program stops attempts to save to folders that you do not have the right to save to.
Thanks to "Rickard" for drawing my attention to this.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

02:00 July 13, 2021 
Version 0.99.5
Coming soon version 1.0.0!
Better menus, clearer explanations and nicer icons.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

04:30 July 12, 2021 
Version 0.99.4
Coming soon version 1.0.0!
Better information from FFmpeg.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

17:30 July 11, 2021 
Version 0.99.3
Coming soon version 1.0.0!
Improved "Save As ..." dialog.
Better designed dialogs.
Some minor bug fixes.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:30 July 10, 2021 
Version 0.99.2
Coming soon version 1.0.0!
Possibility to "Edit saved searches".
Updated svtplay-dl latest snapshot 4.1
Updated svtplay-dl stable version 4.1
Some minor bug fixes.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

15:30 July 9, 2021 
Version 0.99.1
Coming soon version 1.0.0!
Some minor bug fixes.
Automated download of latest svtplay-dl.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:30 July 8, 2021 
Version 0.99.0
Coming soon version 1.0.0!
Bug fix: Select bit rate.
Bug fix: Add to and download from the download list.
Bug fix: Download all episodes of a TV series.
Improved "download from bin.ceicer.com"
Separate dialog box for displaying different things, in order not to interfere with searches and downloads.
Updated svtplay-dl latest snapshot 4.0-5-ga162357
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

16:01 July 6, 2021 
Version 0.29.5 BETA 2
Bug fix: Select bit rate.
Bug fix: Add to and download from the download list.
Bug fix: Download all episodes of a TV series.
Improved "download from bin.ceicer.com"
Separate dialog box for displaying different things.
In order not to interfere with searches and downloads.
Updated svtplay-dl latest snapshot 4.0-4-gc5ae975
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

00:01 July 6, 2021 
Version 0.29.5 BETA
Bug fix: Select bit rate.
Bug fix: Add to and download from the download list.
Bug fix: Download all episodes of a TV series.
Separate dialog box for displaying different things.
In order not to interfere with searches and downloads.
Updated svtplay-dl latest snapshot 4.0-4-gc5ae975
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

17:30 July 1, 2021 
Version 0.29.4
Complete Italian translation.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

03:30 July 1, 2021 
Version 0.29.3
Opportunity to search for the latest svtplay-dl from bin.ceicer.com
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:30 June 30, 2021 
Version 0.29.2
Downloaded compressed svtplay-dl can be automatically removed after decompression.
Updated svtplay-dl latest snapshot 4.0-3-gc66590c
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

16:30 June 28, 2021 
Version 0.29.1
A major reworking of the program, it works better to choose
download quality and all messages from the program
is clearer.
The manual has been revised.
Possibility to zoom in and out with a keyboard press.
Updated svtplay-dl latest snapshot 4.0-2-g09843d6
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

20:30 June 25, 2021 
Version 0.29.0 BETA 3
A major reworking of the program, it should work better to choose
download quality and all messages from the program
should be clear.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

17:00 June 25, 2021 
Version 0.29.0 BETA 2
A major reworking of the program, it should work better to choose
download quality and all messages from the program
should be clear.
Updated svtplay-dl latest snapshot 4.0-2-g09843d6
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

03:00 June 21, 2021 
Version 0.29.0 BETA 1
A major reworking of the program, it should work better to choose
download quality and all messages from the program
should be clear.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

20:00 June 20, 2021 
Version 0.28.15
Important bug fix: The download list now works.
Does not work earlier if you used the "Add to Downloads list" button.
The menu worked.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

15:00 June 20, 2021 
Version 0.28.14
Significantly improved decompression speed of downloaded
svtplay-dl.zip (Windows).
The Linux version can decompress *.tar.gz and *.zip
The Windows version can decompress *.tar.gz and *.zip
Improved dialogs for saving media files and selecting the
svtplay-dl download location.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

20:00 June 19, 2021 
Version 0.28.13 BETA 3
Significantly improved decompression speed of downloaded
svtplay-dl.zip (Windows).
The Linux version can decompress *.tar.gz and *.zip
The Windows version can decompress *.tar.gz and *.zip
Improved dialogs for saving media files and selecting the
svtplay-dl download location.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

17:35 June 18, 2021 
Version 0.28.12
Updated svtplay-dl stable version 4.0
Updated svtplay-dl latest snapshot 4.0  
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:30 June 15, 2021 
Version 0.28.11
"download svtplay-dl from bin.ceicer.com" shares the language
settings with the main application.
Minor improvements.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:00 June 14, 2021 
Version 0.28.10 BETA
"download svtplay-dl from bin.ceicer.com" shares the language
settings with the main application.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:59 June 13, 2021 
Version 0.28.9
Updated svtplay-dl latest snapshot 3.9.1-2-g00e5c84 
Maintenance Tool starts with "Update components" (Windows).
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:45 June 12, 2021
Version 0.28.8
Nicer message box when updating.
Moved the configuration file for "download svtplay-dl".
Visit source code and binary files.
Full Italian translation.
Updated Italian help file.
Minor improvements.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

10:30 June 10, 2021
Version 0.28.7 BETA
Nicer message box when updating.
Moved the configuration file for "download svtplay-dl".
Visit source code and binary files.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

00:30 June 10, 2021
Version 0.28.6
Bug fix: Unable to download. Removed "--remux"     
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

21:30 June 7, 2021
Version 0.28.5
Updated svtplay-dl stable version 3.9.1
Updated svtplay-dl latest snapshot 3.9.1      
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

18:30 June 4, 2021
Version 0.28.4
Updated svtplay-dl latest snapshot 3.8-27-gd32bc02        
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

22:14 June 3, 2021
Version 0.28.3
Updated svtplay-dl latest snapshot 3.8-25-g88432ed        
"-list quality" (search function) updated.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:30 May 30, 2021
Version 0.28.2
Updated svtplay-dl latest snapshot 3.8-23-g89284c9
Added "Auto Select" option for bitrate and method.           
"-list quality" (search function) further updated.
Opportunity to test language files in "svtplay-dl from bin.ceicer.com"
Full Italian translation.
Updated help files, English, Italian and Swedish.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

01:48 May 28, 2021
Version 0.28.0                   
"--list-quality" (search function) updated.
Works with the latest svtplay-dl.
New: Download svtplay-dl from streamCapture2.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

21:48 May 24, 2021
Version 0.27.6                    
Updated svtplay-dl latest snapshot 3.8-22-g45fceaa
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

22:48 May 23, 2021
Version 0.27.5                    
Updated svtplay-dl latest snapshot 3.8-18-g1c3d8f1
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

17:30 May 20, 2021
Version 0.27.4                    
Updated svtplay-dl latest snapshot 3.8-13-g3d46e85
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:30 May 17, 2021
Version 0.27.3
Complete Italian translation.
Updated svtplay-dl latest snapshot 3.8-10-gc9a606d
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

14:05 May 16, 2021
Version 0.27.2
'st' cookie works with the download list.
Updated svtplay-dl latest snapshot 3.8-3-g107d3e0
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

05:05 May 16, 2021
Version 0.27.1
Bug Fix: Failed to search for video streams when login required.
Manage many different 'st' cookies.
Edit the configuration file.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

17:55 May 14, 2021
Version 0.26.7
Updated svtplay-dl latest snapshot 3.8-2-g2c5101a
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

20:55 May 13, 2021
Version 0.26.6
Updated svtplay-dl stable version 3.8
Updated svtplay-dl latest snapshot 3.8
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

15:55 May 13, 2021
Version 0.26.5
Updated svtplay-dl latest snapshot 3.7-16-g494c9e3
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

20:55 May 11, 2021
Version 0.26.4
Updated svtplay-dl latest snapshot 3.7-12-ga5e4166
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:55 May 10, 2021
Version 0.26.3
Updated svtplay-dl latest snapshot 3.7-10-gd575112
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

11:05 May 9, 2021
Version 0.26.2
Updated svtplay-dl latest snapshot 3.7-4-g3497e05
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

14:05 May 6, 2021
Version 0.26.1
Possible to use 'st' cookies.
Optimized, more efficient code.
Updated svtplay-dl stable version 3.7
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

12:45 April 28, 2021
Version 0.25.8
Updated svtplay-dl latest snapshot 3.7
Improved display of downloadable video streams.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

21:00 April 22, 2021
Version 0.25.6
Bug fix: Unable to search for video streams.
Updated svtplay-dl latest snapshot 3.6-3-gaae55fc
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

19:00 April 20, 2021
Version 0.25.5
Updated svtplay-dl stable version 3.6
Updated svtplay-dl latest snapshot 3.6-2-g0dcb01f
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:00 April 15, 2021
Version 0.25.4
Updated svtplay-dl stable version 3.5
Updated svtplay-dl latest snapshot 3.5-1-g8805627
Updated FFmpeg 4.4
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

23:59 April 13, 2021
Version 0.25.3
Updated svtplay-dl stable version 3.4
Changes in the code to make the program a little faster and slightly smaller.
Possible to edit the name of streaming services.
Possible to have spaces in the name of streaming services.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

00:30 April 8, 2021
Version 0.25.1
Bug fixed: Error opening your own language file in the program.
Nicer presentation of "Method" and "Bitrate".
Help translated into Italian.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

12:31 April 3, 2021
Version 0.25.0
Updated svtplay-dl latest snapshot 3.3-2-gcdf394e
Name change of "Pay TV" to "Login"
"Direct Download of all Video Streams in current serie (Not from the Download List)" shows when each file has been downloaded and how many files remain.
Improved progress bar.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

08:31 March 17, 2021
Version 0.24.0
Updated svtplay-dl stable version 3.3
Updated svtplay-dl latest snapshot 3.3
Progress indicator showing the downloads.
Bug fix: Now you can see the number of searches you have previously chosen to save.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

10:00 March 16, 2021
Version 0.23.2
Updated svtplay-dl latest snapshot 3.2-10-g7440e3f
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

12:05 March 15, 2021
Version 0.23.1
Updated svtplay-dl latest snapshot 3.2-9-g028971b
Updated manual.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

03:30 March 13, 2021
Version 0.23.0
Bugfix download from Viafree.
Drag and drop function added.
More messages in the status bar.
Better translations in Linux AppImage.
Updated svtplay-dl stable version 3.2
Updated svtplay-dl latest snapshot 3.2-2-g6ef5d61.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

21:30 March 6, 2021
Version 0.22.4
Updated svtplay-dl latest snapshot 3.1-5-gcb3612b (Linux, Windows).
Linux AppImage updated to GLIBC 2.27 (2018-02-01). Now works with most distributions from May 2018 and newer.
Clearer messages if you try to copy to the default folder and the file already exists.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 7.5.0 (Linux).

00:30 March 1, 2021
Version 0.22.3
Updated svtplay-dl latest snapshot 3.1-4-g998f51f (Windows).
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

12:00 February 27, 2021
Version 0.22.2
"Update" is disabled if there is no update (Linux).
Minor corrections
Updated graphical interface.
Updated FFmpeg 4.3.2 (Linux)
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

00:00 February 26, 2021
Version 0.22.1
Updated svtplay-dl stable version 3.0-7-ge423c1c (Linux)
Updated svtplay-dl stable version 3.1 (Windows)
Updated svtplay-dl latest snapshot 3.1 (Linux, Windows)
Updated FFmpeg 4.3.2
Updated graphical interface.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

14:50 February 18, 2021
Version 0.22.0
Updated svtplay-dl latest snapshot 3.0-5-g8cd5793.
Italian translation.
Incomplete German translation removed.
Opportunity to test language files.
"About" shows a working e-mail address.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

00:50 February 15, 2021
Version 0.21.2
Updated svtplay-dl stable version 2.9-8-ga263a66
Updated svtplay-dl latest snapshot 3.0
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

12:50 January 25, 2021
Version 0.21.1
AppImage: "Delete all settings and Exit" 
Removes shortcuts on the desktop and in the application menu.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

22:50 January 24, 2021
Version 0.21.0
Updated svtplay-dl latest snapshot 2.8-11-gd193679.
AppImage: Desktop file and icon on the desktop.
AppImage: Desktop file and icon in the application menu.
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux).

22:50 January 4, 2021
Version 0.20.18
Updated svtplay-dl latest snapshot 2.8-9-gc3ff052
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux). 

21:02 December 9, 2020
Version 0.20.17
Updated svtplay-dl stable version 2.8-3-g18a8758
Uses Qt 5.15.2, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.2, Compiled with GCC 5.4.0 (Linux). 

19:02 November 21, 2020
Version 0.20.16
Updated svtplay-dl stable version 2.8
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux). 

18:01 November 15, 2020
Version 0.20.15
Updated svtplay-dl version 2.7-5-gbf1f9c5
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux). 

21:55 November 6, 2020
Version 0.20.14
Updated svtplay-dl version 2.7-4-gef557cb
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux). 

03:45 November 1, 2020
Version 0.20.13
Warning for too high bitrate in case of failed download.
More reliable update of AppImage.
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux). 

18:34 October 27, 2020
Version 0.20.12
Bug fix: svtplay-dl in system path not found.
Updated svtplay-dl version 2.7
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).  

18:09 October 13, 2020
Version 0.20.11
Updated svtplay-dl version 2.7-3-ga550bd1
More reliable update of AppImage.
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).       

18:09 October 11, 2020
Version 0.20.10
Updated svtplay-dl version 2.6-4-gd6dc139
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

20:09 October 2, 2020
Version 0.20.9
Updated svtplay-dl version 2.6
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

22:05 September 27, 2020
Version 0.20.8
Updated svtplay-dl version 2.5-7-g7db9f7e
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

19:39 September 23, 2020
Version 0.20.7
Updated svtplay-dl stable version 2.5
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

19:39 September 22, 2020
Version 0.20.6
Updated svtplay-dl version 2.5-4-ge92ebbb
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

00:55 September 20, 2020
Version 0.20.5
Updated svtplay-dl version 2.5
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

00:43 September 19, 2020
Version 0.20.4
Updated svtplay-dl version 2.4-71-g5fc7750
Uses Qt 5.15.1, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.1, Compiled with GCC 5.4.0 (Linux).

19:43 September 14, 2020
Version 0.20.3
Bug fix: Works better with the latest versions of svtplay-dl.
Updated svtplay-dl version 2.4-67-g478fd3b
Updated FFmpeg to version 4.3.1 (Linux)
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

18:43 September 13, 2020
Version 0.20.2
Opportunity to create a shortcuts.
The program updates itself. (Applies to Linux AppImage).
Updated svtplay-dl version 2.4-62-g2920bb9
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

15:20 July 12, 2020
Version 0.20.1
Opportunity to create a shortcut on the desktop.
Opportunity to receive notifications when download is complete.
Bug fix: The program could disappear if you switch between different number of screens.
Bug fix: Swedish translation of "FFmpeg can be found in system path" has been corrected.
Minor improvements.
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

13:26 June, 28 2020
Version 0.20.0
Opportunity to select svtplay-dl from the system path.
Opportunity to select svtplay-dl from the file system.
Bug fix: Now streamCapture remembers the selected font.
Minor improvements.
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

21:43 June, 15 2020
Version 0.19.2
Bug fix: Unpleasant bug that caused the program to crash.
This happened when the program was looking for updates at program startup.
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

17:07 June, 14 2020
Version 0.19.1
Uses the included FFmpeg or, if not included, the system's FFmpeg.
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

23:28 May, 3 2020
Version 0.19.0
Updated svtplay-dl version 2.4-42-g916e199
Opportunity to switch between svtplay-dl stable and latest snapshot.
Updated FFmpeg version 4.2.3
Installation packages for Linux and Windows. Shortcuts on the start menu.
Easy update with minimal download.
Uses Qt 5.15.0, Compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.15.0, Compiled with GCC 5.4.0 (Linux).

20:27 April, 27 2020
Version 0.18.5
Bug fix: Shortcuts on "Start", "Program menu" broken in version 0.18.4 (Windows).
Link to svtplay-dl forum for issues.
Selectable font.
Better contrasting colors between font and background.
Minor improvements.
Uses Qt 5.14.2, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.14.2, Compiled with GCC 5.4.0 (Linux).

12:42 April, 9 2020
Version 0.18.4
Updated font.
Updated swedish translation.
New background color.
Uses Qt 5.14.2, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.14.2, Compiled with GCC 5.4.0 (Linux).

01:09 April, 3 2020
Version 0.18.3
Updated svtplay-dl version 2.4-35-g81cb18f
Uses Qt 5.14.2, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.14.2, Compiled with GCC 5.4.0 (Linux).

13:49 February, 2 2020
Version 0.18.2
Updated svtplay-dl version 2.4-31-g25d3105
Updated FFmpeg version 4.2.2
Uses Qt 5.14.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.14.0, Compiled with GCC 5.4.0 (Linux).

12:35 December, 31 2019
Version 0.18.1
Check that svtplay-dl and ffmpeg are included and found.
No indication of svtplay-dl version in the "About" dialog.
Uses Qt 5.14.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.14.0, Compiled with GCC 5.4.0 (Linux).

16:23 December, 20 2019
Version 0.18.0
After "List all Episodes", select and download selected episodes.
Uses Qt 5.14.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.14.0, Compiled with GCC 5.4.0 (Linux).

22:55 November, 25 2019
Version 0.17.0
Improved display of search results.
Opportunity to use a preselected folder for download. 
Possibility to use a default folder where the media files and subtitles are copied. 
Ability to list all episodes. 
Ability to download all episodes at once. 
Opportunity to cancel ongoing download. (Windows)
Ability to delete all configuration files.
Improved subtitle management.
Many bug fixes.
Corrected misspellings
"Uninstaller" removed from "Portable" version.
Uses Qt 5.13.2, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.2, Compiled with GCC 5.4.0 (Linux).

11:01 October, 20 2019
Version 0.16.0
Static FFmpeg version 4.2.1 (Linux and Windows).
Updated svtplay-dl version 2.4-29-gc59a305
Clearer message from svtplay-dl about the search result.
Minor bug fixes.
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

21:49 October 11, 2019
Version 0.15.0
Bug: "/" at the end of the address is now deleted.
Menu selection to uninstall (Windows).
Updated svtplay-dl version 2.4-26-g5dcc899
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

21:24 September 16, 2019
Version 0.14.0
Removed question marks from search address.
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

19:53 September 16, 2019
Version 0.13.9
Updated svtplay-dl version 2.4-20-g0826b8f
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

23:24 September 13, 2019
Version 0.13.8
Updated svtplay-dl version 2.4-6-gce9bfd3
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

21:59 September  11, 2019
Version 0.13.7
Updated svtplay-dl version 2.4-2-g5466853
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

09:19 September  7, 2019
Version 0.13.6
Updated svtplay-dl version 2.3-23-gbc15c69
Updated ffmpeg version 4.2 (Windows).
Uses Qt 5.13.1, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.1, Compiled with GCC 5.4.0 (Linux).

22:17 September  2, 2019
Version 0.13.5
Updated svtplay-dl version 2.2-5-gd1904b2 (Linux).
Updated svtplay-dl version 2.2-7-g62cbf8a (Windows).
Clearer code and comments.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

00:06 September  1, 2019
Version 0.13.4
Updated svtplay-dl version 2.2-2-g838b3ec (Linux).
Updated svtplay-dl version 2.2-14-gc77a4a5 (Windows).
Minor bug. Don't show "Search..." after unsuccessful search.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

18:03  August  28, 2019
Version 0.13.3
Updated svtplay-dl version 2.2-1-g9146d0d
Download link points to gitlab.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

09:48  August  24, 2019
Version 0.13.2
Updated svtplay-dl version 2.1-65-gb64dbf3
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

17:43  August  21, 2019
Version 0.13.1
Bug fix: "Automatically checks for updates when the program starts" now works.
Updated svtplay-dl version 2.1-57-gf429cfc
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

17:59  August  17, 2019
Version 0.13.0
Information about what is updated when the program checks for updates.
Improved "About".
Moved "Password" from "File" menu to "Pay TV".
Better status messages.
Confirmation that the video stream has been downloaded.
Display numbers of downloaded streams.
Operating system dependent display of path.
Minor bug fix.
Architecture instruction set 64-bit.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

12:07  August  6, 2019
Version 0.12.0
Updated svtplay-dl (version 2.1-56-gbe6005e, Windows).
Downgraded ffmpeg (Windows, version 4.1.4, stable).
Create automatic folders, if needed. (Same video stream in different qualities).
Ability to view more information from svtplay-dl.
Add status-tip text.
Architecture instruction set 64-bit.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows)
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux)

16:32 July 31, 2019
Version 0.11.0
Architecture instruction set 64-bit.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows)
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux)

22:41 July 30, 2019
Version 0.11.0 RC 1
Improved help and manual.
Editable Download list.
Possibility to save the same stream in different sizes.
Ability to enter user name and password. For paid streaming services.
Replace obsolete code with more modern.
Use QSettings.
Several bug fixes.
Architecture instruction set 64-bit.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows)
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux)

14:39 July 5, 2019
Version 0.10.7
Several bug fixes. Works better to download a single file.
Duplicate in the selection of different video quality removed.
Architecture instruction set 64-bit.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows)
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

14:59 June 29, 2019
Version 0.10.5
Updated svtplay-dl version 2.1-53-gd33186e
Updated ffmpeg version N-94129-g098ab93257 (Windows).
Architecture instruction set 64-bit.
Uses Qt 5.13.0, Compiled with MinGW GCC 7.3.0 (Windows).
Uses Qt 5.13.0, Compiled with GCC 5.4.0 (Linux).

21:29 February 13, 2018
Version 0.10.3
Updated svtplay-dl (version 1.9.7).
Architecture instruction set 64-bit.
Uses Qt 5.10.0, Compiled with MSVC++ 15.5 (MSVS 2017).

15:41 August 9, 2017
Version 0.10.1
Check box to search for, load and insert subtitles in the video file.
Dynamic user interface.
Updated ffmpeg (version 3.3.3).
Architecture instruction set 64-bit.
Uses Qt 5.9.1, Compiled with MSVC++ 15.0 (MSVS 2017).

01:19 July 26, 2017
Version 0.10.0
Combo Box to choose quality.
Architecture instruction set 64-bit.
Uses Qt 5.9.1, Compiled with MSVC++ 15.0 (MSVS 2017).

15:48 July 16, 2017
Version 0.9.4
Updated ffmpeg (version 3.3.2).
Architecture instruction set 64-bit.
Uses Qt 5.9.1, Compiled with MSVC++ 15.0 (MSVS 2017).

09:38 May 7, 2017
Version 0.9.3
Updated svtplay-dl (version 1.9.4).
Updated ffmpeg (version 3.2.4).
Display Version history

16:33 January 27, 2017
Version 0.9.2
Updated svtplay-dl (version 1.9.1).

18:00 January 14, 2017
Version 0.9.1
Updated svtplay-dl.
Updated ffmpeg.
German translation.
Minor improvments.

01:55 October 30, 2016
Release version 0.9.0
