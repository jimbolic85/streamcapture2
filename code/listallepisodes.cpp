// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "info.h"
#include "newprg.h"
#include "ui_newprg.h"
void Newprg::listAllEpisodes()
{
    if(!fileExists(svtplaydl)) {
        ui->teOut->setTextColor(QColor("red"));
        ui->teOut->setText(svtplaydl +
                           tr(" cannot be found or is not an executable program."));
        ui->teOut->append(tr("Please click on \"Tools\" and select svtplay-dl."));
        ui->teOut->setTextColor(QColor("black"));
        return;
    }

    QStringList ARG;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Stcookies");
    QString stcookie = settings.value("stcookie", "").toString();
    settings.endGroup();

    if(!stcookie.isEmpty()) {
        ARG << "--cookies"  << "st=" + stcookie;
    }

    ui->teOut->setTextColor(QColor("black"));
    ui->pbAdd->setDisabled(true);
    ui->actionAdd->setDisabled(true);
    ui->actionAddAllEpisodesToDownloadList->setEnabled(true);
    ui->pbDownloadAll->setDisabled(true);
    ui->comboBox->clear();
    ui->leQuality->setText(tr("Searching..."));
    ui->leMethod->setText(tr("Searching..."));
    ui->teOut->setText(tr("The request is processed...\nStarting search..."));
    ui->pbSok->setEnabled(false);
    ui->actionSearch->setEnabled(false);
    ui->actionListAllEpisodes->setEnabled(false);
    QTimer::singleShot(2000, this, [this] {
        ui->pbSok->setEnabled(true);
        ui->actionSearch->setEnabled(true);
        ui->actionListAllEpisodes->setEnabled(true);
    });
    ui->teOut->setReadOnly(true);
    QString sok = ui->leSok->text();
    sok = sok.simplified();

    if(sok == "") {
        ui->teOut->setText(tr("The search field is empty!"));
        ui->leQuality->setText("");
        ui->leMethod->setText("");
    } else if((sok.left(7) != "http://") && (sok.left(8) != "https://")) {
        ui->teOut->setText(tr("Incorrect URL"));
        ui->leQuality->setText("");
        ui->leMethod->setText("");
    } else {
        address = ui->leSok->text().trimmed();
        int questionmark = address.indexOf('?');
        address = address.left(questionmark);
        // Hit

        if(ui->comboPayTV->currentIndex() == 0) {  // No Password
//            tesstsearch
//            QString test = doTestSearch(address);
//            if(test == "skit") {
//                ui->teOut->setText("ERROR");
//                return;
//            }
//            if(test != "") {
//                ui->teOut->setText(test);
//            }
            ARG << QStringLiteral(u"-A")
                << QStringLiteral(u"--get-only-episode-url") << address;
        } else { // Password
// cppcheck
//            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
//            // settings.setIniCodec("UTF-8");
            QString provider = ui->comboPayTV->currentText();
            settings.beginGroup("Provider");
            QString username = settings.value(provider + "/username").toString();
            QString password = settings.value(provider + "/password").toString();
            settings.endGroup();

            if(password == "") {
                password = secretpassword;

                if(password == "") {
                    bool ok;
                    QString newpassword = QInputDialog::getText(
                                              nullptr, provider + " " + tr("Enter your password"),
                                              tr("Spaces are not allowed. Use only the characters your "
                                                 "streaming provider approves.\nThe Password will not be "
                                                 "saved."),
                                              QLineEdit::Password, "", &ok);

                    if(newpassword.indexOf(' ') >= 0) {
                        return;
                    }

                    if(ok) {
                        secretpassword = newpassword;
                        password = newpassword;
                    }
                }
            }

// tesstsearch
//            QString test = doTestSearch(address, username, password);
//            if(test != "") {
//                ui->teOut->setText(test);
//                return;
//            }
            ARG << QStringLiteral(u"-A")
                << "--get-only-episode-url"
                << "--username=" + username << "--password=" + password << address;
        }

        CommProcess2 = new QProcess(nullptr);
        CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess2->start(svtplaydl, ARG);
        processpid = CommProcess2->processId();
        connect(CommProcess2, SIGNAL(finished(int, QProcess::ExitStatus)), this,
                SLOT(onCommProcessExit_sok(int, QProcess::ExitStatus)));
        connect(CommProcess2, &QProcess::readyReadStandardOutput, [this]() {
            QString result(CommProcess2->readAllStandardOutput());

            if(result.mid(0, 25) == "ERROR: svtplay-dl crashed") {
                ui->teOut->setTextColor(QColor("red"));
                ui->teOut->append(svtplaydl + tr(" crashed."));
                ui->teOut->append(
                    tr("Can not find any video streams, please check the address."));
                ui->teOut->setTextColor(QColor("black"));
                return;
            }

            if(result.mid(0, 34) == "Traceback (most recent call last):") {
                return;
            }

            if(result.mid(0, 6) != "ERROR:") {
                result = result.simplified();
                result.replace("Url: ", "");
                QStringList list = result.split("INFO:", Qt::SkipEmptyParts);
                list.removeDuplicates();

                foreach(QString temp, list) {
                    ui->teOut->append(temp.trimmed());
                }
            } else {
                ui->teOut->append(result);
            }
        });
    }
}
