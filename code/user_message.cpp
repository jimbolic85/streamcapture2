// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include <QNetworkReply>
#include "newprg.h"
#include "info.h"
void Newprg::userMessage()
{
//    qDebug() << "HIT";
    QUrl url(USER_MESSAGE);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
//    reply->waitForBytesWritten(1000);

    if(reply->error() != QNetworkReply::NoError) {
//        qDebug() << "ERROR 1";
        return;
    }

    QObject::connect(
        reply, &QNetworkReply::finished,
    [reply, this] {
        QByteArray *bytes = new QByteArray;
        *bytes = reply->readAll();

        if(reply->error() == QNetworkReply::ContentNotFoundError)
        {
//            qDebug() << "ERROR 2";
            return;
        }
        QString s(*bytes);
        s = s.trimmed();

        if(s.isEmpty())
        {
            return;
        }
        ui->teOut->setText(s);
//        QTextCursor cursor = ui->teOut->textCursor();
        ui->teOut->moveCursor(QTextCursor::Start);
//        qDebug() << s;
    });
}
