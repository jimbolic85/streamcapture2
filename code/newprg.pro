#  ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#
#    		streamCapture2
#    		Copyright (C) 2016 - 2022 Ingemar Ceicer
#    		https://gitlab.com/posktomten/streamcapture2/
#    		ic_streamcapture2@ceicer.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
# ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#-------------------------------------------------
#
# Project created by QtCreator 2017-05-07T13:14:14
#
#-------------------------------------------------

QT += core gui network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#CONFIG += static

TARGET = streamcapture2
TEMPLATE = app
#CONFIG += optimize_full
#CONFIG += static
#CONFIG += precompile_header

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
# DEFINES += QT_DISABLE_DEPRECATED_BEFORE
# DEFINES += QT_NO_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#CONFIG += C++17
#CONFIG += /opt/Qt5.15.0_systemfreetype_static/bin/lrelease embed_translations
#CONFIG += C:\Qt\Qt\5.15.0\mingw81_32\bin\lrelease embed_translations

#CONFIG += /opt/Qt5.15.0_systemfreetype_static/bin/lrelease embed_translation#s
# CONFIG += lrelease embed_translations

# Input

#CONFIG += embed_manifest_exe
#contains(QMAKE_HOST.version_string, x86_64):{
#    message("Host is 64bit")

#}




HEADERS += newprg.h \
           downloadlistdialog.h \
           info.h

FORMS +=   newprg.ui \
           downloadlistdialog.ui

SOURCES += \
            about.cpp \
            check_svtplaydl_for_updates.cpp \
            coppytodefaultlocation.cpp \
            download.cpp \
            download_runtime.cpp \
            downloadall.cpp \
            downloadallepisodes.cpp \
            downloadfromceicer.cpp \
            downloadlistdialog.cpp \
            downloadlistdialog_get.cpp \
            drag_and_drop.cpp \
            firstrun.cpp \
            info.cpp \
            language.cpp \
            listallepisodes.cpp \
            newprg.cpp \
            main.cpp \
            nfo.cpp \
            offline_installer.cpp \
            paytv_create.cpp \
            paytv_edit.cpp \
            qsystemtrayicon.cpp \
            save.cpp \
            screenshot.cpp \
            setgetconfig.cpp \
            shortcuts.cpp \
            sok.cpp \
            st_create.cpp \
            st_edit.cpp \
            test_translation.cpp \
            user_message.cpp \
            zoom.cpp


RESOURCES += myres.qrc \
             fonts_forall.qrc

RC_FILE = myapp.rc

TRANSLATIONS += i18n/_streamcapture2_template_xx_XX.ts \
                i18n/_streamcapture2_sv_SE.ts \
                i18n/_streamcapture2_de_DE.ts \
                i18n/_streamcapture2_it_IT.ts

CODECFORSRC = UTF-8
#UI_DIR = ../code
win32:RC_ICONS += images/icon.ico

equals(QT_MAJOR_VERSION, 5) {
DESTDIR="../build-executable5"
unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lupdateappimage # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lupdateappimaged # Debug

win32: CONFIG (release, debug|release): LIBS += -L../lib5/ -ldownload_install # Release
else: win32: CONFIG (debug, debug|release): LIBS += -L../lib5/ -ldownload_installd # Debug

CONFIG (release, debug|release): LIBS += -L../lib5/ -lcheckupdate  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcheckupdated # Debug
CONFIG (release, debug|release): LIBS += -L../lib5/ -lselectfont  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lselectfontd # Debug
CONFIG (release, debug|release): LIBS += -L../lib5/ -ldownloadunpack # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -ldownloadunpackd # Debug
#LIBS += -L../lib_ssl/ -lcrypto
#LIBS += -L../lib_ssl/ -lssl
}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR="../build-executable6"
#DESTDIR="C:\Users\posktomten\PROGRAMMERING\streamcapture2\NSIS\nsisbuild"
unix: CONFIG (release, debug|release): LIBS += -L../lib6/ -lupdateappimage # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib6/ -lupdateappimaged # Debug

win32: CONFIG (release, debug|release): LIBS += -L../lib6/ -ldownload_install # Release
else: win32: CONFIG (debug, debug|release): LIBS += -L../lib6/ -ldownload_installd # Debug

CONFIG (release, debug|release): LIBS += -L../lib6/ -lcheckupdate  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -lcheckupdated # Debug
CONFIG (release, debug|release): LIBS += -L../lib6/ -lselectfont  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -lselectfontd # Debug
CONFIG (release, debug|release): LIBS += -L../lib6/ -ldownloadunpack # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -ldownloadunpackd # Debug

}
equals(QT_MAJOR_VERSION, 5) {
win32:INCLUDEPATH += "../include_Qt5_win"
win32:DEPENDPATH += "../include_Qt5_win"

unix:INCLUDEPATH += "../include_Qt5_lin"
unix:DEPENDPATH += "../include_Qt5_lin"
}
equals(QT_MAJOR_VERSION, 6) {
win32:INCLUDEPATH += "../include_Qt6_win"
win32:DEPENDPATH += "../include_Qt6_win"

unix:INCLUDEPATH += "../include_Qt6_lin"
unix:DEPENDPATH += "../include_Qt6_lin"
}

message (----------------------------------------)
message (OS: $$QMAKE_HOST.os)
#message (Arch: $$QMAKE_HOST.arch)
#message (Cpu count: $$QMAKE_HOST.cpu_count)
#message (Name: $$QMAKE_HOST.name)
#message (Version: $$QMAKE_HOST.version)
#message (Version string: $$QMAKE_HOST.version_string)
compiler=$$basename(QMAKESPEC)
message(compiler: $$compiler)
message (QMAKE_QMAKE: $$QMAKE_QMAKE)
#message (QT_VERSION: $$QT_VERSION)
message(_PRO_FILE_PWD_: $$_PRO_FILE_PWD_)
message(TEMPLATE: $$TEMPLATE)
message(TARGET: $$TARGET)
message(DESTDIR: $$DESTDIR)
#message(HEADERS $$HEADERS)
#message(SOURCES: $$SOURCES)
#message(INCLUDEPATH $$INCLUDEPATH)
message(LIBS: $$LIBS)

