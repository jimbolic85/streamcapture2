// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


#include "newprg.h"
#include "info.h"
void Info::getSystem(int x, int y)
{
//    const QString ITALIAN_TRANSLATER = tr("bovirus");
#ifdef Q_OS_WINDOWS
    QIcon icon(":/images/icon.ico");
#endif
#ifdef Q_OS_LINUX
    QIcon icon(":/images/appicon.png");
#endif
    this->setWindowIcon(icon);
//    QPixmap pixmap(":/images/appiconsmal.png");
    const QString ffmpeg_homepage =
        FFMPEG_HOMEPAGE const QString svtplay_dl_homepage =
            SVTPLAYDL_HOMEPAGE const QString license =
                DISPLAY_NAME + tr(" is free software, license ") +
                " GPL version " + LICENCE_VERSION + ".";
    const QString syfte =
        tr("A graphical shell for ") +
        "<a style=\"text-decoration:none;\" href='" + svtplay_dl_homepage +
        "'>svtplay-dl</a>" + tr(" and ") +
        "<a style=\"text-decoration:none;\" href='" + ffmpeg_homepage +
        "'>FFmpeg</a>.<br>" +
        tr(" streamCapture2 handles downloads of video streams.<br>");
    const QString translator = tr("Many thanks to ") + " " + ITALIAN_TRANSLATER +
                               tr(" for the Italian translation. And for many good ideas that have made the program better.") + "<br>";
    /*  */
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString sp = settings.value("language", "").toString();
    settings.endGroup();
    QString applicationHomepage;

    if(sp == "sv_SE") {
        applicationHomepage = APPLICATION_HOMEPAGE;
    } else {
        applicationHomepage = APPLICATION_HOMEPAGE_ENG;
    }

    /* */
// STOP EDIT
    QString v = "";
//    v += QSysInfo::kernelType() + ' ' + QSysInfo::prettyProductName() + " (" +
//         QSysInfo::currentCpuArchitecture() + ").<br>";
    v += tr("Created:") + " " + BUILD_DATE_TIME "<br>" +
//         tr("by a computer with") + " " + QSysInfo::buildAbi();
         tr("Compiled on:") + " " + COMPILEDON;
    v += "<br>";
    // write GCC version
    v += tr("Qt version:") + " " + QT_VERSION_STR + "<br>";
    QString temp;
#if defined _MSC_VER || __GNUC__ || defined __clang__ || defined __ICC ||      \
    defined __clang__
#if defined __GNUC__ || defined __clang__ || defined __ICC || defined __clang__
#if defined __GNUC__ && !defined __clang__
#if defined __GNUC__ && !defined __MINGW32__
#define COMPILER "GCC"
#endif
#ifdef __MINGW32__
    QString COMPILER = "MinGW (GCC " + tr("for") + " Windows) ";
#endif
    temp = (QString(tr("Compiler:") + " %1 %2.%3.%4")
            .arg(COMPILER)
            .arg(__GNUC__)
            .arg(__GNUC_MINOR__)
            .arg(__GNUC_PATCHLEVEL__));
#endif
#ifdef __ICC
#define INTEL "Intel icl"
    temp = (QString(tr("") + " %1 %2 Build date: %3 Update: %4%5")
            .arg(INTEL)
            .arg(__INTEL_COMPILER)
            .arg(__INTEL_COMPILER_BUILD_DATE)
            .arg(__INTEL_COMPILER_UPDATE));
#endif
    v += temp;
#if defined __clang__
    v += (QString(tr("Compiler:") + " %1 %2.%3.%4")
          .arg("Clang")
          .arg(__clang_major__)
          .arg(__clang_minor__));
#endif
#endif
#ifdef _MSC_VER
#ifdef __clang__
    v += " " + tr("compatible with") + " ";
#endif
#endif
// MSVC version
#ifdef _MSC_VER
#define COMPILER ""
//    QString tmp = QString::number(_MSC_VER);
    //    qDebug() << _MSC_VER;
    QString version;
    QString full_version = tr("Full version number:") + " " + QString::number(_MSC_FULL_VER);
    version = tr("Micrsoft Visual C++ version ") + QString::number(_MSC_VER);
    version += tr("<br>Full version number: ") + QString::number(_MSC_FULL_VER);
#ifndef __clang__
    v += (QString(tr("Compiler:") + " %1 %2")
          .arg(COMPILER)
          .arg(version));
#endif
#ifdef __clang__
    v += (QString(" %1 %2")
          .arg(COMPILER)
          .arg(version));
#endif
#endif
#else
    v += tr("Unknown compiler.");
    v += "<br>";
#endif
    v += "<br>";
    QString CURRENT_YEAR_S = CURRENT_YEAR;
    CURRENT_YEAR_S = CURRENT_YEAR_S.right(4);
#if __cplusplus==201703L
    QString compiled = "C++17";
#elif __cplusplus==201402L
    QString compiled = "C++14";
#elif __cplusplus==201103L
    QString compiled = "C++11";
#else
    QString compiled = "C++";
#endif
    v += tr("Revision:") + " " + compiled;
    /*
    + "<br>"<br>";
    float f_glibc_minor = __GLIBC_MINOR__;
    float glibc = __GLIBC__ + f_glibc_minor / 100;
    v += tr("You are using GLIBC version") + " " + QString::number(glibc) + ".";
    */
    const QString auther =
        "<h4 style=\"color:green;\">Copyright &copy; " START_YEAR " - " +
        CURRENT_YEAR_S +
        " " PROGRAMMER_NAME "</h4><br><a style=\"text-decoration:none;\" href='" +
        applicationHomepage + "'>" + tr("Home page") +
        "</a> | <a style=\"text-decoration:none;\" href='" SOURCEKODE "'>" +
        tr("Source code") +
        "</a> | <a style=\"text-decoration:none;\" href='" WIKI "'>" +
        tr("Wiki") +
        "</a><br><a style=\"text-decoration:none;\"  "
        "href=\"mailto:" PROGRAMMER_EMAIL "\">" PROGRAMMER_EMAIL "</a>  <br>" +
        tr("Phone: ") + PROGRAMMER_PHONE "<br><br>";
    QMessageBox msgBox(this);
    // context menu
//    msgBox.setIconPixmap(pixmap);
    msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
    msgBox.setText(
        "<h1 style=\"color:green;\">" DISPLAY_NAME " " VERSION
        "</h1>" +
        auther + translator + "<br>" + syfte + "<br>" +
//        tr("This program uses Qt version ") + QT_VERSION_STR + "<br>" +
        v + "<br><br>" + license + "<br><br>" +
//        tr(" running on ") + v + "<br><br>" + license + "<br><br>" +
        " streamCapture2 " + tr("is located in") + " \"" + QDir::toNativeSeparators(QApplication::applicationDirPath()) + "\"");
    msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
    msgBox.move(x + 25, y + 25);
    msgBox.exec() ;
}
