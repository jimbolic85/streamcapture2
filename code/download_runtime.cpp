// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "newprg.h"

#ifdef Q_OS_WIN
void Newprg::downloadRuntime()
{
    QMessageBox msgBox;
    msgBox.setIconPixmap(QPixmap(":/images/redistuable.png"));
    msgBox.setWindowTitle(QStringLiteral(u"Microsoft Visual C++ Redistributable"));
    msgBox.setText(tr("Only if svtplay-dl does not work do you need to install\n\"Microsoft Visual C++ Redistributable\".\nDownload and double-click to install."));
    QPushButton *btnDownload = msgBox.addButton(tr("Download"), QMessageBox::AcceptRole);
    QPushButton *btnCancel = msgBox.addButton(tr("Cancel"), QMessageBox::RejectRole);
    msgBox.setDefaultButton(btnCancel);
    msgBox.exec();

    if(msgBox.clickedButton() == btnDownload) {
        QString path;

        if(QSysInfo::currentCpuArchitecture() == "x86_64") {
            path = "https://bin.ceicer.com/streamcapture2/bin/VC_redist.x64.exe";
        } else {
            path = "https://bin.ceicer.com/streamcapture2/bin/VC_redist.x86.exe";
        }

        QDesktopServices::openUrl(path);
//        if(!QDesktopServices::openUrl(path)) {
//            QMessageBox msgBox1;
//            msgBox1.setIcon(QMessageBox::Critical);
//            msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
//            msgBox1.setText(tr("An unexpected error has occurred. You can download from the Microsoft website.\nSearch for \"Microsoft Visual C ++ Redistributable\"."));
//            msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
//            msgBox1.exec();
//        }
    }
}

#endif
