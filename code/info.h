// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#if !defined(INFO_H)
#define INFO_H

#include <QWidget>

// EDIT All
#define VERSION "2.8.3"
#define DISPLAY_VERSION "2.8.3 BETA"
#define FFMPEG

#if defined(Q_OS_LINUX)

// EDIT Linux
#define EXPERIMENTAL

#elif defined(Q_OS_WINDOWS)

//EDIT Windows
#define PORTABLE
//#define OFFLINE_INSTALLER // Windows

#endif // Q_OS

// END EDIT

#define SVTPLAYDL_ISSUES "https://github.com/spaam/svtplay-dl/issues"
#define DOWNLOAD_PATH                                                          \
    "https://gitlab.com/posktomten/streamcapture2/-/wikis/Home"
#define MANUAL_PATH "https://bin.ceicer.com/streamcapture2/help-current/index_"
//#define MANUAL_PATH "https://bin.ceicer.com/streamcapture2/help-current2/index_"
#define DISPLAY_NAME "streamCapture2"
#define EXECUTABLE_NAME "streamcapture2"
#define LIBRARY_NAME "downloadunpack"
#define BUILD_DATE_TIME __DATE__ " " __TIME__
#define START_YEAR "2016"
#define CURRENT_YEAR __DATE__
#define LICENCE_VERSION "3"
#define PROGRAMMER_EMAIL "ic_streamcapture2@ceicer.com"
#define PROGRAMMER_NAME "Ingemar Ceicer"
#define PROGRAMMER_PHONE "+46706361747"
#define APPLICATION_HOMEPAGE                                                   \
    "https://gitlab.com/posktomten/streamcapture2/wikis/Home"
#define APPLICATION_HOMEPAGE_ENG                                               \
    "https://gitlab.com/posktomten/streamcapture2/wikis/Home"
#define SOURCEKODE "https://gitlab.com/posktomten/streamcapture2"
#define WIKI "https://gitlab.com/posktomten/streamcapture2/wikis/Home"
#define FFMPEG_HOMEPAGE "https://www.ffmpeg.org/";
#define SVTPLAYDL_HOMEPAGE "https://svtplay-dl.se/";
#define ITALIAN_TRANSLATER "bovirus"
#if defined(EXPERIMENTAL)
#define EXPERIMENTAL_VERSION_LINK "\"https://bin.ceicer.com/streamcapture2/bin\""
#endif // EXPERIMENTAL
#define SIZE_OF_RECENT 50
#define RED 218
#define GREEN 255
#define BLUE 221
#define TABSTOPWIDTH 30

#if defined(Q_OS_LINUX) // LINUX

#define FONT 12

#define APPIMAGE // For "Create Icon"

#if (__GLIBC_MINOR__ > 27)
#define HIGH_GLIBC
#endif

#define VERSION_PATH "http://bin.ceicer.com/streamcapture2/version_linux.txt"
#define USER_MESSAGE "https://bin.ceicer.com/streamcapture2/user_linux.txt"

#define CHECK_LATEST_STABLE "http://bin.ceicer.com/svtplay-dl/Linux/version_stable.txt"
#define CHECK_LATEST "http://bin.ceicer.com/svtplay-dl/Linux/version.txt"

#if defined(Q_PROCESSOR_X86_64) // 64 bit

#if !defined(HIGH_GLIBC)

#define COMPILEDON "Lubuntu 18.04.6 LTS 64-bit, GLIBC 2.27"

#define ARG1 "streamcapture2-ffmpeg-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/streamcapture2-ffmpeg-x86_64.AppImage.zsync"

#elif defined(HIGH_GLIBC)

#if (__GLIBC_MINOR__ == 31)
#define COMPILEDON "Ubuntu 20.04.5 LTS 64-bit, GLIBC 2.31"
#elif (__GLIBC_MINOR__ == 35)
#define COMPILEDON "Ubuntu 22.04.1 LTS 64-bit, GLIBC 2.35"
#endif // __GLIBC_MINOR__
#if defined(FFMPEG)
#define ARG1 "streamcapture2-ffmpeg-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/HIGH_GLIBC/streamcapture2-ffmpeg-x86_64.AppImage.zsync"
#elif !defined(FFMPEG)
#define ARG1 "streamcapture2-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/HIGH_GLIBC/streamcapture2-x86_64.AppImage.zsync"
#endif // FFMPEG

#endif // HIGH_GLIBC

#elif defined(Q_PROCESSOR_X86_32) // 32 bit

#define COMPILEDON "Lubuntu 18.04.6 LTS 32-bit, GLIBC 2.27"

#if defined(FFMPEG)
#define ARG1 "streamcapture2-ffmpeg-i386.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/streamcapture2-ffmpeg-i386.AppImage.zsync"
#elif !defined(FFMPEG)
#define ARG1 "streamcapture2-i386.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/streamcapture2-i386.AppImage.zsync"
#endif

#endif // Q_PROCESSOR_

#elif defined(Q_OS_WINDOWS) // WINDOWS

#define FONT 11
#if defined(OFFLINE_INSTALLER)
#define PATH "https://bin.ceicer.com/streamcapture2/bin/"
#define FILENAME "streamCapture2_setup.exe"
#endif // OFFLINE_INSTALLER
#define VERSION_PATH "https://bin.ceicer.com/streamcapture2/version_windows.txt"
#define USER_MESSAGE "https://bin.ceicer.com/streamcapture2/user_windows.txt"

#if defined(Q_PROCESSOR_X86_64) // Windows 64 bit
#define CHECK_LATEST_STABLE "https://bin.ceicer.com/svtplay-dl/Windows/64bit/version_stable.txt"
#define CHECK_LATEST "https://bin.ceicer.com/svtplay-dl/Windows/64bit/version.txt"

#define COMPILEDON "Microsoft Windows 10 Pro<br>Version: 21H2<br>OS build: 19044.1682<br>System type: 64-bit operating system, x64-based processor"

#elif defined (Q_PROCESSOR_X86_32) // Windows 32 bit
#define CHECK_LATEST_STABLE "https://bin.ceicer.com/svtplay-dl/Windows/32bit/version_stable.txt"
#define CHECK_LATEST "https://bin.ceicer.com/svtplay-dl/Windows/32bit/version.txt"

#define COMPILEDON "Microsoft Windows 10 Pro<br>Version: 21H2<br>OS build: 19044.1682<br>System type: 32-bit operating system, x86-based processor"

#endif // Q_PROCESSOR

#endif // Q_OS

class Info : public QWidget
{
    Q_OBJECT
public:
    void getSystem(int x, int y);
};

#endif // INFO_H
