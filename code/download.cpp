// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include "info.h"
/* DOWNLOAD */
void Newprg::download()
{
    getSvtplaydlVersion(&svtplaydl);

    if(!fileExists(svtplaydl)) {
        ui->teOut->setTextColor(QColor("red"));
        ui->teOut->setText(svtplaydl +
                           tr(" cannot be found or is not an executable program."));
        ui->teOut->append(tr("Please click on \"Tools\" and \"Download svtplay-dl...\""));
        ui->teOut->setTextColor(QColor("black"));
        return;
    }

    QString preferred, quality, amount, resolution;
    ui->teOut->clear();
    QStringList ARG;
    ARG << "--verbose";
    ARG << "--force";

    switch(ui->comboResolution->currentIndex()) {
        case 1:
            ARG << "--resolution" << "480";
            resolution = "480p";
            break;

        case 2:
            ARG << "--resolution" << "720";
            resolution = "720p";
            break;

        case 3:
            ARG << "--resolution" << "1080";
            resolution = "1080p";
    }

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Stcookies");
    QString stcookie = settings.value("stcookie", "").toString();
    settings.endGroup();

    if(!stcookie.isEmpty()) {
        ARG << "--cookies"  << "st=" + stcookie;
    }

    ui->teOut->setTextColor(QColor("black"));
    avbrutet = false;
    ui->teOut->setReadOnly(true);
    QString explanation =  tr("The request is processed...") +  "\n" + tr("Preparing to download...");
    /*  */
    settings.beginGroup("Path");
    QString savepath =
        settings.value("savepath").toString();
    QString copypath = settings.value("copypath").toString();
    settings.endGroup();
    settings.beginGroup("Settings");
    bool selectfilename = settings.value("selectfilename", "false").toBool();
    settings.endGroup();
    QString save_path;
    QString save_path2;
    QString folderaddress;

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        save_path = savepath;
    } else {
        save_path = save(tr("Download streaming media to folder"));
    }

//#ifdef Q_OS_WINDOWS

//        if(save_path == QStandardPaths::writableLocation(QStandardPaths::MoviesLocation)) {
//            ui->teOut->setTextColor(QColor("red"));
//            ui->teOut->append(tr("You are using the Windows operating system. Unfortunately, downloads to") + " \"" + save_path + "\"  " +
//                              tr("often fail. If the download fails, select another location to download to."));
//            ui->teOut->setTextColor(QColor("black"));
//        }

//#endif

    if(save_path != "nothing") {
        QString kopierastill;
        amount = ui->comboAmount->currentText();

        if(amount.at(0) == '-') {
            amount = "unknown";
        }

        if(ui->comboBox->currentText().at(0) == '-') {
            preferred = "unknown";
            quality = "unknown";
        } else {
            preferred = ui->leMethod->text();
            preferred = preferred.trimmed();
            preferred = preferred.toLower();
            quality = ui->leQuality->text();
            quality = quality.trimmed();
        }

        /* CREATE FOLDER */
        folderaddress = address;

        if(folderaddress.right(1) == '/') {
            int storlek = folderaddress.size();
            folderaddress.remove(storlek - 1, 1);
        }

        int hittat = folderaddress.lastIndexOf('/');
        int hittat2 = folderaddress.indexOf('?');
        folderaddress = folderaddress.mid(hittat, hittat2 - hittat);
//            save_path2 = QLatin1String("");

        if(ui->actionCreateFolder->isChecked()) {
            switch(ui->comboResolution->currentIndex()) {
                case 1:
                    resolution = "480p";
                    break;

                case 2:
                    resolution = "720p";
                    break;

                case 3:
                    resolution = "1080p";
                    break;

                default:
                    resolution = "unknown";
            }

            QDir dir(save_path + folderaddress + "/" + preferred + '_' + quality + '_' + amount + '_' + resolution);

            if(!dir.exists()) {
                dir.mkpath(".");
            }

            save_path2 = dir.path();
        } else {
            save_path2 = save_path;
        }

#ifdef Q_OS_WIN
        // NTFS
        qt_ntfs_permission_lookup++; // turn checking on
#endif
        QFileInfo fi(save_path2);

        if(!fi.isWritable()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("You do not have the right to save to the "
                              "folder.\nDownload is interrupted."));
//                msgBox.addButton(QMessageBox::Ok);
//                msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            ui->teOut->clear();
#ifdef Q_OS_WIN
            qt_ntfs_permission_lookup--; // turn checking of
#endif
            return;
        }

#ifdef Q_OS_WIN
        qt_ntfs_permission_lookup--; // turn checking of
#endif
        ui->teOut->append(tr("The video stream is saved in ") + "\"" + QDir::toNativeSeparators(save_path2 + "\""));

        if(ui->actionDownloadToDefaultLocation->isChecked()) {
            const QFileInfo downloadlocation(savepath);

            if(!downloadlocation.exists()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("The default folder for downloading video streams cannot be "
                                  "found.\nDownload is interrupted."));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                ui->teOut->clear();
                return;
            }

#ifdef Q_OSS_WIN
            qt_ntfs_permission_lookup++; // turn checking on
#endif

            if(!downloadlocation.isWritable()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("You do not have the right to save to the "
                                  "default folder.\nDownload is interrupted."));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                ui->teOut->clear();
#ifdef Q_OSS_WIN
                qt_ntfs_permission_lookup--; // turn checking of
#endif
                return;
            }
        } else {
#ifdef Q_OSS_WIN
            qt_ntfs_permission_lookup--; // turn checking of
#endif
            const QFileInfo downloadlocation(save_path);

            if(!downloadlocation.exists()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("The folder for downloading video streams cannot be "
                                  "found.\nDownload is interrupted."));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                ui->teOut->clear();
                return;
            }

            if(!downloadlocation.isWritable()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("You do not have the right to save to the "
                                  "folder.\nDownload is interrupted."));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                ui->teOut->clear();
                return;
            }
        }

        kopierastill = tr("Selected folder to copy to is ") + "\"" +
                       QDir::toNativeSeparators(copypath + "\"");
//        kopierastill += "<br>" + tr("The copy was successful.");
        QProcess *CommProcess2 = new QProcess(nullptr);
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        const QString path = QCoreApplication::applicationDirPath();
        const QString oldpath = env.value("PATH");
#ifdef Q_OS_LINUX
        env.insert("PATH", path + ":" + oldpath); // Add an environment variable
#endif
#ifdef Q_OS_WIN                               // QT5
        env.insert("PATH", path + ";" + oldpath); // Add an environment variable
#endif
        CommProcess2->setProcessEnvironment(env);
        CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess2->setWorkingDirectory(save_path2);

        if(ui->chbSubtitle->isChecked()) {
            ARG << "--subtitle";
        }

        if(ui->comboBox->currentText().at(0) != '-') {
            if(amount != "unknown") {
                ARG << "--quality" << quality << "--preferred" << preferred << "--flexible-quality" << amount;
            } else {
                ARG << "--quality" << quality << "--preferred" << preferred;
            }
        }

        if(ui->comboPayTV->currentIndex() != 0) {  // Password
            {
                /* */
// cppcheck
//                    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
//                                       DISPLAY_NAME, EXECUTABLE_NAME);
//                    // settings.setIniCodec("UTF-8");
                QString provider = ui->comboPayTV->currentText();
                settings.beginGroup("Provider");
                QString username = settings.value(provider + "/username").toString();
                QString password = settings.value(provider + "/password").toString();
                settings.endGroup();

                if(password == "") {
                    password = secretpassword;

                    if(password == "") {
                        bool ok;
                        QInputDialog inputdialog;
                        inputdialog.setOkButtonText(tr("Ok"));
                        inputdialog.setCancelButtonText(tr("Cancel"));
                        inputdialog.setWindowTitle(tr("Enter your password"));
                        inputdialog.setInputMode(QInputDialog::TextInput);
                        inputdialog.setTextEchoMode(QLineEdit::Password);
                        inputdialog.setLabelText(tr("Spaces are not allowed. Use only the characters\nyour streaming provider approves."));
                        ok = inputdialog.exec();
                        QString newpassword = inputdialog.textValue();

                        if(newpassword.indexOf(' ') >= 0) {
                            return;
                        }

                        if(ok) {
                            secretpassword = newpassword;
                        }
                    }
                }

                /* */
                ARG << "--username" << username << "--password" << password;
            }
        }

        /* */
        auto *filnamn = new QString;
        ui->teOut->append(explanation);
        ui->teOut->append(tr("Starts downloading: ") + "\"" + address + "\"");
//        connect(CommProcess2, SIGNAL(started()), this, SLOT(onCommProcess2Start()));
        connect(CommProcess2, &QProcess::readyReadStandardOutput,
        [this, filnamn, CommProcess2]() {
            QString result(CommProcess2->readAllStandardOutput());

            if(result.trimmed().contains("Can't find any streams with that video resolution")) {
                ui->teOut->setTextColor(QColor("red"));
                ui->teOut->append(tr("Unable to find any streams with the selected video resolution."));
                ui->teOut->setTextColor(QColor("black"));
            }

            if(result.contains(".mp4")) {
                int stopp = result.indexOf(".mp4") + 4;
                int start = result.lastIndexOf(" ", stopp) + 1;
                int langd = result.size();
                int storlek = langd - start - stopp;
                QString tmp = result.mid(start, storlek);
                tmp = tmp.trimmed();
                *filnamn = tmp;
            }

            if(ui->actionShowMore->isChecked()) {
                // progressbar
                if(ui->progressBar->value() >= 100) {
                    ui->progressBar->setValue(0);
                }

                ui->progressBar->setValue(ui->progressBar->value() + 1);
                ui->teOut->setTextColor(QColor("purple"));
                ui->teOut->append(result);
                ui->teOut->setTextColor(QColor("black"));
            } else {
                // progressbar
                if(ui->progressBar->value() >= 100) {
                    ui->progressBar->setValue(0);
                }

                if(result.trimmed().contains("Merge audio and video")) {
                    ui->teOut->append(tr("Merge audio and video..."));
                }

                if(result.trimmed().contains("removing old files")) {
                    ui->teOut->append(tr("Removing old files, if there are any..."));
                }

                ui->progressBar->setValue(ui->progressBar->value() + 1);
            }
        });
        connect(
            CommProcess2,
            QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        [ this, filnamn, preferred, quality, amount, resolution, kopierastill, folderaddress, save_path2 ](int exitCode, QProcess::ExitStatus exitStatus) {
            if(!avbrutet) {
                if(filnamn->isEmpty()) {
                    QString *x_filnamn = new QString;
                    ui->teOut->setTextColor(QColor("red"));
                    *x_filnamn = *filnamn;
                    x_filnamn->replace("\\\\", "\\", Qt::CaseSensitivity::CaseInsensitive);
                    ui->teOut->append(tr("The download failed.") + "\"" + preferred + ", " + quality + ", " + amount + ", " + resolution + "\"");

                    if(!ui->actionNotifications->isChecked()) {
                        showNotification(tr("The download failed.") + "\"" + preferred + ", " + quality + ", " + amount + ", " + resolution + "\"", 3);
                    }

                    ui->teOut->setTextColor(QColor("black"));
                } else {
                    ui->teOut->setTextColor(QColor("darkBlue"));
                    QString *x_filnamn = new QString;
                    *x_filnamn = *filnamn;
                    x_filnamn->replace("\\\\", "\\", Qt::CaseSensitivity::CaseInsensitive);
                    ui->teOut->append(tr("Download succeeded") + " \"" + *x_filnamn + "\" " + preferred + ", " + quality + ", " + amount + "," + resolution);
                    ui->teOut->setTextColor(QColor("black"));
                    ui->teOut->setTextColor(QColor("darkBlue"));
                    ui->progressBar->setValue(10);
                    QString undertexten = *filnamn;
                    int langd = undertexten.size();
                    undertexten = undertexten.replace(langd - 3, 3, "srt");

                    if(QFile::exists(save_path2 + "/" + undertexten)) {
                        ui->teOut->append(tr("Download succeeded") + " \"" + undertexten + "\"");
                    }

                    ui->teOut->setTextColor(QColor("black"));
// copy

                    if(ui->actionCopyToDefaultLocation->isChecked()) {
                        QString *copyto = new QString;

                        if(ui->actionCreateFolder->isChecked()) {
                            QString resolution;

                            switch(ui->comboResolution->currentIndex()) {
                                case 1:
                                    resolution = "480p";
                                    break;

                                case 2:
                                    resolution = "720p";
                                    break;

                                case 3:
                                    resolution = "1080p";
                                    break;

                                default:
                                    resolution = "unknown";
                            }

                            *copyto = folderaddress + "/" + preferred + '_' + quality + '_' + amount + '_' + resolution;
                            copyToDefaultLocation(*filnamn, &save_path2, copyto);
                        } else {
                            copyto->clear();
                            copyToDefaultLocation(*filnamn, &save_path2, copyto);
                        }
                    }

                    ui->teOut->append(tr("Download completed"));
                    ui->progressBar->setValue(0);

                    if(!ui->actionNotifications->isChecked()) {
                        // showNotification
                        showNotification(tr("Download completed"), 1);
                    }

                    ui->teOut->setTextColor(QColor("black"));

                    if(ui->actionCopyToDefaultLocation->isChecked()) {
                        ui->teOut->append(kopierastill);
                    }

                    // hit
                }
            } else {
                ui->teOut->setTextColor(QColor("red"));
                ui->teOut->append(tr("The download failed "));
                ui->teOut->setTextColor(QColor("black"));

                if(!ui->actionNotifications->isChecked()) {
                    // showNotification
                    showNotification(tr("The download failed "), 3);
                }

                avbrutet = false;
            }

            statusExit(exitStatus, exitCode);
            /* ... */
        });

        if(selectfilename) {
            // SELECT FILE NAME
            QInputDialog selectFilename;
            selectFilename.setSizeGripEnabled(true);
            selectFilename.setCancelButtonText(tr("Cancel"));
            selectFilename.setOkButtonText(tr("Ok"));
            selectFilename.setWindowTitle(tr("Save as..."));
            bool ok;
            QString filename = folderaddress.remove("/");
            selectFilename.setInputMode(QInputDialog::TextInput);
            selectFilename.setLabelText(tr("Select file name"));
            selectFilename.setTextValue(filename);
            selectFilename.setModal(true);
            ok = selectFilename.exec();

            if(ok) {
                folderaddress = "/" + selectFilename.textValue().trimmed();
            }
        }

        // END SELECT FILE NAME
        // ipac
//        QString tmp = save_path2  + folderaddress + "-x-";
        QString tmp = save_path2  + folderaddress;
        ARG << "--output" << tmp;
        ARG << address;
//        CommProcess2->setWorkingDirectory(save_path2);
//        qDebug() << ARG;
        CommProcess2->start(svtplaydl, ARG);
//        CommProcess2->start(svtplaydl, QStringList() << "--help");
        ARG.clear();
    } else {
        ui->teOut->setText(tr("No folder is selected"));
    }

//    save_path2.clear();
//    folderaddress.clear();
//    CommProcess2->deleteLater();
}

/* END DOWNLOAD */
void Newprg::initSok()
{
    if(!ui->actionSvtplayDlSystem->isChecked()) {
        if(!fileExists(svtplaydl)) {
            return;
        }
    }

    ui->teOut->setTextColor("black");
    ui->teOut->setReadOnly(true);
    ui->comboBox->clear();
    ui->leQuality->setText(tr("Searching..."));
    ui->leMethod->setText(tr("Searching..."));
    ui->teOut->setText(tr("The request is processed...\nStarting search..."));
}
