/********************************************************************************
** Form generated from reading UI file 'downloadunpack.ui'
**
** Created by: Qt User Interface Compiler version 6.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DOWNLOADUNPACK_H
#define UI_DOWNLOADUNPACK_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DownloadUnpack
{
public:
    QAction *actionSearch;
    QAction *actionDownload;
    QAction *actionTryToUnzip;
    QAction *actionExit;
    QAction *actionEnglish;
    QAction *actionSwedish;
    QAction *actionPreCheckLanguagefile;
    QAction *actionItalien;
    QAction *actionSourceCode;
    QAction *actionBinaryFiles;
    QAction *actionDownloadLatest;
    QAction *actionZoomIn;
    QAction *actionZoomDefault;
    QAction *actionZoomOut;
    QAction *actionShowToolbar;
    QAction *actionDownloadToBleedingedge;
    QAction *actionDownloadToStable;
    QAction *actionStable;
    QAction *actionBleedingedge;
    QAction *actionSearchLatestStable;
    QAction *actionDownloadLatestStable;
    QAction *actionDownloadLatestStableToDirectoryStable;
    QAction *actionDownloadLatestBleedingedgeToDirectoryBleedingedge;
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QPlainTextEdit *pteDisplay;
    QHBoxLayout *horizontalLayout_2;
    QComboBox *comboOS;
    QLabel *lblAntalFiler;
    QSpacerItem *horizontalSpacer_2;
    QProgressBar *progressBar;
    QHBoxLayout *horizontalLayout;
    QComboBox *comboSvtplaydl;
    QSpacerItem *horizontalSpacer;
    QFormLayout *formLayout;
    QMenuBar *menubar;
    QMenu *menuFile;
    QMenu *menuVisit;
    QMenu *menuZoom;
    QMenu *menuSettings;
    QMenu *menuChooseLocation;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *DownloadUnpack)
    {
        if (DownloadUnpack->objectName().isEmpty())
            DownloadUnpack->setObjectName(QString::fromUtf8("DownloadUnpack"));
        DownloadUnpack->resize(739, 415);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DownloadUnpack->sizePolicy().hasHeightForWidth());
        DownloadUnpack->setSizePolicy(sizePolicy);
        DownloadUnpack->setMinimumSize(QSize(0, 0));
        DownloadUnpack->setBaseSize(QSize(600, 448));
        QFont font;
        font.setPointSize(10);
        DownloadUnpack->setFont(font);
        DownloadUnpack->setToolTipDuration(1);
#if QT_CONFIG(whatsthis)
        DownloadUnpack->setWhatsThis(QString::fromUtf8(""));
#endif // QT_CONFIG(whatsthis)
        DownloadUnpack->setAutoFillBackground(false);
        DownloadUnpack->setDocumentMode(false);
        actionSearch = new QAction(DownloadUnpack);
        actionSearch->setObjectName(QString::fromUtf8("actionSearch"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/search_bleedingedge.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSearch->setIcon(icon);
        actionDownload = new QAction(DownloadUnpack);
        actionDownload->setObjectName(QString::fromUtf8("actionDownload"));
        actionDownload->setEnabled(true);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/downloadny.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDownload->setIcon(icon1);
        actionTryToUnzip = new QAction(DownloadUnpack);
        actionTryToUnzip->setObjectName(QString::fromUtf8("actionTryToUnzip"));
        actionTryToUnzip->setCheckable(true);
        actionTryToUnzip->setChecked(true);
        actionExit = new QAction(DownloadUnpack);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/exitny.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionExit->setIcon(icon2);
        actionEnglish = new QAction(DownloadUnpack);
        actionEnglish->setObjectName(QString::fromUtf8("actionEnglish"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/english.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEnglish->setIcon(icon3);
        actionSwedish = new QAction(DownloadUnpack);
        actionSwedish->setObjectName(QString::fromUtf8("actionSwedish"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/swedish.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSwedish->setIcon(icon4);
        actionPreCheckLanguagefile = new QAction(DownloadUnpack);
        actionPreCheckLanguagefile->setObjectName(QString::fromUtf8("actionPreCheckLanguagefile"));
        actionItalien = new QAction(DownloadUnpack);
        actionItalien->setObjectName(QString::fromUtf8("actionItalien"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/italian.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionItalien->setIcon(icon5);
        actionSourceCode = new QAction(DownloadUnpack);
        actionSourceCode->setObjectName(QString::fromUtf8("actionSourceCode"));
#if QT_CONFIG(statustip)
        actionSourceCode->setStatusTip(QString::fromUtf8("https://github.com/spaam/svtplay-dl"));
#endif // QT_CONFIG(statustip)
        actionBinaryFiles = new QAction(DownloadUnpack);
        actionBinaryFiles->setObjectName(QString::fromUtf8("actionBinaryFiles"));
#if QT_CONFIG(statustip)
        actionBinaryFiles->setStatusTip(QString::fromUtf8("https://bin.ceicer.com/svtplay-dl/"));
#endif // QT_CONFIG(statustip)
        actionDownloadLatest = new QAction(DownloadUnpack);
        actionDownloadLatest->setObjectName(QString::fromUtf8("actionDownloadLatest"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/download_bleedingedge.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDownloadLatest->setIcon(icon6);
        actionZoomIn = new QAction(DownloadUnpack);
        actionZoomIn->setObjectName(QString::fromUtf8("actionZoomIn"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/images/zoompluss.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionZoomIn->setIcon(icon7);
        actionZoomDefault = new QAction(DownloadUnpack);
        actionZoomDefault->setObjectName(QString::fromUtf8("actionZoomDefault"));
        actionZoomOut = new QAction(DownloadUnpack);
        actionZoomOut->setObjectName(QString::fromUtf8("actionZoomOut"));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/images/zoomminus.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionZoomOut->setIcon(icon8);
        actionShowToolbar = new QAction(DownloadUnpack);
        actionShowToolbar->setObjectName(QString::fromUtf8("actionShowToolbar"));
        actionDownloadToBleedingedge = new QAction(DownloadUnpack);
        actionDownloadToBleedingedge->setObjectName(QString::fromUtf8("actionDownloadToBleedingedge"));
        actionDownloadToBleedingedge->setEnabled(true);
        actionDownloadToBleedingedge->setIcon(icon6);
        actionDownloadToBleedingedge->setVisible(true);
        actionDownloadToBleedingedge->setIconVisibleInMenu(true);
        actionDownloadToBleedingedge->setShortcutVisibleInContextMenu(true);
        actionDownloadToStable = new QAction(DownloadUnpack);
        actionDownloadToStable->setObjectName(QString::fromUtf8("actionDownloadToStable"));
        actionDownloadToStable->setEnabled(true);
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/images/download_stable.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDownloadToStable->setIcon(icon9);
        actionDownloadToStable->setVisible(true);
        actionDownloadToStable->setIconVisibleInMenu(true);
        actionDownloadToStable->setShortcutVisibleInContextMenu(true);
        actionStable = new QAction(DownloadUnpack);
        actionStable->setObjectName(QString::fromUtf8("actionStable"));
        actionStable->setIcon(icon9);
        actionBleedingedge = new QAction(DownloadUnpack);
        actionBleedingedge->setObjectName(QString::fromUtf8("actionBleedingedge"));
        actionBleedingedge->setIcon(icon6);
        actionSearchLatestStable = new QAction(DownloadUnpack);
        actionSearchLatestStable->setObjectName(QString::fromUtf8("actionSearchLatestStable"));
        QIcon icon10;
        icon10.addFile(QString::fromUtf8(":/images/search_stable.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSearchLatestStable->setIcon(icon10);
        actionDownloadLatestStable = new QAction(DownloadUnpack);
        actionDownloadLatestStable->setObjectName(QString::fromUtf8("actionDownloadLatestStable"));
        actionDownloadLatestStable->setIcon(icon9);
        actionDownloadLatestStableToDirectoryStable = new QAction(DownloadUnpack);
        actionDownloadLatestStableToDirectoryStable->setObjectName(QString::fromUtf8("actionDownloadLatestStableToDirectoryStable"));
        actionDownloadLatestStableToDirectoryStable->setIcon(icon9);
        actionDownloadLatestBleedingedgeToDirectoryBleedingedge = new QAction(DownloadUnpack);
        actionDownloadLatestBleedingedgeToDirectoryBleedingedge->setObjectName(QString::fromUtf8("actionDownloadLatestBleedingedgeToDirectoryBleedingedge"));
        actionDownloadLatestBleedingedgeToDirectoryBleedingedge->setIcon(icon6);
        centralwidget = new QWidget(DownloadUnpack);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(centralwidget->sizePolicy().hasHeightForWidth());
        centralwidget->setSizePolicy(sizePolicy1);
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        pteDisplay = new QPlainTextEdit(centralwidget);
        pteDisplay->setObjectName(QString::fromUtf8("pteDisplay"));
        pteDisplay->setInputMethodHints(Qt::ImhMultiLine|Qt::ImhNoEditMenu|Qt::ImhNoPredictiveText);
        pteDisplay->setUndoRedoEnabled(false);
        pteDisplay->setReadOnly(true);

        gridLayout->addWidget(pteDisplay, 4, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        comboOS = new QComboBox(centralwidget);
        comboOS->setObjectName(QString::fromUtf8("comboOS"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(comboOS->sizePolicy().hasHeightForWidth());
        comboOS->setSizePolicy(sizePolicy2);
        comboOS->setStyleSheet(QString::fromUtf8(""));
        comboOS->setSizeAdjustPolicy(QComboBox::AdjustToContents);
        comboOS->setIconSize(QSize(40, 40));

        horizontalLayout_2->addWidget(comboOS);

        lblAntalFiler = new QLabel(centralwidget);
        lblAntalFiler->setObjectName(QString::fromUtf8("lblAntalFiler"));
        sizePolicy2.setHeightForWidth(lblAntalFiler->sizePolicy().hasHeightForWidth());
        lblAntalFiler->setSizePolicy(sizePolicy2);

        horizontalLayout_2->addWidget(lblAntalFiler);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout_2, 0, 0, 1, 1);

        progressBar = new QProgressBar(centralwidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        sizePolicy2.setHeightForWidth(progressBar->sizePolicy().hasHeightForWidth());
        progressBar->setSizePolicy(sizePolicy2);
        progressBar->setValue(0);
        progressBar->setTextVisible(false);

        gridLayout->addWidget(progressBar, 2, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        comboSvtplaydl = new QComboBox(centralwidget);
        comboSvtplaydl->setObjectName(QString::fromUtf8("comboSvtplaydl"));
        sizePolicy2.setHeightForWidth(comboSvtplaydl->sizePolicy().hasHeightForWidth());
        comboSvtplaydl->setSizePolicy(sizePolicy2);
        comboSvtplaydl->setStyleSheet(QString::fromUtf8(""));
        comboSvtplaydl->setSizeAdjustPolicy(QComboBox::AdjustToContents);

        horizontalLayout->addWidget(comboSvtplaydl);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        gridLayout->addLayout(horizontalLayout, 1, 0, 1, 1);

        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));

        gridLayout->addLayout(formLayout, 3, 0, 1, 1);

        DownloadUnpack->setCentralWidget(centralwidget);
        menubar = new QMenuBar(DownloadUnpack);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 739, 20));
        menubar->setLayoutDirection(Qt::LeftToRight);
        menubar->setInputMethodHints(Qt::ImhNone);
        menubar->setNativeMenuBar(true);
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuVisit = new QMenu(menubar);
        menuVisit->setObjectName(QString::fromUtf8("menuVisit"));
        menuZoom = new QMenu(menubar);
        menuZoom->setObjectName(QString::fromUtf8("menuZoom"));
        menuSettings = new QMenu(menubar);
        menuSettings->setObjectName(QString::fromUtf8("menuSettings"));
        menuChooseLocation = new QMenu(menuSettings);
        menuChooseLocation->setObjectName(QString::fromUtf8("menuChooseLocation"));
        DownloadUnpack->setMenuBar(menubar);
        statusbar = new QStatusBar(DownloadUnpack);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        DownloadUnpack->setStatusBar(statusbar);
        QWidget::setTabOrder(comboOS, comboSvtplaydl);
        QWidget::setTabOrder(comboSvtplaydl, pteDisplay);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuSettings->menuAction());
        menubar->addAction(menuZoom->menuAction());
        menubar->addAction(menuVisit->menuAction());
        menuFile->addSeparator();
        menuFile->addAction(actionDownload);
        menuFile->addAction(actionDownloadLatestStable);
        menuFile->addAction(actionDownloadLatest);
        menuFile->addSeparator();
        menuFile->addAction(actionDownloadToStable);
        menuFile->addAction(actionDownloadToBleedingedge);
        menuFile->addSeparator();
        menuFile->addAction(actionDownloadLatestStableToDirectoryStable);
        menuFile->addAction(actionDownloadLatestBleedingedgeToDirectoryBleedingedge);
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
        menuVisit->addAction(actionSourceCode);
        menuVisit->addAction(actionBinaryFiles);
        menuZoom->addAction(actionZoomIn);
        menuZoom->addAction(actionZoomDefault);
        menuZoom->addAction(actionZoomOut);
        menuZoom->addSeparator();
        menuZoom->addAction(actionShowToolbar);
        menuSettings->addAction(menuChooseLocation->menuAction());
        menuSettings->addAction(actionSearch);
        menuSettings->addAction(actionSearchLatestStable);
        menuChooseLocation->addAction(actionStable);
        menuChooseLocation->addAction(actionBleedingedge);

        retranslateUi(DownloadUnpack);

        QMetaObject::connectSlotsByName(DownloadUnpack);
    } // setupUi

    void retranslateUi(QMainWindow *DownloadUnpack)
    {
        DownloadUnpack->setWindowTitle(QCoreApplication::translate("DownloadUnpack", "MainWindow", nullptr));
        actionSearch->setText(QCoreApplication::translate("DownloadUnpack", "Search beta", nullptr));
#if QT_CONFIG(tooltip)
        actionSearch->setToolTip(QCoreApplication::translate("DownloadUnpack", "\"\"", nullptr));
#endif // QT_CONFIG(tooltip)
        actionDownload->setText(QCoreApplication::translate("DownloadUnpack", "Download...", nullptr));
        actionDownload->setIconText(QCoreApplication::translate("DownloadUnpack", "Download...", nullptr));
#if QT_CONFIG(tooltip)
        actionDownload->setToolTip(QCoreApplication::translate("DownloadUnpack", "Download", nullptr));
#endif // QT_CONFIG(tooltip)
        actionTryToUnzip->setText(QCoreApplication::translate("DownloadUnpack", "Try to decompress", nullptr));
        actionExit->setText(QCoreApplication::translate("DownloadUnpack", "Exit", nullptr));
#if QT_CONFIG(shortcut)
        actionExit->setShortcut(QCoreApplication::translate("DownloadUnpack", "F4", nullptr));
#endif // QT_CONFIG(shortcut)
        actionEnglish->setText(QCoreApplication::translate("DownloadUnpack", "English", nullptr));
        actionSwedish->setText(QCoreApplication::translate("DownloadUnpack", "Swedish", nullptr));
        actionPreCheckLanguagefile->setText(QCoreApplication::translate("DownloadUnpack", "Load external language file...", nullptr));
        actionItalien->setText(QCoreApplication::translate("DownloadUnpack", "Italian", nullptr));
        actionSourceCode->setText(QCoreApplication::translate("DownloadUnpack", "Source code...", nullptr));
        actionBinaryFiles->setText(QCoreApplication::translate("DownloadUnpack", "Binary files...", nullptr));
        actionDownloadLatest->setText(QCoreApplication::translate("DownloadUnpack", "Download beta...", nullptr));
        actionDownloadLatest->setIconText(QCoreApplication::translate("DownloadUnpack", "Download beta...", nullptr));
#if QT_CONFIG(tooltip)
        actionDownloadLatest->setToolTip(QCoreApplication::translate("DownloadUnpack", "Download latest", nullptr));
#endif // QT_CONFIG(tooltip)
        actionZoomIn->setText(QCoreApplication::translate("DownloadUnpack", "Zoom In", nullptr));
#if QT_CONFIG(shortcut)
        actionZoomIn->setShortcut(QCoreApplication::translate("DownloadUnpack", "+", nullptr));
#endif // QT_CONFIG(shortcut)
        actionZoomDefault->setText(QCoreApplication::translate("DownloadUnpack", "Zoom Default", nullptr));
#if QT_CONFIG(shortcut)
        actionZoomDefault->setShortcut(QCoreApplication::translate("DownloadUnpack", "0", nullptr));
#endif // QT_CONFIG(shortcut)
        actionZoomOut->setText(QCoreApplication::translate("DownloadUnpack", "Zoom Out", nullptr));
#if QT_CONFIG(shortcut)
        actionZoomOut->setShortcut(QCoreApplication::translate("DownloadUnpack", "-", nullptr));
#endif // QT_CONFIG(shortcut)
        actionShowToolbar->setText(QCoreApplication::translate("DownloadUnpack", "Show Toolbar", nullptr));
        actionDownloadToBleedingedge->setText(QCoreApplication::translate("DownloadUnpack", "Download to directory \"beta\"", nullptr));
        actionDownloadToBleedingedge->setIconText(QCoreApplication::translate("DownloadUnpack", "Download to\n"
"directory \"beta\"", nullptr));
#if QT_CONFIG(tooltip)
        actionDownloadToBleedingedge->setToolTip(QCoreApplication::translate("DownloadUnpack", "\"\"", nullptr));
#endif // QT_CONFIG(tooltip)
        actionDownloadToStable->setText(QCoreApplication::translate("DownloadUnpack", "Download to directory \"stable\"", nullptr));
        actionDownloadToStable->setIconText(QCoreApplication::translate("DownloadUnpack", "Download to\n"
"directory \"stable\"", nullptr));
#if QT_CONFIG(tooltip)
        actionDownloadToStable->setToolTip(QCoreApplication::translate("DownloadUnpack", "\"\"", nullptr));
#endif // QT_CONFIG(tooltip)
        actionStable->setText(QCoreApplication::translate("DownloadUnpack", "stable...", nullptr));
        actionStable->setIconText(QCoreApplication::translate("DownloadUnpack", "stable...", nullptr));
#if QT_CONFIG(tooltip)
        actionStable->setToolTip(QCoreApplication::translate("DownloadUnpack", "stable...", nullptr));
#endif // QT_CONFIG(tooltip)
        actionBleedingedge->setText(QCoreApplication::translate("DownloadUnpack", "beta...", nullptr));
        actionBleedingedge->setIconText(QCoreApplication::translate("DownloadUnpack", "beta...", nullptr));
#if QT_CONFIG(tooltip)
        actionBleedingedge->setToolTip(QCoreApplication::translate("DownloadUnpack", "beta...", nullptr));
#endif // QT_CONFIG(tooltip)
        actionSearchLatestStable->setText(QCoreApplication::translate("DownloadUnpack", "Search stable", nullptr));
        actionSearchLatestStable->setIconText(QCoreApplication::translate("DownloadUnpack", "Search stable", nullptr));
#if QT_CONFIG(tooltip)
        actionSearchLatestStable->setToolTip(QCoreApplication::translate("DownloadUnpack", "\"\"", nullptr));
#endif // QT_CONFIG(tooltip)
        actionDownloadLatestStable->setText(QCoreApplication::translate("DownloadUnpack", "Download stable...", nullptr));
        actionDownloadLatestStable->setIconText(QCoreApplication::translate("DownloadUnpack", "Download\n"
"stable...", nullptr));
#if QT_CONFIG(tooltip)
        actionDownloadLatestStable->setToolTip(QCoreApplication::translate("DownloadUnpack", "Download stable", nullptr));
#endif // QT_CONFIG(tooltip)
        actionDownloadLatestStableToDirectoryStable->setText(QCoreApplication::translate("DownloadUnpack", "Download stable to directory \"stable\"", nullptr));
        actionDownloadLatestStableToDirectoryStable->setIconText(QCoreApplication::translate("DownloadUnpack", "Download stable to\n"
"directory \"stable\"", nullptr));
#if QT_CONFIG(tooltip)
        actionDownloadLatestStableToDirectoryStable->setToolTip(QCoreApplication::translate("DownloadUnpack", "Download stable\n"
"to directory \"stable\"", nullptr));
#endif // QT_CONFIG(tooltip)
        actionDownloadLatestBleedingedgeToDirectoryBleedingedge->setText(QCoreApplication::translate("DownloadUnpack", "Download beta to directory \"beta\"", nullptr));
        actionDownloadLatestBleedingedgeToDirectoryBleedingedge->setIconText(QCoreApplication::translate("DownloadUnpack", "Download beta to\n"
"directory \"beta\"", nullptr));
#if QT_CONFIG(tooltip)
        actionDownloadLatestBleedingedgeToDirectoryBleedingedge->setToolTip(QCoreApplication::translate("DownloadUnpack", "Download beta to\n"
"directory \"beta\"", nullptr));
#endif // QT_CONFIG(tooltip)
        lblAntalFiler->setText(QString());
        menuFile->setTitle(QCoreApplication::translate("DownloadUnpack", "&File", nullptr));
        menuVisit->setTitle(QCoreApplication::translate("DownloadUnpack", "V&isit", nullptr));
        menuZoom->setTitle(QCoreApplication::translate("DownloadUnpack", "&View", nullptr));
        menuSettings->setTitle(QCoreApplication::translate("DownloadUnpack", "&Tools", nullptr));
        menuChooseLocation->setTitle(QCoreApplication::translate("DownloadUnpack", "Choose location for", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DownloadUnpack: public Ui_DownloadUnpack {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DOWNLOADUNPACK_H
