/********************************************************************************
** Form generated from reading UI file 'updatedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UPDATEDIALOG_H
#define UI_UPDATEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_UpdateDialog
{
public:
    QLabel *lbl;

    void setupUi(QDialog *UpdateDialog)
    {
        if (UpdateDialog->objectName().isEmpty())
            UpdateDialog->setObjectName(QString::fromUtf8("UpdateDialog"));
        UpdateDialog->resize(450, 200);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(UpdateDialog->sizePolicy().hasHeightForWidth());
        UpdateDialog->setSizePolicy(sizePolicy);
        UpdateDialog->setStyleSheet(QString::fromUtf8("QDialog {border:1px solid black;background-color: rgb(218, 255, 221)}\n"
""));
        lbl = new QLabel(UpdateDialog);
        lbl->setObjectName(QString::fromUtf8("lbl"));
        lbl->setGeometry(QRect(9, 57, 431, 71));
        QFont font;
        font.setPointSize(24);
        lbl->setFont(font);
        lbl->setFrameShape(QFrame::NoFrame);

        retranslateUi(UpdateDialog);

        QMetaObject::connectSlotsByName(UpdateDialog);
    } // setupUi

    void retranslateUi(QDialog *UpdateDialog)
    {
        UpdateDialog->setWindowTitle(QCoreApplication::translate("UpdateDialog", "Dialog", nullptr));
        lbl->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class UpdateDialog: public Ui_UpdateDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UPDATEDIALOG_H
