//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CREATESHORTCUT (Linux)
//          Copyright (C) 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/libcreateshortcut
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef CREATECHORTCUT_H
#define CREATECHORTCUT_H

#ifndef QT_STATIC
#include "createshortcut_global.h"
#endif
#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>
#include <QSettings>
#include <QStandardPaths>

#define APPIMAGE
#ifndef QT_STATIC
class CREATECHORTCUT_EXPORT Createshortcut : public QWidget
#endif
#ifdef QT_STATIC
    class Createshortcut : public QWidget
#endif
    {

        Q_OBJECT

    private:

    public:

#ifndef QT_STATIC

        CREATECHORTCUT_EXPORT void makeShortcutFile(QString *display_name, QString *executable_name, QString *comments, QString *categories, QString *icon, bool applications, bool desktop);


        CREATECHORTCUT_EXPORT void removeApplicationShortcut(QString *executable_name);
        CREATECHORTCUT_EXPORT void removeDesktopShortcut(QString *executable_name);
#endif

#ifdef QT_STATIC

        void makeShortcutFile(QString *display_name, QString *executable_name, QString *comments, QString *categories, QString *icon, bool applications, bool desktop);


        void removeApplicationShortcut(QString *executable_name);
        void removeDesktopShortcut(QString *executable_name);
#endif
    };

#endif // CREATECHORTCUT_H
