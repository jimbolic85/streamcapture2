/********************************************************************************
** Form generated from reading UI file 'selectfont.ui'
**
** Created by: Qt User Interface Compiler version 6.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTFONT_H
#define UI_SELECTFONT_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFontComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SelectFont
{
public:
    QFormLayout *formLayout;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QComboBox *comboFilter;
    QComboBox *comboFontSizes;
    QSpacerItem *horizontalSpacer_2;
    QFontComboBox *comboFontFamilies;
    QVBoxLayout *verticalLayout_2;
    QRadioButton *normal;
    QRadioButton *bold;
    QRadioButton *italic;
    QRadioButton *boldItalic;
    QSpacerItem *verticalSpacer;
    QTextEdit *textEdit;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pbDefault;
    QPushButton *pbExit;
    QSpacerItem *horizontalSpacer;

    void setupUi(QDialog *SelectFont)
    {
        if (SelectFont->objectName().isEmpty())
            SelectFont->setObjectName("SelectFont");
        SelectFont->resize(343, 400);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SelectFont->sizePolicy().hasHeightForWidth());
        SelectFont->setSizePolicy(sizePolicy);
        SelectFont->setMinimumSize(QSize(331, 331));
        SelectFont->setSizeGripEnabled(true);
        formLayout = new QFormLayout(SelectFont);
        formLayout->setObjectName("formLayout");
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName("verticalLayout");
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName("horizontalLayout");
        comboFilter = new QComboBox(SelectFont);
        comboFilter->setObjectName("comboFilter");
        comboFilter->setMinimumSize(QSize(200, 0));
        comboFilter->setInsertPolicy(QComboBox::NoInsert);

        horizontalLayout->addWidget(comboFilter);

        comboFontSizes = new QComboBox(SelectFont);
        comboFontSizes->setObjectName("comboFontSizes");
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(comboFontSizes->sizePolicy().hasHeightForWidth());
        comboFontSizes->setSizePolicy(sizePolicy1);
        comboFontSizes->setMinimumSize(QSize(103, 0));
        comboFontSizes->setMaximumSize(QSize(103, 16777215));

        horizontalLayout->addWidget(comboFontSizes);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        comboFontFamilies = new QFontComboBox(SelectFont);
        comboFontFamilies->setObjectName("comboFontFamilies");
        sizePolicy.setHeightForWidth(comboFontFamilies->sizePolicy().hasHeightForWidth());
        comboFontFamilies->setSizePolicy(sizePolicy);
        comboFontFamilies->setMinimumSize(QSize(308, 0));
        comboFontFamilies->setMaximumSize(QSize(308, 16777215));
        comboFontFamilies->setBaseSize(QSize(0, 0));
        comboFontFamilies->setInsertPolicy(QComboBox::InsertAfterCurrent);
        comboFontFamilies->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);

        verticalLayout->addWidget(comboFontFamilies);


        formLayout->setLayout(0, QFormLayout::SpanningRole, verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(7);
        verticalLayout_2->setObjectName("verticalLayout_2");
        normal = new QRadioButton(SelectFont);
        normal->setObjectName("normal");
        normal->setMinimumSize(QSize(140, 0));
        normal->setChecked(true);

        verticalLayout_2->addWidget(normal);

        bold = new QRadioButton(SelectFont);
        bold->setObjectName("bold");
        bold->setMinimumSize(QSize(140, 0));
        QFont font;
        font.setBold(true);
        bold->setFont(font);

        verticalLayout_2->addWidget(bold);

        italic = new QRadioButton(SelectFont);
        italic->setObjectName("italic");
        italic->setMinimumSize(QSize(140, 0));
        QFont font1;
        font1.setItalic(true);
        italic->setFont(font1);

        verticalLayout_2->addWidget(italic);

        boldItalic = new QRadioButton(SelectFont);
        boldItalic->setObjectName("boldItalic");
        boldItalic->setMinimumSize(QSize(140, 0));
        QFont font2;
        font2.setBold(true);
        font2.setItalic(true);
        boldItalic->setFont(font2);

        verticalLayout_2->addWidget(boldItalic);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        formLayout->setLayout(1, QFormLayout::LabelRole, verticalLayout_2);

        textEdit = new QTextEdit(SelectFont);
        textEdit->setObjectName("textEdit");
        sizePolicy.setHeightForWidth(textEdit->sizePolicy().hasHeightForWidth());
        textEdit->setSizePolicy(sizePolicy);
        textEdit->setMinimumSize(QSize(140, 0));
        textEdit->setMaximumSize(QSize(16777215, 16777215));
        QFont font3;
        font3.setUnderline(false);
        textEdit->setFont(font3);
        textEdit->setFocusPolicy(Qt::ClickFocus);
        textEdit->setContextMenuPolicy(Qt::DefaultContextMenu);
        textEdit->setStyleSheet(QString::fromUtf8("padding:2px;"));
        textEdit->setInputMethodHints(Qt::ImhMultiLine|Qt::ImhPreferLatin);
        textEdit->setFrameShape(QFrame::StyledPanel);
        textEdit->setFrameShadow(QFrame::Plain);
        textEdit->setDocumentTitle(QString::fromUtf8(""));
        textEdit->setUndoRedoEnabled(false);
        textEdit->setHtml(QString::fromUtf8("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10.8pt;\"><br /></p></body></html>"));
        textEdit->setAcceptRichText(false);
        textEdit->setPlaceholderText(QString::fromUtf8(""));

        formLayout->setWidget(1, QFormLayout::FieldRole, textEdit);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName("horizontalLayout_2");
        pbDefault = new QPushButton(SelectFont);
        pbDefault->setObjectName("pbDefault");
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(pbDefault->sizePolicy().hasHeightForWidth());
        pbDefault->setSizePolicy(sizePolicy2);
        pbDefault->setMinimumSize(QSize(140, 0));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/default.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbDefault->setIcon(icon);
        pbDefault->setIconSize(QSize(20, 20));

        horizontalLayout_2->addWidget(pbDefault);

        pbExit = new QPushButton(SelectFont);
        pbExit->setObjectName("pbExit");
        pbExit->setEnabled(true);
        sizePolicy2.setHeightForWidth(pbExit->sizePolicy().hasHeightForWidth());
        pbExit->setSizePolicy(sizePolicy2);
        pbExit->setMinimumSize(QSize(140, 0));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/exitny.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbExit->setIcon(icon1);
        pbExit->setIconSize(QSize(20, 20));

        horizontalLayout_2->addWidget(pbExit);

        horizontalSpacer = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        formLayout->setLayout(2, QFormLayout::SpanningRole, horizontalLayout_2);


        retranslateUi(SelectFont);

        QMetaObject::connectSlotsByName(SelectFont);
    } // setupUi

    void retranslateUi(QDialog *SelectFont)
    {
        SelectFont->setWindowTitle(QCoreApplication::translate("SelectFont", "Select font", nullptr));
#if QT_CONFIG(whatsthis)
        comboFilter->setWhatsThis(QCoreApplication::translate("SelectFont", "Choose the type of font:<br>\"Monospace fonts\", fonts where all characters are the same width. <br>\"Proportional fonts\", fonts where the width varies depending on the character.", nullptr));
#endif // QT_CONFIG(whatsthis)
#if QT_CONFIG(whatsthis)
        comboFontSizes->setWhatsThis(QString());
#endif // QT_CONFIG(whatsthis)
        normal->setText(QCoreApplication::translate("SelectFont", "Normal", nullptr));
        bold->setText(QCoreApplication::translate("SelectFont", "Bold", nullptr));
        italic->setText(QCoreApplication::translate("SelectFont", "Italic", nullptr));
        boldItalic->setText(QCoreApplication::translate("SelectFont", "Bold and italic", nullptr));
        textEdit->setMarkdown(QString());
        pbDefault->setText(QCoreApplication::translate("SelectFont", "Default", nullptr));
        pbExit->setText(QCoreApplication::translate("SelectFont", "Exit", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SelectFont: public Ui_SelectFont {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTFONT_H
