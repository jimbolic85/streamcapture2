//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          zsyncUpdateAppImage
//          Copyright (C) 2020 - 2022  Ingemar Ceicer
//          https://gitlab.com/posktomten/zsyncupdateappimage
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef UPDATE_H
#define UPDATE_H

#include <QObject>
#include <QDialog>



class Update : public QObject
{
    Q_OBJECT
public:
    explicit Update(QObject *parent = nullptr);
// Signal is sent when the update is complete (true) or failed (false)
signals:
    void isUpdated(bool updated);


private:

    bool fileExists(QString & path);
// Perform the update
public:
    void doUpdate(const QString zsync_arg_1, const QString zsync_arg_2, const QString progname, QDialog *ud);

};

#endif // UPDATE_H
