//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkupdate
//          Copyright (C) 2020 - 2022 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include <qglobal.h>
#ifndef CHECKUPDATE_H
#define CHECKUPDATE_H

#ifdef Q_OS_LINUX
#include "checkupdate_global.h"
#endif


#if !(defined Q_OS_WIN && defined QT_STATIC)
#include "checkupdate_global.h"
#endif
#include <QNetworkReply>
#include <QWidget>
#if (defined QT_STATIC)

class CheckUpdate : public QWidget
#else
class CHECKUPDATE_EXPORT CheckUpdate : public QWidget
#endif
{

    Q_OBJECT

private:
    int jfrVersion(const QString &currentVersion, const QString &newVersion);
    QString moreInfo(const QString *infoVersion);
    void networkError(const QString &progName, const QString &currentVersion);

    void updateNeeded(const QString &progName, const QString &currentVersion, const QString &onlyNewVersion,        const QString &info, const QString &updateinstruktions);
    QString networkErrorMessages(QNetworkReply::NetworkError error);

signals:
//    CHECKUPDATE_EXPORT void foundUpdate(bool);
    void foundUpdate(bool);

public:
//    CHECKUPDATE_EXPORT void check(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &updateinstruktions);
    void check(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &updateinstruktions);

//    CHECKUPDATE_EXPORT void checkOnStart(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &updateinstructions);
    void checkOnStart(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &updateinstructions);

    ~CheckUpdate();
    CheckUpdate();
};

#endif // CHECKUPDATE_H
