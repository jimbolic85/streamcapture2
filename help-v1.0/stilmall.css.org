body {
  font-family: Ubuntu, sans-serif;
  font-size: 1em;
  color: black;
  background: white;
}

#container {
  position: relative;
  margin: 0 auto;
  width: 80%;
  padding-left: 1em;
  padding-right: 1em;
  overflow: auto;
  text-align: left;
  color: inherit;
  background-color: inherit;
}

.languagecontainer {
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 1em;
  margin-right: 4em;
  font-size: inherit;
}

.language {
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-right: 4em;
}

.language img {
  margin: 0;
}

.download {
  display: flex;
  justify-content: center;
}

.download_item {
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: 0;
  box-shadow: 10px 10px 5px #aaaaaa;
  border: 1px solid #BFBFBF;
  padding: 1em;
  margin: 1em;
  background-color: RGB(218, 255, 221);
  font-size: inherit;
}

h2 {
  font-family: inherit;
  color: green;
  background-color: inherit;
  font-size: 1.5em;
  font-weight: bolder;
  display: block;
}

h3 {
  font-family: inherit;
  font-size: 1.1em;
  font-weight: bold;
}

p {
  font-family: inherit;
  font-size: inherit;
  font-weight: normal;
}

ol {
  padding-left: 1em;
  list-style-position: outside;
}

li {
  margin-bottom: 0.5em;
  font-size: inherit;
}

a:link {
  color: blue;
  background-color: inherit;
  text-decoration: none;
}

a:visited {
  color: blue;
  background-color: inherit;
}

a:hover,
a:focus {
  color: red;
  background-color: inherit;
}

a:active {
  color: blue;
  background-color: inherit;
}

img {
  box-shadow: 10px 10px 5px #aaaaaa;
  margin-top: 2em;
  width: 80%;
  height: auto;
}

h3 {
  margin-top: 2em;
}

textarea {
  min-width: 80%;
  max-width: 80%;
  height: 8em;
  min-height: 2em;
  color: blue;
  background: white;
}

input[type=text] {
  color: blue;
  background: white;
}

input[type=submit] {
  cursor: pointer;
  width: 80px;
  height: 30px;
}

input[type=button] {
  cursor: pointer;
  width: 80px;
  height: 30px;
}

select {
  width: 140px;
  overflow: hidden;
  padding-left: 0.2em;
  padding-top: 0.2em;
  padding-bottom: 0.2em;
}

.noshadow {
  box-shadow: none;
  width: auto;
  height: 48px;
  margin-right: 2em;
}

.noshadowling {
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 100%;
  height: 48px;
  margin-right: 2em;
  margin-left: 1em;
}

.margintop {
  margin-top: 2em;
}

.margin-right1em {
  padding-right: 1em;
}

.margin-right4em {
  padding-right: 4em;
}

.kursiv {
  font-style: italic;
  font-weight: normal;
  font-size: inherit;
}

.rod {
  color: red;
  background: white;
  font-weight: bold;
  min-height: 20px;
  max-height: 20px;
}

.fet {
  font-weight: bold;
  min-height: 20px;
  max-height: 20px;
}

@media only screen and (max-width: 1200px) {
  #container {
    width: 100%;
    padding-left: 0;
    padding-right: 0;
  }

  .languagecontainer {
    margin: 0;
    justify-content: flex-start;
    align-items: center;
  }

  .language {
    display: flex;
    justify-content: flex-start;
    margin-right: 1em;
  }

  .download {
    flex-direction: column;
    font-family: inherit;
    font-size: inherit;
  }

  .download_item {
    background-color: inherit;
    color: black;
    box-shadow: none;
    border: none;
    font-family: inherit;
    font-size: inherit;
  }

  ol {
    list-style-position: inside;
  }

  li {
    margin-bottom: 1.5em;
  }

  img {
    width: 100%;
  }
}
