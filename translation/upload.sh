#!/bin/bash

if [[ -x "/home/ingemar/Qt/6.4.1/gcc_64/bin/qmake" ]]
then

    SOKVAG="/home/ingemar/Qt/6.4.1/gcc_64/bin/"
    
elif [[ -x "/opt/Qt/Qt5.15.7/bin/qmake" ]]
then

    SOKVAG="/opt/Qt/Qt5.15.7/bin/bin/"

else

	echo "Cannot find executable program files."
	exit 1
	
fi

date_now=$(date "+%FT%H-%M-%S")
mkdir -p all_translations-${date_now}/ts all_translations-${date_now}/qm
cp *.ts all_translations-${date_now}/ts
cd all_translations-${date_now}/ts
${SOKVAG}lrelease *.ts
mv *.qm ../qm
cd ../../
/usr/bin/zip -r all_translations-${date_now}.zip all_translations-${date_now}
rm -r all_translations-${date_now}

fil=all_translations-${date_now}.zip 


HOST="`sed -n 1p ../../secret`"
USER="`sed -n 2p ../../secret`"
PASSWORD="`sed -n 3p ../../secret`"


ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
passive
cd "/bin.ceicer.com/public_html/streamcapture2/bin/all_translations"
put $fil
bye
EOF

rm all_translations-${date_now}.zip
