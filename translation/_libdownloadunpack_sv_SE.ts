<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE" sourcelanguage="en_US">
<context>
    <name>DownloadUnpack</name>
    <message>
        <location filename="../download.cpp" line="61"/>
        <source>Is not writable. Cancels</source>
        <translation>Är inte skrivbar. Avbryter</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="65"/>
        <source>Starting to download...
</source>
        <translation>Börjar att ladda ner...
</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="91"/>
        <location filename="../download.cpp" line="218"/>
        <location filename="../download.cpp" line="220"/>
        <source>to</source>
        <translation>till</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="91"/>
        <location filename="../download.cpp" line="93"/>
        <source>Check your file permissions and antivirus software.</source>
        <translation>Kontrollera dina filbehörigheter och antivirusprogram.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="91"/>
        <source>Can not save</source>
        <translation>Kan inte spara</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="93"/>
        <source>Can not save to</source>
        <translation>Kan inte spara till</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="95"/>
        <source>is downloaded to</source>
        <translation>är nedladdad till</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="108"/>
        <source>Starting to decompress</source>
        <translation>Börjar dekomprimera</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="125"/>
        <source>is decompressed.</source>
        <translation>är dekomprimerad.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="131"/>
        <source>Removed</source>
        <translation>Tog bort</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="143"/>
        <source>Could not delete</source>
        <translation>Kunde inte ta bort</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="169"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Linux operating systems. It will not work with Windows operating system.</source>
        <translation>Varning! Den version av svtplay-dl som du laddade ner är avsedd för Linux operativsystem. Det kommer inte att fungera med Windows operativsystem.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="177"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Windows operating systems. It will not work with Linux operating system.</source>
        <translation>Varning! Den version av svtplay-dl som du laddade ner är avsedd för Windows operativsystem. Det kommer inte att fungera med Linux operativsystem.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="210"/>
        <source>Download failed, please try again.</source>
        <translation>Nedladdningen misslyckades, försök igen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="218"/>
        <location filename="../download.cpp" line="220"/>
        <source>Changed name from</source>
        <translation>Ändrade namn från</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="236"/>
        <source>Select &quot;Tools&quot;, &quot;Use svtplay-dl beta&quot; to use this version.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Använd svtpay-dl beta&quot; för att använda den här versionen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="238"/>
        <source>Select &quot;Tools&quot;, &quot;Use svtplay-dl stable&quot; to use this version.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Använd svtpay-dl stabila&quot; för att använda den här versionen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="268"/>
        <source>Failed to decompress.</source>
        <translation>Misslyckades med att dekomprimera.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="186"/>
        <source>Current location for stable:</source>
        <translation>Nuvarande plats för stabila:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="195"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Ta en skärmdump (5 sekunders fördröjning)</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="207"/>
        <location filename="../downloadunpack.cpp" line="210"/>
        <source>For</source>
        <translation>För</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="242"/>
        <location filename="../downloadunpack.cpp" line="274"/>
        <location filename="../downloadunpack.cpp" line="334"/>
        <location filename="../downloadunpack.cpp" line="357"/>
        <source>will be downloaded to</source>
        <translation>Kommer att laddas ner till</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="285"/>
        <location filename="../downloadunpack.cpp" line="334"/>
        <location filename="../downloadunpack.cpp" line="357"/>
        <source>Selected svtplay-dl</source>
        <translation>Vald svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="32"/>
        <source>Download svtplay-dl from bin.ceicer.com</source>
        <translation>Ladda ner svtplay-dl från bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="285"/>
        <location filename="../downloadunpack.cpp" line="302"/>
        <location filename="../downloadunpack.cpp" line="319"/>
        <source>will be downloaded.</source>
        <translation>kommer att laddas ner.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="186"/>
        <source>Current location for beta:</source>
        <translation>Nuvarande plats för beta:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="242"/>
        <source>The stable version of svtplay-dl</source>
        <translation>Den stabila versionen av svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="274"/>
        <source>The beta version of svtplay-dl</source>
        <translation>Betaversionen av svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="274"/>
        <location filename="../downloadunpack.cpp" line="357"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation>För att använda denn version, Klicka
&quot;Verktyg&quot;, &quot;Använd svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="478"/>
        <source>files</source>
        <translation>filer</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="480"/>
        <source>file</source>
        <translation>fil</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="37"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="198"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="232"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="289"/>
        <source>Try to decompress</source>
        <translation>Försöker att dekomprimera</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="338"/>
        <source>Source code...</source>
        <translation>Källkod...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="346"/>
        <source>Binary files...</source>
        <translation>Binära filer...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="298"/>
        <source>Exit</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="236"/>
        <source>Choose location for</source>
        <translation>Välj platsen för</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="215"/>
        <source>V&amp;isit</source>
        <translation>&amp;Besök</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="222"/>
        <source>&amp;View</source>
        <translation>&amp;Visa</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="257"/>
        <source>Search beta</source>
        <translation>Sök beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="260"/>
        <location filename="../downloadunpack.ui" line="420"/>
        <location filename="../downloadunpack.ui" line="448"/>
        <location filename="../downloadunpack.ui" line="502"/>
        <source>&quot;&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="272"/>
        <location filename="../downloadunpack.ui" line="275"/>
        <source>Download...</source>
        <translation>Ladda ner...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="278"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="301"/>
        <source>F4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="310"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="319"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="324"/>
        <source>Load external language file...</source>
        <translation>Ladda extern språkfil...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="333"/>
        <source>Italian</source>
        <translation>Italienska</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="481"/>
        <location filename="../downloadunpack.ui" line="484"/>
        <location filename="../downloadunpack.ui" line="487"/>
        <source>beta...</source>
        <translation>beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="496"/>
        <location filename="../downloadunpack.ui" line="499"/>
        <source>Search stable</source>
        <translation>Sök stabila</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="511"/>
        <source>Download stable...</source>
        <translation>Ladda ner stabila...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="514"/>
        <source>Download
stable...</source>
        <translation>Ladda ner
stabila...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="518"/>
        <source>Download stable</source>
        <translation>Ladda ner stabila</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="527"/>
        <source>Download stable to directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="530"/>
        <source>Download stable to
directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila till
mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="534"/>
        <source>Download stable
to directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila
till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="544"/>
        <source>Download beta to directory &quot;beta&quot;</source>
        <translation>Ladda ner beta till mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="547"/>
        <location filename="../downloadunpack.ui" line="551"/>
        <source>Download beta to
directory &quot;beta&quot;</source>
        <translation>ladda ner beta till
mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="364"/>
        <source>Download latest</source>
        <translation>Ladda ner senaste</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="466"/>
        <location filename="../downloadunpack.ui" line="469"/>
        <location filename="../downloadunpack.ui" line="472"/>
        <source>stable...</source>
        <translation>stabila...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="441"/>
        <source>Download to directory &quot;stable&quot;</source>
        <translation>Ladda ner till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="358"/>
        <location filename="../downloadunpack.ui" line="361"/>
        <source>Download beta...</source>
        <translation>Ladda ner beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="413"/>
        <source>Download to directory &quot;beta&quot;</source>
        <translation>Ladda ner till mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="416"/>
        <source>Download to
directory &quot;beta&quot;</source>
        <translation>Ladda ner till
mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="444"/>
        <source>Download to
directory &quot;stable&quot;</source>
        <translation>Ladda ner till
mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="373"/>
        <source>Zoom In</source>
        <translation>Zooma in</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="376"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="381"/>
        <source>Zoom Default</source>
        <translation>Zooma som standard</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="384"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="393"/>
        <source>Zoom Out</source>
        <translation>Zooma ut</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="396"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="401"/>
        <source>Show Toolbar</source>
        <translation>Visa verktygsfält</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="387"/>
        <location filename="../downloadunpack.cpp" line="410"/>
        <location filename="../filedialog.cpp" line="36"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="386"/>
        <location filename="../downloadunpack.cpp" line="409"/>
        <location filename="../filedialog.cpp" line="35"/>
        <source>Choose</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="70"/>
        <source>Search and Exit</source>
        <translation>Sök och Avsluta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="81"/>
        <source>Select svtplay-dl and download to the &quot;stable&quot; folder and to the &quot;beta&quot; folder.</source>
        <translation>Välj svtplay-dl och ladda ner till  mappen &quot;stable&quot; och till mappen &quot;beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="93"/>
        <source>Direct download of stable to the &quot;stable&quot; folder and beta to the &quot;beta&quot; folder.</source>
        <translation>Direkt nedladdning av stable till &quot;stable&quot;-mappen och beta till &quot;beta&quot;-mappen.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="242"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use stable svtplay-dl&quot;.</source>
        <translation>För att använda denna svtplay-dl, klicka
&quot;Verktyg&quot;, &quot;Använd stabila svtplay-dl&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="302"/>
        <source>stable svtplay-dl</source>
        <translation>stabila svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="319"/>
        <source>svtplay-dl beta</source>
        <translation>svtplay-dl beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="334"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation>För att använda denna version, klicka
&quot;Verktyg&quot;, Använd stabila svtplay-dl&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="388"/>
        <location filename="../downloadunpack.cpp" line="411"/>
        <location filename="../filedialog.cpp" line="37"/>
        <source>Save svtplay-dl in directory</source>
        <translation>Spara svtplay-dl i mappen</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="37"/>
        <source>The font size changes to the selected font size</source>
        <translation>Teckenstorleken ändras till vald teckenstorlek</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="40"/>
        <source>Save a screenshot</source>
        <translation>Spara en skärmdump</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="42"/>
        <source>Images (*.png)</source>
        <translation>bildfiler (*.png)</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="178"/>
        <source>No error.</source>
        <translation>Inga fel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="182"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Fjärrservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="186"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Fjärrvärdnamnet hittades inte (ogiltigt värdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="190"/>
        <source>The connection to the remote server timed out.</source>
        <translation>Anslutningen till fjärrservern tog för lång tid.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="194"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>Operationen avbröts via anrop till abort() eller close() innan den avslutades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="198"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>SSL/TLS-handskakningen misslyckades och den krypterade kanalen kunde inte upprättas. Signalen sslErrors() borde ha sänts ut.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="202"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket, men systemet har initierat roaming till en annan åtkomstpunkt. Begäran bör skickas in på nytt och kommer att behandlas så snart anslutningen återupprättats.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="206"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket eller misslyckande med att starta nätverket.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="210"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>Bakgrundsbegäran är för närvarande inte tillåten på grund av plattformspolicy.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="214"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>Omdirigeringarna nådde maxgränsen. Gränsen är som standard inställd på 50 eller enligt QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="218"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Vid omdirigeringar upptäckte API:et för nätverksåtkomst en omdirigering från ett krypterat protokoll (https) till ett okrypterat (http).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="222"/>
        <source>An unknown network-related error was detected.</source>
        <translation>Ett okänt nätverksrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="226"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>Anslutningen till proxyservern nekades (proxyservern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="230"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Proxyservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="234"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Proxyvärdnamnet hittades inte (ogiltigt proxyvärdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="238"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>Anslutningen till proxyn tog för läng tid eller så svarade proxyn inte i tid på den skickade begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="242"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Proxyn kräver autentisering för att uppfylla begäran men accepterade inte erbjudna användarnamn och lösenord (eller så har inga användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="246"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation>Ett okänt proxyrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="250"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>Åtkomsten till fjärrinnehållet nekades (liknande HTTP-fel 403).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="254"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>Åtgärden som begärs på fjärrinnehållet är inte tillåten.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="258"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Fjärrinnehållet hittades inte på servern (liknande HTTP-fel 404).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="262"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Fjärrservern kräver autentisering för att visa innehållet men angivna användarnamn och lösenord accepterades inte (eller så har inget användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="266"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>Begäran behövde skickas igen, men detta misslyckades till exempel eftersom uppladdningsdata inte kunde läsas en andra gång.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="270"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Begäran kunde inte slutföras på grund av en konflikt med resursens nuvarande tillstånd.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="274"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation>Den begärda resursen är inte längre tillgänglig på servern.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="278"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>Ett okänt fel relaterat till fjärrinnehållet upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="282"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>Network Access API kan inte uppfylla begäran eftersom protokollet inte är känt.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="286"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>Den begärda åtgärden är ogiltig för detta protokoll.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="290"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>Ett haveri i protokollet upptäcktes (analysfel, ogiltiga eller oväntade svar, etc.).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="294"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Servern stötte på ett oväntat tillstånd som hindrade den från att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="298"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Servern stöder inte den funktionalitet som krävs för att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="302"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation>Servern kan inte hantera begäran just nu.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="306"/>
        <source>Unknown Server Error.</source>
        <translation>Okänt serverfel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="310"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Fjärrservern vägrade anslutningen (servern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="314"/>
        <source>Unknown Error.</source>
        <translation>Okänt fel.</translation>
    </message>
</context>
</TS>
