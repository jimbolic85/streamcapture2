<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>SelectFont</name>
    <message>
        <location filename="../selectfont.ui" line="26"/>
        <location filename="../selectfont.cpp" line="26"/>
        <source>Select font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="45"/>
        <source>Choose the type of font:&lt;br&gt;&quot;Monospace fonts&quot;, fonts where all characters are the same width. &lt;br&gt;&quot;Proportional fonts&quot;, fonts where the width varies depending on the character.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="282"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="314"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="160"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="139"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="178"/>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="197"/>
        <source>Bold and italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="28"/>
        <source>Select font size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="83"/>
        <source>All fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="83"/>
        <source>Monospaced fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="84"/>
        <source>Proportional fonts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
