<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>DownloadUnpack</name>
    <message>
        <location filename="../downloadunpack.ui" line="37"/>
        <source>MainWindow</source>
        <translation>Finestra principale</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="198"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="232"/>
        <source>&amp;Tools</source>
        <translation>&amp;Strumenti</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="289"/>
        <source>Try to decompress</source>
        <translation>Prova a decomprimere</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="338"/>
        <source>Source code...</source>
        <translation>Codice sorgente...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="346"/>
        <source>Binary files...</source>
        <translation>File binari...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="298"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="236"/>
        <source>Choose location for</source>
        <translation>Scegli percorso per</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="215"/>
        <source>V&amp;isit</source>
        <translation>V&amp;isita</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="222"/>
        <source>&amp;View</source>
        <translation>&amp;Visualizza</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="257"/>
        <source>Search beta</source>
        <translation>Cerca beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="260"/>
        <location filename="../downloadunpack.ui" line="420"/>
        <location filename="../downloadunpack.ui" line="448"/>
        <location filename="../downloadunpack.ui" line="502"/>
        <source>&quot;&quot;</source>
        <translation>&quot;&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="272"/>
        <location filename="../downloadunpack.ui" line="275"/>
        <source>Download...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="278"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="301"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="310"/>
        <source>English</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="319"/>
        <source>Swedish</source>
        <translation>Svedese</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="324"/>
        <source>Load external language file...</source>
        <translation>Carica file lingua esterno...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="333"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="481"/>
        <location filename="../downloadunpack.ui" line="484"/>
        <location filename="../downloadunpack.ui" line="487"/>
        <source>beta...</source>
        <translation>beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="496"/>
        <location filename="../downloadunpack.ui" line="499"/>
        <source>Search stable</source>
        <translation>Cerca stabile</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="511"/>
        <source>Download stable...</source>
        <translation>Download stabile...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="514"/>
        <source>Download
stable...</source>
        <translation>Download stabile...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="518"/>
        <source>Download stable</source>
        <translation>Download stabile</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="527"/>
        <source>Download stable to directory &quot;stable&quot;</source>
        <translation>Download stabile nella cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="530"/>
        <source>Download stable to
directory &quot;stable&quot;</source>
        <translation>Download stabile nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="534"/>
        <source>Download stable
to directory &quot;stable&quot;</source>
        <translation>Download stabile nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="544"/>
        <source>Download beta to directory &quot;beta&quot;</source>
        <translation>Download beta nella 
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="547"/>
        <location filename="../downloadunpack.ui" line="551"/>
        <source>Download beta to
directory &quot;beta&quot;</source>
        <translation>Download beta nella
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="364"/>
        <source>Download latest</source>
        <translation>Download più recente</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="466"/>
        <location filename="../downloadunpack.ui" line="469"/>
        <location filename="../downloadunpack.ui" line="472"/>
        <source>stable...</source>
        <translation>stabile...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="441"/>
        <source>Download to directory &quot;stable&quot;</source>
        <translation>Download nella cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="358"/>
        <location filename="../downloadunpack.ui" line="361"/>
        <source>Download beta...</source>
        <translation>Download beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="413"/>
        <source>Download to directory &quot;beta&quot;</source>
        <translation>Download nella cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="416"/>
        <source>Download to
directory &quot;beta&quot;</source>
        <translation>Download nella
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="444"/>
        <source>Download to
directory &quot;stable&quot;</source>
        <translation>Download nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="373"/>
        <source>Zoom In</source>
        <translation>Zoom +</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="376"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="381"/>
        <source>Zoom Default</source>
        <translation>Zoom predefinito</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="384"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="393"/>
        <source>Zoom Out</source>
        <translation>Zoom -</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="396"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="401"/>
        <source>Show Toolbar</source>
        <translation>Visualizza barra strumenti</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="61"/>
        <source>Is not writable. Cancels</source>
        <translation>Non è scrivibile. Annulla</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="65"/>
        <source>Starting to download...
</source>
        <translation>Avvio download...
</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="91"/>
        <location filename="../download.cpp" line="218"/>
        <location filename="../download.cpp" line="220"/>
        <source>to</source>
        <translation>in</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="91"/>
        <location filename="../download.cpp" line="93"/>
        <source>Check your file permissions and antivirus software.</source>
        <translation>Verifica permessi file e software antivirus.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="91"/>
        <source>Can not save</source>
        <translation>Impossibile salvare</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="93"/>
        <source>Can not save to</source>
        <translation>Impossibile salvare in</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="95"/>
        <source>is downloaded to</source>
        <translation>è stato scaricato in</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="108"/>
        <source>Starting to decompress</source>
        <translation>Avvio decompressione</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="125"/>
        <source>is decompressed.</source>
        <translation>è stato decompresso.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="131"/>
        <source>Removed</source>
        <translation>Rimosso</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="143"/>
        <source>Could not delete</source>
        <translation>Impossibile eliminare</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="169"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Linux operating systems. It will not work with Windows operating system.</source>
        <translation>Attenzione! La versione di svtplay-dl che hai scaricato è destinata ai sistemi operativi Linux. 
Non funzionerà con il sistema operativo Windows.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="177"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Windows operating systems. It will not work with Linux operating system.</source>
        <translation>Attenzione! La versione di svtplay-dl che hai scaricato è destinata ai sistemi operativi Windows. 
Non funzionerà con il sistema operativo Linux.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="210"/>
        <source>Download failed, please try again.</source>
        <translation>Download non riuscito, riprova.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="218"/>
        <location filename="../download.cpp" line="220"/>
        <source>Changed name from</source>
        <translation>Modifica nome da</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="236"/>
        <source>Select &quot;Tools&quot;, &quot;Use svtplay-dl beta&quot; to use this version.</source>
        <translation>Per usare questa versione seleziona &quot;Strumenti&quot; -&gt; &quot;Usa svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="238"/>
        <source>Select &quot;Tools&quot;, &quot;Use svtplay-dl stable&quot; to use this version.</source>
        <translation>Per usare questa versione seleziona &quot;Strumenti&quot; -&gt; &quot;Usa svtplay-dl stabile&quot;.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="268"/>
        <source>Failed to decompress.</source>
        <translation>Decompressione fallita.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="207"/>
        <location filename="../downloadunpack.cpp" line="210"/>
        <source>For</source>
        <translation>Per</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="242"/>
        <location filename="../downloadunpack.cpp" line="274"/>
        <location filename="../downloadunpack.cpp" line="334"/>
        <location filename="../downloadunpack.cpp" line="357"/>
        <source>will be downloaded to</source>
        <translation>verrà scaricato in</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="274"/>
        <source>The beta version of svtplay-dl</source>
        <translation>La versione beta di svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="274"/>
        <location filename="../downloadunpack.cpp" line="357"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation>Per usare questa versione seleziona
&quot;Strumenti&quot; -&gt; &quot;Usa svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="478"/>
        <source>files</source>
        <translation>file</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="480"/>
        <source>file</source>
        <translation>file</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="387"/>
        <location filename="../downloadunpack.cpp" line="410"/>
        <location filename="../filedialog.cpp" line="36"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="32"/>
        <source>Download svtplay-dl from bin.ceicer.com</source>
        <translation>Download svtplay-dl da bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="186"/>
        <source>Current location for stable:</source>
        <translation>Percorso attuale versioen stabile:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="195"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Cattura una schermata (ritardo 5 secondi)</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="285"/>
        <location filename="../downloadunpack.cpp" line="334"/>
        <location filename="../downloadunpack.cpp" line="357"/>
        <source>Selected svtplay-dl</source>
        <translation>svtplay-dl selezionato</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="186"/>
        <source>Current location for beta:</source>
        <translation>Percorso attuale per beta:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="70"/>
        <source>Search and Exit</source>
        <translation>Cerca ed esci</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="81"/>
        <source>Select svtplay-dl and download to the &quot;stable&quot; folder and to the &quot;beta&quot; folder.</source>
        <translation>Seleziona svtplay-dl e download nella cartella &quot;stable&quot; e nella cartella &quot;beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="93"/>
        <source>Direct download of stable to the &quot;stable&quot; folder and beta to the &quot;beta&quot; folder.</source>
        <translation>Download diretto della stabile nella cartella &quot;stable&quot; e beta nella cartella &quot;beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="242"/>
        <source>The stable version of svtplay-dl</source>
        <translation>La versione stabile di svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="242"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use stable svtplay-dl&quot;.</source>
        <translation>Per usare questa versione di svtplay-dl, fai clic su
&quot;Strumenti&quot;, &quot;Usa svtplay-dl stabile&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="285"/>
        <location filename="../downloadunpack.cpp" line="302"/>
        <location filename="../downloadunpack.cpp" line="319"/>
        <source>will be downloaded.</source>
        <translation>verrà scaricato.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="302"/>
        <source>stable svtplay-dl</source>
        <translation>svtplay-dl stabile</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="319"/>
        <source>svtplay-dl beta</source>
        <translation>svtplay-dl beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="334"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation>Per usare questa versione di svtplay-dl, fai clic su
&quot;Strumenti&quot;, &quot;Usa svtplay-dl stabile&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="386"/>
        <location filename="../downloadunpack.cpp" line="409"/>
        <location filename="../filedialog.cpp" line="35"/>
        <source>Choose</source>
        <translation>Scegli</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="388"/>
        <location filename="../downloadunpack.cpp" line="411"/>
        <location filename="../filedialog.cpp" line="37"/>
        <source>Save svtplay-dl in directory</source>
        <translation>Salva svtplay-dl nella cartella</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="37"/>
        <source>The font size changes to the selected font size</source>
        <translation>Dimensione tipo di carattere modificata come da dimensione selezionata</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="40"/>
        <source>Save a screenshot</source>
        <translation>Salva schermata</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="42"/>
        <source>Images (*.png)</source>
        <translation>Immagini (*.png)</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="178"/>
        <source>No error.</source>
        <translation>Nessun errore.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="182"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server remoto ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="186"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Non è stato trovato il nome host remoto (nome host non valido).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="190"/>
        <source>The connection to the remote server timed out.</source>
        <translation>Timeout della connessione verso il server remoto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="194"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>L&apos;operazione è stata annullata tramite chiamate a abort() o close() prima che fosse terminata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="198"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>L&apos;handshake SSL/TLS non è riuscito e non è stato possibile stabilire il canale crittografato.&lt;br&gt;Dovrebbe essere stato emesso il segnale sslErrors().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="202"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete, tuttavia il sistema ha avviato il roaming verso un altro punto di accesso.&lt;br&gt;La richiesta deve essere ripresentata e sarà elaborata non appena la connessione sarà ristabilita.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="206"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete o del mancato avvio della rete.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="210"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>La richiesta in background non è attualmente consentita a causa della politica della piattaforma.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="214"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>È stato raggiunto il limite massimo durante i reindirizzamenti.&lt;br&gt;Per impostazione predefinita, il limite è impostato su 50 o impostato da QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="218"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Durante i reindirizzamenti, l&apos;API di accesso alla rete ha rilevato un reindirizzamento da un protocollo crittografato (https) a uno non crittografato (http).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="222"/>
        <source>An unknown network-related error was detected.</source>
        <translation>È stato rilevato un errore di rete sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="226"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>La connessione al server proxy è stata rifiutata (il server proxy non accetta richieste).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="230"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server proxy ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="234"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Non è stato trovato il nome host proxy (nome host proxy non valido).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="238"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>La connessione al proxy è scaduta oppure il proxy non ha risposto in tempo alla richiesta inviata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="242"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Per elaborare la richiesta il proxy richiede l&apos;autenticazione ma non ha accettato (se presenti) le credenziali offerte.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="246"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al proxy.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="250"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>L&apos;accesso al contenuto remoto è stato negato (simile all&apos;errore HTTP 403).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="254"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>L&apos;operazione richiesta sul contenuto remoto non è consentita.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="258"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Nel server non è stato trovato il contenuto remoto (simile all&apos;errore HTTP 404).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="262"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Per elaborare la richiesta il server remoto richiede l&apos;autenticazione ma le credenziali fornite (se presenti) non sono state accettate.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="266"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>La richiesta doveva essere inviata di nuovo, ma questo non è riuscito ad esempio perché non è stato possibile leggere i dati di caricamento una seconda volta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="270"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Impossibile completare la richiesta a causa di un conflitto con lo stato attuale della risorsa.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="274"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation>La risorsa richiesta non è più disponibile nel server.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="278"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al contenuto remoto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="282"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>L&apos;API di accesso alla rete non può soddisfare la richiesta perché il protocollo è sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="286"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>L&apos;operazione richiesta non è valida per questo protocollo.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="290"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>È stata rilevata un&apos;interruzione del protocollo (errore di analisi, risposte non valide o impreviste, ecc.).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="294"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Il server ha riscontrato una condizione imprevista che gli ha impedito di soddisfare la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="298"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Il server non supporta la funzionalità necessaria per soddisfare la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="302"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation>In questo momento il server non è in grado di gestire la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="306"/>
        <source>Unknown Server Error.</source>
        <translation>Errore server sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="310"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Il server remoto ha rifiutato la connessione (il server non accetta richieste).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="314"/>
        <source>Unknown Error.</source>
        <translation>Errore sconosciuto.</translation>
    </message>
</context>
</TS>
