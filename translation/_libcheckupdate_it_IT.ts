<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="it_IT">
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../checkupdate.cpp" line="53"/>
        <location filename="../checkupdate.cpp" line="74"/>
        <location filename="../checkupdate.cpp" line="145"/>
        <location filename="../checkupdate.cpp" line="259"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Nessuna connessione internet disponibile.
Verifica le impostazioni internet ed il firewall.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="35"/>
        <location filename="../checkupdate.cpp" line="51"/>
        <location filename="../checkupdate.cpp" line="75"/>
        <location filename="../checkupdate.cpp" line="93"/>
        <location filename="../checkupdate.cpp" line="102"/>
        <location filename="../checkupdate.cpp" line="111"/>
        <location filename="../checkupdate.cpp" line="143"/>
        <location filename="../checkupdate.cpp" line="178"/>
        <location filename="../checkupdate.cpp" line="251"/>
        <location filename="../checkupdate.cpp" line="260"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="91"/>
        <source>Your version of </source>
        <translation>La versione di </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="92"/>
        <source> is newer than the latest official version. </source>
        <translation> è più recente della versione ufficiale più recente. </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="101"/>
        <source>You have the latest version of </source>
        <translation>Hai la versione aggiornata di </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="110"/>
        <source>There was an error when the version was checked.</source>
        <translation>Si è verificato un errore durante il controllo versione.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="177"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Si è verificato un errore durante il controllo versione.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="222"/>
        <source>Updates:</source>
        <translation>Aggiornamenti:</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="245"/>
        <source>There is a new version of </source>
        <translation>È dispoibile una nuova versione di </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="247"/>
        <source>Latest version: </source>
        <translation>Versione aggiornata: </translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="178"/>
        <source>No error.</source>
        <translation>Nessun errore.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="182"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server remoto ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="186"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Non è stato trovato il nome host remoto (nome host non valido).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="190"/>
        <source>The connection to the remote server timed out.</source>
        <translation>Timeout della connessione verso il server remoto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="194"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>L&apos;operazione è stata annullata tramite chiamate a abort() o close() prima che fosse terminata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="198"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>L&apos;handshake SSL/TLS non è riuscito e non è stato possibile stabilire il canale crittografato.&lt;br&gt;Dovrebbe essere stato emesso il segnale sslErrors().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="202"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete, tuttavia il sistema ha avviato il roaming verso un altro punto di accesso.&lt;br&gt;La richiesta deve essere ripresentata e sarà elaborata non appena la connessione sarà ristabilita.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="206"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete o del mancato avvio della rete.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="210"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>La richiesta in background non è attualmente consentita a causa della politica della piattaforma.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="214"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>È stato raggiunto il limite massimo durante i reindirizzamenti.&lt;br&gt;Per impostazione predefinita, il limite è impostato su 50 o impostato da QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="218"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Durante i reindirizzamenti, l&apos;API di accesso alla rete ha rilevato un reindirizzamento da un protocollo crittografato (https) a uno non crittografato (http).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="222"/>
        <source>An unknown network-related error was detected.</source>
        <translation>È stato rilevato un errore di rete sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="226"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>La connessione al server proxy è stata rifiutata (il server proxy non accetta richieste).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="230"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server proxy ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="234"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Non è stato trovato il nome host proxy (nome host proxy non valido).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="238"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>La connessione al proxy è scaduta oppure il proxy non ha risposto in tempo alla richiesta inviata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="242"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Per elaborare la richiesta il proxy richiede l&apos;autenticazione ma non ha accettato (se presenti) le credenziali offerte.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="246"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al proxy.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="250"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>L&apos;accesso al contenuto remoto è stato negato (simile all&apos;errore HTTP 403).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="254"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>L&apos;operazione richiesta sul contenuto remoto non è consentita.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="258"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Nel server non è stato trovato il contenuto remoto (simile all&apos;errore HTTP 404).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="262"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Per elaborare la richiesta il server remoto richiede l&apos;autenticazione ma le credenziali fornite (se presenti) non sono state accettate.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="266"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>La richiesta doveva essere inviata di nuovo, ma questo non è riuscito ad esempio perché non è stato possibile leggere i dati di caricamento una seconda volta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="270"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Impossibile completare la richiesta a causa di un conflitto con lo stato attuale della risorsa.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="274"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation>La risorsa richiesta non è più disponibile nel server.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="278"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al contenuto remoto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="282"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>L&apos;API di accesso alla rete non può soddisfare la richiesta perché il protocollo è sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="286"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>L&apos;operazione richiesta non è valida per questo protocollo.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="290"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>È stata rilevata un&apos;interruzione del protocollo (errore di analisi, risposte non valide o impreviste, ecc.).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="294"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Il server ha riscontrato una condizione imprevista che gli ha impedito di soddisfare la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="298"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Il server non supporta la funzionalità necessaria per soddisfare la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="302"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation>In questo momento il server non è in grado di gestire la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="306"/>
        <source>Unknown Server Error.</source>
        <translation>Errore server sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="310"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Il server remoto ha rifiutato la connessione (il server non accetta richieste).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="314"/>
        <source>Unknown Error.</source>
        <translation>Errore sconosciuto.</translation>
    </message>
</context>
</TS>
