<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>SelectFont</name>
    <message>
        <location filename="../selectfont.ui" line="26"/>
        <location filename="../selectfont.cpp" line="26"/>
        <source>Select font</source>
        <translation>Seleziona font</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="139"/>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="160"/>
        <source>Bold</source>
        <translation>Grassettto</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="45"/>
        <source>Choose the type of font:&lt;br&gt;&quot;Monospace fonts&quot;, fonts where all characters are the same width. &lt;br&gt;&quot;Proportional fonts&quot;, fonts where the width varies depending on the character.</source>
        <translation>Scegli il tipo di carattere:&lt;br&gt;&quot;Caratteri monospazio&quot;, caratteri in cui tutti i caratteri hanno la stessa larghezza. &lt;br&gt;&quot;Caratteri proporzionali&quot;, caratteri la cui larghezza varia a seconda del carattere.</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="178"/>
        <source>Italic</source>
        <translation>Corsivo</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="197"/>
        <source>Bold and italic</source>
        <translation>Grassetto/corsivo</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="282"/>
        <source>Default</source>
        <translation>Predefiniti</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="314"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="28"/>
        <source>Select font size.</source>
        <translation>Seleziona la dimensione del font.</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="83"/>
        <source>All fonts</source>
        <translation>Tutte le font</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="83"/>
        <source>Monospaced fonts</source>
        <translation>Font monospazio</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="84"/>
        <source>Proportional fonts</source>
        <translation>Font proporzionali</translation>
    </message>
</context>
</TS>
