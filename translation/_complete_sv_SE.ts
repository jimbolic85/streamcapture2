<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>DownloadListDialog</name>
    <message>
        <location filename="../downloadlistdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="75"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="92"/>
        <location filename="../downloadlistdialog.cpp" line="106"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="109"/>
        <source>Save as...</source>
        <translation>Spara som...</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="107"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="108"/>
        <source>Save as text file</source>
        <translation>Spara som textfil</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="125"/>
        <source>Could not save the file.
Check your file permissions.</source>
        <translation>Det gick inte att spara filen.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="207"/>
        <source>Unable to find file</source>
        <translation>Kan inte hitta filen</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="208"/>
        <source>Unable to find</source>
        <translation>Kan inte hitta</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="126"/>
        <location filename="../downloadlistdialog.cpp" line="211"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../info.cpp" line="45"/>
        <source> streamCapture2 handles downloads of video streams.&lt;br&gt;</source>
        <translation> streamCapture2 hanterar nedladdningar av videoströmmar.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="46"/>
        <source>Many thanks to </source>
        <translation>Många tack till </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="40"/>
        <source>A graphical shell for </source>
        <translation>Ett grafiskt skal för </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="42"/>
        <source> and </source>
        <translation> och </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="37"/>
        <source> is free software, license </source>
        <translation> är fri mjukvara, licens </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="68"/>
        <source>Created:</source>
        <translation>Skapad:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="70"/>
        <source>Compiled on:</source>
        <translation>Kompilerad på:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="73"/>
        <source>Qt version:</source>
        <translation>Qt version:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="118"/>
        <source>Full version number:</source>
        <translation>Fullständigt versionsnummer:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="133"/>
        <source>Unknown compiler.</source>
        <translation>Okänd kompilator.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="148"/>
        <source>Revision:</source>
        <translation>Revision:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="166"/>
        <source>Phone: </source>
        <translation>Telefon: </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="178"/>
        <source>is located in</source>
        <translation>finns i</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="179"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="159"/>
        <source>Home page</source>
        <translation>Hemsida</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="47"/>
        <source> for the Italian translation. And for many good ideas that have made the program better.</source>
        <translation> för den italienska översättningen. Och för många bra idéer som har gjort programmet bättre.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="83"/>
        <source>for</source>
        <translation>för</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="85"/>
        <location filename="../info.cpp" line="101"/>
        <location filename="../info.cpp" line="122"/>
        <source>Compiler:</source>
        <translation>Kompilator:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="109"/>
        <source>compatible with</source>
        <translation>kompatibel med</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="161"/>
        <source>Source code</source>
        <translation>Källkod</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="163"/>
        <source>Wiki</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="119"/>
        <source>Micrsoft Visual C++ version </source>
        <translation>Micrsoft Visual C++ version </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="120"/>
        <source>&lt;br&gt;Full version number: </source>
        <translation>&lt;br&gt;Fullständigt versionsnummer: </translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../newprg.cpp" line="755"/>
        <location filename="../newprg.cpp" line="779"/>
        <source>Enter your password</source>
        <translation>Skriv in ditt lösenord</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="756"/>
        <location filename="../newprg.cpp" line="780"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Det är inte tillåtet med mellanslag. Använd endast skrivtecken som din streaminleverantör godkänner.
Lösenordet sparas inte.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="845"/>
        <source>svtplay-dl crashed.</source>
        <translation>svtplay-dl kraschade.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="851"/>
        <source>Could not stop svtplay-dl.</source>
        <translation>Kunde inte stoppa svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="862"/>
        <source>Delete any files that may have already been downloaded.</source>
        <translation>Ta bort alla filer som kan ha blivit nedladdade.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="859"/>
        <source>svtplay-dl stopped. Exit code </source>
        <translation>svtplay-dl stoppade. Exit kod </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="884"/>
        <source>Copy to: </source>
        <translation>Kopiera till: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1017"/>
        <source>Download to: </source>
        <translation>Ladda ner till: </translation>
    </message>
</context>
<context>
    <name>Newprg</name>
    <message>
        <location filename="../about.cpp" line="45"/>
        <source>Version history</source>
        <translation>Versionshistorik</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="55"/>
        <source>License streamCapture2</source>
        <translation>Licens streamCapture2</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="65"/>
        <source>License svtplay-dl</source>
        <translation>Licens svtplay-dl</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="75"/>
        <source>License FFmpeg</source>
        <translation>Licens FFmpeg</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="86"/>
        <source>License 7zip</source>
        <translation>Licens 7zip</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="102"/>
        <source> could not be found. Please download a portable streamCapture2 where FFmpeg is included.</source>
        <translation> kunde inte hittas. Ladda ner en portable streamCapture2 där FFmpeg ingår.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="107"/>
        <location filename="../about.cpp" line="111"/>
        <source>Or install </source>
        <translation>Eller installera </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="107"/>
        <location filename="../about.cpp" line="111"/>
        <source> in your system.</source>
        <translation> i ditt system.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="155"/>
        <location filename="../about.cpp" line="159"/>
        <source> You can download svtplay-dl from bin.ceicer.com. Select &quot;Tools&quot;, &quot;Download svtplay-dl...&quot;</source>
        <translation> Du kan ladda ner svtplay-dl från bin.ceicer.com. Välj &quot;Verktyg&quot;, &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="105"/>
        <source> could not be found. Please go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install FFmpeg.</source>
        <translation> kan inte hittas. Gå till &quot;Verktyg&quot;, Underhållsverktyg&quot; för att installera FFmpeg.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="110"/>
        <source> could not be found. Please download an AppImage where FFmpeg is included.</source>
        <translation> kunde inte hittas. Ladda ner en AppImage där FFmpeg ingår.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="139"/>
        <source>About FFmpeg</source>
        <translation>Om FFmpeg</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="153"/>
        <source>svtplay-dl is not found in the system path.</source>
        <translation>svtplay-dl finns inte i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="158"/>
        <source>svtplay-dl.exe is not found in the system path.</source>
        <translation>svtplay-dl.exe finns inte i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="201"/>
        <location filename="../about.cpp" line="210"/>
        <source> cannot be found. Go to &quot;Tools&quot;, &quot;Download svtplay-dl...&quot; to download.</source>
        <translation> kan inte hittas. Gå till &quot;Verktyg&quot;, &quot;Ladda ner svtplay-dl...&quot; för att ladda ner.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="203"/>
        <location filename="../about.cpp" line="212"/>
        <source>Please click &quot;Tools&quot; and select svtplay-dl.</source>
        <translation>Klicka på &quot;Verktyg&quot; och välj svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="227"/>
        <source>version </source>
        <translation>version </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="162"/>
        <location filename="../about.cpp" line="217"/>
        <location filename="../about.cpp" line="252"/>
        <source>About svtplay-dl</source>
        <translation>Om svtplay-dl</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="35"/>
        <location filename="../language.cpp" line="74"/>
        <location filename="../language.cpp" line="113"/>
        <source>Restart Now</source>
        <translation>Starta om nu</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="38"/>
        <location filename="../language.cpp" line="77"/>
        <location filename="../language.cpp" line="116"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Programmet måste startas om för att de nya språkinställningarna ska börja gälla.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="312"/>
        <location filename="../download.cpp" line="492"/>
        <location filename="../download_runtime.cpp" line="29"/>
        <location filename="../downloadall.cpp" line="149"/>
        <location filename="../downloadall.cpp" line="245"/>
        <location filename="../downloadall.cpp" line="290"/>
        <location filename="../downloadallepisodes.cpp" line="45"/>
        <location filename="../downloadallepisodes.cpp" line="62"/>
        <location filename="../downloadallepisodes.cpp" line="80"/>
        <location filename="../language.cpp" line="34"/>
        <location filename="../language.cpp" line="73"/>
        <location filename="../language.cpp" line="112"/>
        <location filename="../newprg.cpp" line="904"/>
        <location filename="../newprg.cpp" line="954"/>
        <location filename="../newprg.cpp" line="1211"/>
        <location filename="../newprg.cpp" line="1653"/>
        <location filename="../offline_installer.cpp" line="38"/>
        <location filename="../paytv_create.cpp" line="64"/>
        <location filename="../paytv_create.cpp" line="99"/>
        <location filename="../paytv_create.cpp" line="132"/>
        <location filename="../paytv_edit.cpp" line="31"/>
        <location filename="../paytv_edit.cpp" line="116"/>
        <location filename="../paytv_edit.cpp" line="147"/>
        <location filename="../paytv_edit.cpp" line="207"/>
        <location filename="../save.cpp" line="36"/>
        <location filename="../st_create.cpp" line="28"/>
        <location filename="../st_create.cpp" line="36"/>
        <location filename="../st_edit.cpp" line="32"/>
        <location filename="../st_edit.cpp" line="110"/>
        <location filename="../st_edit.cpp" line="125"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="350"/>
        <source>Up and running</source>
        <oldsource>Upp and running</oldsource>
        <translation>Uppe och kör</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="137"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="66"/>
        <source>Open the folder where streamCapture2 is located</source>
        <translation>Öppna mappen där streamCapture2 finns</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="70"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Ta en skärmdump (5 sekunders fördröjning)</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="74"/>
        <source>Read message from the developer (if there is any)</source>
        <translation>Läs meddelande från utvecklaren (om det finns något)</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="79"/>
        <source>Force update</source>
        <translation>Tvinga en uppdatering</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="95"/>
        <source>streamCapture2 is not allowed to open </source>
        <translation>streamCapture2 får inte öppna </translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="95"/>
        <source> Use the file manager instead.</source>
        <translation> Använd filhanteraren i stället.</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="38"/>
        <source>Save a screenshot</source>
        <translation>Spara en skärmdump</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="40"/>
        <source>Images (*.png)</source>
        <translation>bildfiler (*.png)</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="138"/>
        <source>Update this AppImage to the latest version</source>
        <translation>Uppdatera den här AppImagen till senaste version</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="309"/>
        <source>Please click on &quot;Tools&quot; and &quot;Maintenance Tool...&quot;</source>
        <translation>Klicka på &quot;Verktyg&quot; och &quot;Underhållsverktyg...&quot;</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="414"/>
        <source>Resolution</source>
        <translation>Upplösning</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="475"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download an AppImage that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;FFmpeg kan inte hittas eller är inte ett körbart program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Ladda ner en AppImage som innehåller FFmpeg.&lt;br&gt;&lt;br&gt;Eller installera FFmpeg i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="486"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download a portable streamCapture2 that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or download and place ffmpeg.exe in the same folder as streamapture2.exe.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;FFmpeg kan inte hittas eller är inget körbart program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Ladda ner en portable streamCapture2 som innehåller FFmpeg.&lt;br&gt;&lt;br&gt;Eller ladda ner och placera ffmpeg.exe i samma mapp som streamcapture2.exe.&lt;br&gt;&lt;br&gt;Eller installera FFmpeg i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="489"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;FFmpeg kan inte hittas eller är inte ett körbart program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Gå till &quot;Verktyg&quot;, &quot;Underhållsverktyg&quot; för att installera.&lt;br&gt;&lt;br&gt;Eller installera FFmpeg i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="633"/>
        <source>Could not save a file to store Recent Search list.
Check your file permissions.</source>
        <translation>Det gick inte att spara en fil för att lagra listan med senaste sökningar.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="658"/>
        <source>Could not save a file to store the list of downloads.
Check your file permissions.</source>
        <translation>Det gick inte att spara en fil för att lagra nedladdningslistan.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="144"/>
        <source>The request is processed...
Preparing to download...</source>
        <translation>Begäran behandlas...
Förbereder sig för att ladda ner...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="263"/>
        <location filename="../downloadall.cpp" line="188"/>
        <source>Selected folder to copy to is </source>
        <translation>Vald mapp att kopiera till är </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="542"/>
        <location filename="../listallepisodes.cpp" line="53"/>
        <source>The request is processed...
Starting search...</source>
        <translation>Begäran behandlas ...
Startar att söka...</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="67"/>
        <location filename="../nfo.cpp" line="46"/>
        <location filename="../sok.cpp" line="74"/>
        <source>The search field is empty!</source>
        <translation>Sökrutan är tom!</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="71"/>
        <location filename="../sok.cpp" line="78"/>
        <source>Incorrect URL</source>
        <translation>Felaktig URL</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1569"/>
        <source>Can not find any video streams, please check the address.

</source>
        <translation>Det går inte att hitta några videoströmmar , kontrollera adressen.

</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1523"/>
        <source>Downloading...</source>
        <translation>Laddar ner...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="338"/>
        <source>Starts downloading: </source>
        <translation>Startar nedladdningen: </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="346"/>
        <location filename="../downloadall.cpp" line="459"/>
        <location filename="../downloadallepisodes.cpp" line="283"/>
        <source>Unable to find any streams with the selected video resolution.</source>
        <translation>Det gick inte att hitta några strömmar med den valda videoupplösningen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="381"/>
        <location filename="../downloadall.cpp" line="485"/>
        <location filename="../downloadallepisodes.cpp" line="313"/>
        <source>Removing old files, if there are any...</source>
        <translation>Tar bort gamla filer, om det finns några...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="397"/>
        <location filename="../download.cpp" line="400"/>
        <source>The download failed.</source>
        <translation>Nedladdningen misslyckades.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="494"/>
        <location filename="../downloadall.cpp" line="292"/>
        <source>Save as...</source>
        <translation>Spara som...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="498"/>
        <location filename="../downloadall.cpp" line="296"/>
        <source>Select file name</source>
        <translation>Välj filnamn</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="520"/>
        <location filename="../downloadallepisodes.cpp" line="370"/>
        <source>No folder is selected</source>
        <translation>Ingen mapp är vald</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="257"/>
        <location filename="../listallepisodes.cpp" line="109"/>
        <location filename="../sok.cpp" line="103"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Det är inte tillåtet med mellanslag. Använd endast skrivtecken som din streaminleverantör godkänner.
Lösenordet sparas inte.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="29"/>
        <location filename="../downloadall.cpp" line="33"/>
        <source>cannot be found or is not an executable program.</source>
        <translation>kan inte hittas eller är inte ett körbart program.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="30"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl.exe.</source>
        <translation>Klicka på &quot;Verktyg&quot; och välj svtplay-dl.exe.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="141"/>
        <source>You have chosen to download more than one file with the same name. In order not to overwrite files, folders will be created for each file.</source>
        <translation>Du har valt att ladda ner mer än en fil med samma namn. För att inte skriva över filer skapas mappar för varje fil.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="204"/>
        <source>The video streams are saved in</source>
        <translation>Videoströmmarna sparas i</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="416"/>
        <location filename="../downloadall.cpp" line="421"/>
        <source>The download failed.
If a username and password or &apos;st&apos; cookie is required, you must enter these.</source>
        <translation>Nedladdningen misslyckades.
Om ett användarnamn och lösenord eller &apos;st&apos; cookie krävs måste du ange dessa.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="30"/>
        <location filename="../downloadallepisodes.cpp" line="27"/>
        <location filename="../listallepisodes.cpp" line="28"/>
        <location filename="../newprg.cpp" line="208"/>
        <location filename="../setgetconfig.cpp" line="543"/>
        <location filename="../sok.cpp" line="31"/>
        <source> cannot be found or is not an executable program.</source>
        <translation> kan inte hittas eller är inget körbart program.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="34"/>
        <location filename="../downloadallepisodes.cpp" line="28"/>
        <location filename="../listallepisodes.cpp" line="29"/>
        <location filename="../setgetconfig.cpp" line="544"/>
        <location filename="../sok.cpp" line="32"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl.</source>
        <translation>Vänligen klicka på &quot;Verktyg&quot; och välj svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="31"/>
        <source>Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>Klicka på &quot;Verktyg och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="71"/>
        <source>The request is processed...</source>
        <translation>Begäran behandlas...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="71"/>
        <source>Preparing to download...</source>
        <translation>Förbereder för nedladdning...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="88"/>
        <source>Download streaming media to folder</source>
        <translation>Laddar ner strömmande media till mapp</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="189"/>
        <source>The video stream is saved in </source>
        <oldsource>The video file is saved in </oldsource>
        <translation>Videoströmmen sparas i </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="198"/>
        <location filename="../downloadall.cpp" line="63"/>
        <location filename="../downloadallepisodes.cpp" line="157"/>
        <source>The default folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation>Standardmappen för att ladda ner videoströmmar kan inte hittas.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="216"/>
        <source>You do not have the right to save to the default folder.
Download is interrupted.</source>
        <translation>Du har inte rätt att spara i standardmappen.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="238"/>
        <source>The folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation>Mappen för nedladdning av videoströmmar kan inte hittas.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="173"/>
        <location filename="../download.cpp" line="252"/>
        <location filename="../downloadall.cpp" line="77"/>
        <location filename="../downloadall.cpp" line="108"/>
        <location filename="../downloadall.cpp" line="182"/>
        <location filename="../downloadallepisodes.cpp" line="171"/>
        <location filename="../downloadallepisodes.cpp" line="200"/>
        <source>You do not have the right to save to the folder.
Download is interrupted.</source>
        <translation>Du har inte rätten att spara i mappen du valt.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="377"/>
        <location filename="../downloadall.cpp" line="481"/>
        <location filename="../downloadallepisodes.cpp" line="307"/>
        <source>Merge audio and video...</source>
        <translation>Slår samman ljud och video...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="409"/>
        <location filename="../download.cpp" line="418"/>
        <location filename="../downloadall.cpp" line="386"/>
        <location filename="../downloadall.cpp" line="393"/>
        <source>Download succeeded</source>
        <translation>Nedladdningen lyckades</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="473"/>
        <location filename="../download.cpp" line="478"/>
        <source>The download failed </source>
        <translation>Nedladdningen misslyckades </translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="170"/>
        <source>The default folder for copying video streams cannot be found.
Download is interrupted.</source>
        <translation>Standardmappen för att kopiera videoströmmar kan inte hittas.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="455"/>
        <location filename="../download.cpp" line="460"/>
        <location filename="../downloadall.cpp" line="431"/>
        <location filename="../downloadall.cpp" line="439"/>
        <location filename="../downloadallepisodes.cpp" line="351"/>
        <location filename="../downloadallepisodes.cpp" line="354"/>
        <location filename="../downloadallepisodes.cpp" line="359"/>
        <source>Download completed</source>
        <translation>Nedladdningen klar</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="44"/>
        <location filename="../downloadallepisodes.cpp" line="61"/>
        <location filename="../downloadallepisodes.cpp" line="79"/>
        <source>Download anyway</source>
        <translation>Ladda ner i all fall</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="37"/>
        <source>You have chosen to create folders for each file and have the files copied.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation>Du har valt att skapa mappar för varje fil och kopiera filerna.
Tyvärr är detta inte möjligt eftersom streamCapture2 inte känner till filnamnen.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="55"/>
        <source>You have chosen to have the files copied.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation>Du har valt att kopiera filerna.
Tyvärr är detta inte möjligt eftersom streamCapture2 inte känner till filnamnen.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="72"/>
        <source>You have chosen to create folders for each file.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation>Du har valt att skapa mappar för varje fil.
Tyvärr är detta inte möjligt eftersom streamCapture2 inte känner till filnamnen.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="124"/>
        <source>Once svtplay-dl has started downloading all episodes, streamCapture2 no longer has control.
If you want to cancel, you may need to log out or restart your computer.
You can try to cancel using the command
&quot;sudo killall python3&quot;

Do you want to start the download?</source>
        <translation>När svtplay-dl har börjat ladda ner alla avsnitt har streamCapture2 inte längre kontroll.
Om du vill avbryta kan du behöva logga ut eller starta om datorn.
Du kan försöka avbryta med kommandot
&quot;sudo killall python3&quot;

Vill du starta nedladdningen?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="182"/>
        <source>Download all episodes to folder</source>
        <translation>Ladda ner alla avsnitt till mappen</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="276"/>
        <source>Starts downloading all episodes: </source>
        <translation>Börjar ladda ner alla avsnitt: </translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="318"/>
        <source>The download failed. Did you forget to enter username and password?</source>
        <translation>Går inte att logga in. Har du glömt att ange användarnamn och lösenord?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="332"/>
        <source>Episode</source>
        <translation>Avsnitt</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="332"/>
        <source>of</source>
        <translation>av</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="344"/>
        <source>The media files (and if you have selected the subtitles) have been downloaded.</source>
        <translation>Mediefilerna (och om du har valt undertexterna) har laddats ner.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="369"/>
        <source>Preparing to download</source>
        <translation>Förbereder att ladda ner</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="540"/>
        <location filename="../download.cpp" line="541"/>
        <location filename="../listallepisodes.cpp" line="51"/>
        <location filename="../listallepisodes.cpp" line="52"/>
        <source>Searching...</source>
        <translation>Söker...</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="147"/>
        <location filename="../sok.cpp" line="145"/>
        <source> crashed.</source>
        <translation> kraschade.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="149"/>
        <location filename="../sok.cpp" line="147"/>
        <source>Can not find any video streams, please check the address.</source>
        <translation>Kan inte hitta någon videoström, kontrollera adressen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="223"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="226"/>
        <source>bold and italic</source>
        <translation>fet och kursiv</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="229"/>
        <source>bold</source>
        <translation>fet</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="231"/>
        <source>italic</source>
        <translation>kursivt</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="235"/>
        <source>Current font:</source>
        <translation>Nuvarande teckensnitt:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="236"/>
        <source>size:</source>
        <translation>storlek:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="268"/>
        <source>View download list</source>
        <translation>Titta på nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="287"/>
        <source>Edit download list</source>
        <translation>Redigera nedladningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="296"/>
        <source>The list of video streams to download will be deleted and can not be restored.
Do you want to continue?</source>
        <translation>Listan över videoströmmar som ska laddas ned raderas och kan inte återställas.
Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="422"/>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update components&quot;.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="903"/>
        <location filename="../newprg.cpp" line="953"/>
        <location filename="../newprg.cpp" line="1210"/>
        <location filename="../save.cpp" line="35"/>
        <source>Choose</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="90"/>
        <location filename="../newprg.cpp" line="955"/>
        <source>Download streaming media to directory</source>
        <translation>Ladda ner strömmande media till mappen</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1151"/>
        <location filename="../newprg.cpp" line="1161"/>
        <source>Cannot find </source>
        <translation>Kan inte hitta </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1212"/>
        <location filename="../newprg.cpp" line="1231"/>
        <source>Select svtplay-dl</source>
        <translation>Välj svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1231"/>
        <source>*.exe (svtplay-dl.exe)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1250"/>
        <source>svtplay-dl is not an executable program.</source>
        <translation>svtplay-dl är inget körbart program.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1591"/>
        <source>The search is complete</source>
        <translation>Sökningen är klar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1593"/>
        <source>The search failed</source>
        <translation>Sökningen misslyckades</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1669"/>
        <source>Edit saved searches...</source>
        <translation>Redigera sparade sökningar...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="492"/>
        <source>Path to svtplay-dl: </source>
        <translation>Sökväg till svtplay-dl: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="496"/>
        <source>Path to svtplay-dl.exe: </source>
        <translation>Sökväg till svtplay-dl.exe: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="467"/>
        <source>svtplay-dl is in the system path.</source>
        <translation>svtplay-dl finns i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="144"/>
        <source>svtplay-dl is not found in the system path.
You can download any svtplay-dl of your choice.</source>
        <translation>svtplay-dl hittas inte i systemsökvägen.
Du kan ladda ner vilken svtplay-dl du vill.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="417"/>
        <source>Please click on &quot;Tools&quot; and &quot;Update&quot;</source>
        <translation>Klicka på &quot;Verktyg&quot; och &quot;Uppdatera&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="472"/>
        <source>svtplay-dl.exe is in the system path.</source>
        <translation>svtplay-dl.exe finns i systemsökvägenvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="478"/>
        <source>ERROR! svtplay-dl is not in the system path.</source>
        <translation>FEL! svtplay-dl finns inte i systemsökvägenvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="482"/>
        <source>ERROR! svtplay-dl.exe is not in the system path.</source>
        <translation>FEL! svtplay-dl.exe finns inte i systemsökvägenvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="503"/>
        <source>ERROR! svtplay-dl could not be found.</source>
        <translation>FEL! svtplay-dl kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="507"/>
        <source>ERROR! svtplay-dl.exe could not be found.</source>
        <translation>FEL! svtplay-dl.exe kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="524"/>
        <source>ERROR! FFmpeg cannot be found.</source>
        <translation>FEL! FFmpeg kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="905"/>
        <source>Copy streaming media to directory</source>
        <translation>Kopiera mediafilen till folder</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1326"/>
        <source>This is an experimental version. This AppImage cannot be updated.</source>
        <translation>Detta är en experimentell version. Den här AppImagen går inte att uppdatera.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1326"/>
        <source>You can find more experimental versions</source>
        <translation>Du kan hitta fler experimentella versioner</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1405"/>
        <location filename="../newprg.cpp" line="1429"/>
        <source>Failed to create desktop shortcut.
Check your file permissions.</source>
        <translation>Misslyckades med att skapa en genväg på skrivbordet.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="37"/>
        <location filename="../st_edit.cpp" line="126"/>
        <source>Enter &apos;st&apos; cookie</source>
        <translation>Ange &apos;st&apos; cookie</translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="39"/>
        <location filename="../st_edit.cpp" line="129"/>
        <source>Enter the &apos;st&apos; cookie that your video stream provider has saved in your browser.</source>
        <translation>Ange &apos;st&apos;-cookien som vidioströmleverantören har sparat i din webbläsare.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="304"/>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Uppdatera&quot; för att uppdatera.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="425"/>
        <location filename="../setgetconfig.cpp" line="312"/>
        <source>Download a new</source>
        <translation>Ladda ner en ny</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="528"/>
        <source>Path to ffmpeg.exe: </source>
        <translation>Sökvägen till ffmpeg.exe: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="531"/>
        <source>Path to ffmpeg: </source>
        <translation>Sökvägen till ffmpeg: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="922"/>
        <location filename="../newprg.cpp" line="972"/>
        <source>You do not have the right to save to</source>
        <translation>Du har inte rättighet att spara i</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="922"/>
        <source>and it cannot be used as the default folder to copy to.</source>
        <translation>och den kan inte användas som standardfolder att kopiera till.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="972"/>
        <source>and it cannot be used as the default folder for downloads.</source>
        <translation>och den kan inte användas som standardfolder för nedladdningar.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1151"/>
        <location filename="../newprg.cpp" line="1161"/>
        <source> in system path.</source>
        <translation> i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1253"/>
        <source>svtplay-dl.exe is not an executable program.</source>
        <translation>svtplay-dl.exe är inte ett körbart program.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1326"/>
        <source>here</source>
        <translation>här</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1362"/>
        <source>Maintenance Tool cannot be found.
Only if you install</source>
        <translation>Underhållsverktyget kan inte hittas.
Endast om du installerar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1364"/>
        <source>is it possible to update and uninstall the program.</source>
        <translation>är det möjligt att uppdatera och avinstallera programmet.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1600"/>
        <source>Select</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1620"/>
        <source>Click to copy to the search box.</source>
        <translation>Klicka för att kopiera till sökrutan.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1643"/>
        <source>The number of previous searches to be saved...</source>
        <oldsource>The number of recently opened files to be displayed...</oldsource>
        <translation>Antalet tidigare sökningar som ska sparas...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1645"/>
        <source>Specify how many previous searches you want to save. If the number of searches exceeds the specified number, the oldest search is deleted.</source>
        <translation>Bestäm antalet tidigare sökningar som ska sparas. Om antalet sökningar överstiger det angivna antalet kommer den äldsta sökningen att tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1678"/>
        <source>Edit saved searches</source>
        <translation>Redigera sparade sökningar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1722"/>
        <source>All your saved settings will be deleted.
All lists of files to download will disappear.
Do you want to continue?</source>
        <translation>Alla dina sparade inställningar raderas.
Alla listor över filer som ska laddas ned försvinner.
Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1768"/>
        <source>Failed to delete your configuration files.
Check your file permissions.</source>
        <translation>Det gick inte att ta bort konfigurationsfilerna.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1781"/>
        <source>was normally terminated. Exit code = </source>
        <translation>avslutades normalt. Exit kod = </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1785"/>
        <source>crashed. Exit code = </source>
        <translation>kraschade. Exit kod = </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1672"/>
        <source>Click to edit all saved searches.</source>
        <translation>Klicka för att redigera alla sparade sökningar.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="95"/>
        <source>Edit streamCapture2 settings</source>
        <translation>Redigera inställningarna för streamCapture2</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="119"/>
        <source>Edit download svtplay-dl settings</source>
        <translation>Redigera inställningarna för ladda ner svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="209"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl...</source>
        <translation>Klicka på &quot;Verktyg&quot; och välj svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1069"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Go to &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-d.exel kan inte hittas eller är inte ett körbart program.
Klicka på &quot;Verktyg&quot; och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1080"/>
        <source>svtplay-dl.exe cannot be found or is not an executable program.
Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-dl.exe kan inte hittas eller är inget körbart program.
Klicka på &quot;Verktyg&quot; och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1119"/>
        <source>svtplay-dl cannot be found or is not an executable program.
 Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-dl kan inte hittas eller är inget körbart program.
Klicka på &quot;Verktyg&quot; och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1128"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-dl kan inte hittas eller är inget körbart program.
Klicka på &quot;Verktyg&quot; och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1287"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Click &quot;Tools&quot;, &quot;Select svtplay-dl...&quot;
to select svtplay-dl.</source>
        <translation>svtplay-dl kan inte hittas eller är inte ett körbart program.
Klicka &quot;Verktyg&quot;, &quot;Välj svtplay-dl...&quot;
för att välja svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1517"/>
        <source>Best quality is selected automatically</source>
        <translation>Bästa kvalite väljs automatiskt</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1518"/>
        <source>Best method is selected automatically</source>
        <translation>Bästa metod väljs automatiskt</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1684"/>
        <source>Remove all saved searches</source>
        <translation>Ta bort alla sparade sökningar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1687"/>
        <source>Click to delete all saved searches.</source>
        <translation>Klicka för att ta bort alla sparade sökningar.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1660"/>
        <source>The number of searches to be saved: </source>
        <oldsource>Set number of Recent Files: </oldsource>
        <translation>Ange maxantalet sparade sökningar: </translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="100"/>
        <location filename="../paytv_edit.cpp" line="118"/>
        <source>Enter your username</source>
        <translation>Skriv in ditt användarnamn</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="313"/>
        <location filename="../downloadall.cpp" line="246"/>
        <location filename="../downloadallepisodes.cpp" line="256"/>
        <location filename="../listallepisodes.cpp" line="108"/>
        <location filename="../paytv_create.cpp" line="133"/>
        <location filename="../paytv_edit.cpp" line="150"/>
        <location filename="../sok.cpp" line="102"/>
        <source>Enter your password</source>
        <translation>Skriv in ditt lösenord</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="56"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="76"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="92"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="164"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="282"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="302"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="318"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="425"/>
        <location filename="../coppytodefaultlocation.cpp" line="40"/>
        <location filename="../download.cpp" line="177"/>
        <location filename="../download.cpp" line="202"/>
        <location filename="../download.cpp" line="220"/>
        <location filename="../download.cpp" line="242"/>
        <location filename="../download.cpp" line="256"/>
        <location filename="../download.cpp" line="311"/>
        <location filename="../download.cpp" line="493"/>
        <location filename="../downloadall.cpp" line="67"/>
        <location filename="../downloadall.cpp" line="81"/>
        <location filename="../downloadall.cpp" line="112"/>
        <location filename="../downloadall.cpp" line="148"/>
        <location filename="../downloadall.cpp" line="174"/>
        <location filename="../downloadall.cpp" line="183"/>
        <location filename="../downloadall.cpp" line="244"/>
        <location filename="../downloadall.cpp" line="291"/>
        <location filename="../downloadallepisodes.cpp" line="161"/>
        <location filename="../downloadallepisodes.cpp" line="174"/>
        <location filename="../downloadallepisodes.cpp" line="204"/>
        <location filename="../newprg.cpp" line="145"/>
        <location filename="../newprg.cpp" line="923"/>
        <location filename="../newprg.cpp" line="975"/>
        <location filename="../newprg.cpp" line="1073"/>
        <location filename="../newprg.cpp" line="1083"/>
        <location filename="../newprg.cpp" line="1121"/>
        <location filename="../newprg.cpp" line="1129"/>
        <location filename="../newprg.cpp" line="1154"/>
        <location filename="../newprg.cpp" line="1164"/>
        <location filename="../newprg.cpp" line="1257"/>
        <location filename="../newprg.cpp" line="1292"/>
        <location filename="../newprg.cpp" line="1327"/>
        <location filename="../newprg.cpp" line="1367"/>
        <location filename="../newprg.cpp" line="1408"/>
        <location filename="../newprg.cpp" line="1432"/>
        <location filename="../newprg.cpp" line="1652"/>
        <location filename="../newprg.cpp" line="1770"/>
        <location filename="../nfo.cpp" line="56"/>
        <location filename="../nfo.cpp" line="66"/>
        <location filename="../nfo.cpp" line="76"/>
        <location filename="../nfo.cpp" line="88"/>
        <location filename="../nfo.cpp" line="158"/>
        <location filename="../offline_installer.cpp" line="61"/>
        <location filename="../paytv_create.cpp" line="63"/>
        <location filename="../paytv_create.cpp" line="98"/>
        <location filename="../paytv_create.cpp" line="131"/>
        <location filename="../paytv_edit.cpp" line="115"/>
        <location filename="../paytv_edit.cpp" line="146"/>
        <location filename="../paytv_edit.cpp" line="206"/>
        <location filename="../setgetconfig.cpp" line="96"/>
        <location filename="../setgetconfig.cpp" line="478"/>
        <location filename="../setgetconfig.cpp" line="491"/>
        <location filename="../setgetconfig.cpp" line="637"/>
        <location filename="../setgetconfig.cpp" line="661"/>
        <location filename="../shortcuts.cpp" line="69"/>
        <location filename="../shortcuts.cpp" line="145"/>
        <location filename="../st_create.cpp" line="27"/>
        <location filename="../st_create.cpp" line="35"/>
        <location filename="../st_edit.cpp" line="109"/>
        <location filename="../st_edit.cpp" line="124"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="316"/>
        <location filename="../downloadall.cpp" line="249"/>
        <location filename="../paytv_create.cpp" line="102"/>
        <location filename="../paytv_create.cpp" line="136"/>
        <location filename="../paytv_edit.cpp" line="120"/>
        <location filename="../paytv_edit.cpp" line="152"/>
        <source>Spaces are not allowed. Use only the characters
your streaming provider approves.</source>
        <translation>Mellanslag är inte tillåtna. Använd endast tecken
som din leverantör godkänner.</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="65"/>
        <location filename="../paytv_edit.cpp" line="208"/>
        <location filename="../st_create.cpp" line="29"/>
        <location filename="../st_edit.cpp" line="111"/>
        <source>Streaming service</source>
        <translation>Strömningstjänst</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="67"/>
        <location filename="../paytv_edit.cpp" line="210"/>
        <location filename="../st_create.cpp" line="31"/>
        <location filename="../st_edit.cpp" line="114"/>
        <source>Enter the name of your streaming service.</source>
        <translation>Ange namnet på din strömningstjänst.</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="147"/>
        <location filename="../paytv_edit.cpp" line="162"/>
        <source>Save password?</source>
        <translation>Spara lösenordet?</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="149"/>
        <location filename="../paytv_edit.cpp" line="164"/>
        <source>Do you want to save the password? (unsafe)?</source>
        <translation>Vill du spara lösenordet (osäkert)?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="128"/>
        <location filename="../newprg.cpp" line="304"/>
        <location filename="../newprg.cpp" line="1724"/>
        <location filename="../paytv_create.cpp" line="155"/>
        <location filename="../paytv_edit.cpp" line="170"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="129"/>
        <location filename="../newprg.cpp" line="305"/>
        <location filename="../newprg.cpp" line="1725"/>
        <location filename="../paytv_create.cpp" line="156"/>
        <location filename="../paytv_edit.cpp" line="171"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="26"/>
        <source>Manage Login details for </source>
        <translation>Hantera inloggningsuppgifter för </translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="27"/>
        <source>Edit, rename or delete
</source>
        <translation>Redigera, byt namn eller ta bort
</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="28"/>
        <location filename="../st_edit.cpp" line="30"/>
        <source>Edit</source>
        <translation>Redigera</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="29"/>
        <source>Rename</source>
        <translation>Byt namn</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="30"/>
        <location filename="../st_edit.cpp" line="31"/>
        <source>Delete</source>
        <oldsource>Delet</oldsource>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="58"/>
        <source>Create New</source>
        <translation>Skapa ny</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="67"/>
        <location filename="../setgetconfig.cpp" line="420"/>
        <source>No Password</source>
        <translation>Inget lösenord</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="37"/>
        <source>You have not selected any place to copy the media files.
Please select a location before proceeding.</source>
        <translation>Du har inte valt någon plats att kopiera mediafilerna till.
Var vänlig och bestäm en plats innan du fortsätter.</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="63"/>
        <source>A file with the same name already exists. The file will not be copied.</source>
        <translation>Det finns redan en fil med samma namn. Filen kopieras inte.</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="70"/>
        <location filename="../coppytodefaultlocation.cpp" line="94"/>
        <source>Copy succeeded</source>
        <translation>Kopieringen lyckades</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="74"/>
        <location filename="../coppytodefaultlocation.cpp" line="98"/>
        <source>Copy failed</source>
        <translation>Kopieringen misslyckades</translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="29"/>
        <source>The mission failed!</source>
        <translation>Uppdraget misslyckades!</translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="32"/>
        <source>Mission accomplished!</source>
        <translation>Uppdrag slutfört!</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="68"/>
        <source>Failure!
The shortcut could not be created in
&quot;~/.local/share/applications&quot;
Check your file permissions.</source>
        <translation>Fel!
Genvägen kunde inte skapas i
&quot;~/.local/share/applikationer&quot;
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="120"/>
        <location filename="../shortcuts.cpp" line="122"/>
        <source>Download video streams.</source>
        <translation>Ladda ner videoströmmar.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="144"/>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation>Fel!
Genvägen kunde inte skapas.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="34"/>
        <source>Compiled language file (*.qm)</source>
        <translation>Kompilerade språkfil (*.qm)</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="34"/>
        <source>Open your language file</source>
        <translation>Öppna din språkfil</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="25"/>
        <source>Manage &apos;st&apos; cookie for </source>
        <translation>Hantera &apos;st&apos; cookie för </translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="27"/>
        <source>Use, Do not use, Edit or Delete
&apos;st&apos; cookie for
</source>
        <translation>Använd, Använd inte, Redigera eller Ta bort
&apos;st&apos; cookie för
</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="28"/>
        <source>Use</source>
        <translation>Använd</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="29"/>
        <source>Do not use</source>
        <translation>Använd inte</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="176"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Ställ in en ny &apos;st&apos;-cookie</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="177"/>
        <source>Paste and save the &apos;st&apos; cookie that your streaming provider has downloaded to your browser.</source>
        <translation>Klistra in och spara &apos;st&apos; cookien som din streamingleverantör har laddat ner till din webbläsare.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="47"/>
        <source>Quality, Method, Codec, Resolution, Language and Role</source>
        <translation>Kvalitet, Metod, Codec, Upplösning, Språk och Roll</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="154"/>
        <source>Unable to log in. Forgot your username and password?</source>
        <translation>Går inte att logga in. Har du glömt att ange användarnamn och lösenord?</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="422"/>
        <location filename="../sok.cpp" line="200"/>
        <source>Deviation</source>
        <translation>Avvikelse</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="211"/>
        <source>ERROR: No videos found. Cant find video id for the video.</source>
        <translation>FEL: Inga videor hittades. Kan inte hitta video-id för videon.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="216"/>
        <source>ERROR: No videos found. Can&apos;t decode api request.</source>
        <translation>FEL: Inga videor hittades. Det går inte att avkoda api-begäran.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="46"/>
        <source>The information from svtplay-dl may or may not contain:</source>
        <translation>Informationen kan innehålla, eller inte innehålla:</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="31"/>
        <source>The font size changes to the selected font size</source>
        <translation>Teckenstorleken ändras till vald teckenstorlek</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="128"/>
        <source>You are using version</source>
        <translation>Du använder version</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="143"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="148"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="150"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="154"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="156"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="158"/>
        <source>Stable:</source>
        <translation>Stabila:</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="143"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="148"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="150"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="154"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="156"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="158"/>
        <source>You have selected the option</source>
        <translation>Du har valt alternativet</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="129"/>
        <source>You are NOT using the latest version.</source>
        <translation>Du använder INTE senaste versionen.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="29"/>
        <source>You have downloaded</source>
        <translation>Du har laddat ner</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="32"/>
        <source>Nothing</source>
        <translation>Ingenting</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="33"/>
        <source>To folder &quot;stable&quot;: Nothing.</source>
        <translation>Till mappen &quot;stable&quot;: Ingenting.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="35"/>
        <source>To folder &quot;stable&quot;: </source>
        <translation>Till mappen &quot;stable&quot;: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="40"/>
        <source>To folder &quot;beta&quot;: Nothing.</source>
        <translation>Till mappen &quot;beta&quot;: Ingenting.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="42"/>
        <source>To folder &quot;beta&quot;: </source>
        <translation>Till mappen &quot;beta&quot;: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="58"/>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl beta can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation>Ett oväntat fel inträffade.&lt;br&gt;Information om svtplay-dl beta kan inte hittas.&lt;br&gt;Kontrollera din internetanslutning.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="78"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="94"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="284"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="304"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="320"/>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl stable can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation>Ett oväntat fel inträffade.&lt;br&gt;Information om svtplay-dl beta kan inte hittas.&lt;br&gt;Kontrollera din internetanslutning.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="128"/>
        <source>of svtplay-dl.</source>
        <translation>av svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="132"/>
        <source>You are not using svtplay-dl.</source>
        <translation>Du använder inte svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="143"/>
        <source>Beta:</source>
        <translation>Beta:</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="143"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="148"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="150"/>
        <source>svtplay-dl available for download</source>
        <translation>svtplay-dl tillgänglig för nedladdning</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="154"/>
        <source>You are using the beta version.</source>
        <translation>Du använder betaversionen.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="148"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="156"/>
        <source>You are using the stable version.</source>
        <translation>Du använder den stabila versionen.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="154"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="156"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="158"/>
        <source>The latest svtplay-dl&lt;br&gt;available for download at bin.ceicer.com is</source>
        <translation>Den senaste svtplay-dl&lt;br&gt;tillgänglig för nedladdning från bin.ceicer.com är</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="226"/>
        <source>&quot;Use stable svtplay-dl.&quot;</source>
        <translation>&quot;Använd stabila svtplay-dl.&quot;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="228"/>
        <source>&quot;Use stable svtplay-dl&quot;, but it can not be found.</source>
        <translation>&quot;Använd stabila svtplay-dl&quot;, men den kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="247"/>
        <source>&quot;Use svtplay-dl beta.&quot;</source>
        <translation>&quot;Använd svtplay-dl beta&quot;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="249"/>
        <source>&quot;Use svtplay-dl beta&quot;, but it can not be found.</source>
        <translation>&quot;Använd svtplay-dl beta&quot;, men den kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="379"/>
        <source>&lt;b&gt;Welcome to streamCapture2!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Välkommen till streamCapture2!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="387"/>
        <source>&lt;b&gt;svtplay-dl stable&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation>&lt;b&gt;svtplay-dl stabila&lt;/b&gt; kan inte hittas. Du har angett en felaktig sökväg eller så har ingen sökväg angetts alls.&lt;br&gt;Eller så har filerna flyttats eller tagits bort.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="388"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="406"/>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Download to directory stable&quot;&lt;/i&gt;</source>
        <translation>&lt;i&gt;Klicka &quot;ladda ner&quot; och &quot;Ladda ner till mappen stable&quot;&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="394"/>
        <source>&lt;b&gt;svtplay-dl beta&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation>&lt;b&gt;svtplay-dl beta&lt;/b&gt;kan inte hittas. Du har angett en felaktig sökväg eller så har ingen sökväg angets.&lt;br&gt;Eller så har filerna blivit flyttade eller borttagna.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="395"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="417"/>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Download to directory beta&quot;&lt;/i&gt;</source>
        <translation>&lt;i&gt;Klicka &quot;Ladda ner&quot; och Ladda ner till mappen beta&quot;&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="403"/>
        <source>&lt;b&gt;svtplay-dl stable is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation>&lt;b&gt;svtplay-dl stabila finns för nedladdning. &lt;/b&gt;&lt;br&gt;Version: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="414"/>
        <source>&lt;b&gt;svtplay-dl beta is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation>&lt;b&gt;svtplay-dl beta finns för nedladdning. &lt;/b&gt;&lt;br&gt;Version: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="262"/>
        <source>&quot;Use the selected svtplay-dl.&quot;</source>
        <translation>&quot;Använd vald svtplay-dl.&quot;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="268"/>
        <source>svtplay-dl is not found in the specified path.</source>
        <translation>svtplay-dl hittas inte i den angivna sökvägen.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="264"/>
        <source>&quot;Use the selected svtplay-dl&quot;, but it can not be found.</source>
        <translation>&quot;Använd vald svtplay-dl&quot;, men den kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../download_runtime.cpp" line="27"/>
        <source>Only if svtplay-dl does not work do you need to install
&quot;Microsoft Visual C++ Redistributable&quot;.
Download and double-click to install.</source>
        <translation>Bara om svtplay-dl inte fungerar behöver du installera
&quot;Microsoft Visual C++ Redistributable&quot;.
Ladda ner och dubbelklicka för att installera.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="163"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="424"/>
        <location filename="../download_runtime.cpp" line="28"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="48"/>
        <source>NFO files contain release information about the media. No NFO file was found.</source>
        <translation>NFO-filer innehåller releaseinformation om media. Ingen NFO-fil hittades.</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="122"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="126"/>
        <source>Episode title:</source>
        <translation>Avsnitstitel:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="130"/>
        <source>Season:</source>
        <translation>Säsong:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="134"/>
        <source>Episode:</source>
        <translation>Avsnitt:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="138"/>
        <source>Plot:</source>
        <translation>Handling:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="142"/>
        <source>Published:</source>
        <translation>Publicerad:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="157"/>
        <source>An unexpected error occurred while downloading the file.</source>
        <translatorcomment>Ett oväntat fel inträffade när filen laddades ned.</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="176"/>
        <source>NFO Info</source>
        <translation>NFO Information</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="34"/>
        <source>To the website</source>
        <translation>Till webbsidan</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="40"/>
        <source>Uninstall</source>
        <translation>Avinstallera</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="42"/>
        <source>Download the new version</source>
        <translation>Ladda ner den nya versionen</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="37"/>
        <source>Download the latest version</source>
        <translation>Ladda ner den senaste versionen</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="63"/>
        <source>An unexpected error occurred.&lt;br&gt;</source>
        <translation>Ett oväntat fel inträffade.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="63"/>
        <source>&lt;br&gt;can not be found or is not an executable program.</source>
        <translation>&lt;br&gt;kan inte hittas eller är inget körbart program.</translation>
    </message>
</context>
<context>
    <name>newprg</name>
    <message>
        <location filename="../newprg.ui" line="123"/>
        <location filename="../newprg.ui" line="1047"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="431"/>
        <source>Method</source>
        <translation>Metod</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="161"/>
        <location filename="../newprg.ui" line="1074"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="793"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="35"/>
        <source>TEST</source>
        <translation>TEST</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="68"/>
        <source>Paste the link to the page where the video is displayed</source>
        <translation>Klistra in länken till sidan där videon visas</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="346"/>
        <source>Quality (Bitrate)</source>
        <oldsource>Bitrate</oldsource>
        <translation>Kvalitet (Bithastighet)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="416"/>
        <source>Media streaming communications protocol.</source>
        <translation>Protokoll för att strömma media.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="480"/>
        <source>Select quality on the video you download</source>
        <translation>Välj kvalitet på videon du laddar ner</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="71"/>
        <location filename="../newprg.ui" line="1059"/>
        <source>Paste</source>
        <translation>Klistra in</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="489"/>
        <source>Quality (bitrate) and method. Higher bitrate gives better quality and larger file.</source>
        <translation>Kvalitet (bithastighet) och metod. Högre bithastighet ger högre kvalitet och större fil.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="714"/>
        <location filename="../newprg.ui" line="1207"/>
        <source>Include Subtitle</source>
        <translation>Inkludera undertexten</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="158"/>
        <source>Download the file you just searched for.</source>
        <translation>Ladda ner filen som du just sökt efter.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="225"/>
        <source>Download all files you added to the list.</source>
        <translation>Ladda ner alla filer som du lagt till på listan.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="525"/>
        <source>Select quality on the video you download.</source>
        <translation>Välj kvalitet på videoströmmen du laddar ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="642"/>
        <source>Select provider. If yoy need a password.</source>
        <translation>Välj en tjänsteleverantör. Om du behöver lösenord.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="679"/>
        <location filename="../newprg.ui" line="1192"/>
        <location filename="../newprg.ui" line="1300"/>
        <source>Password</source>
        <translation>Lösenord</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="806"/>
        <source>&amp;Language</source>
        <translation>&amp;Språk</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="850"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="900"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjälp</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="918"/>
        <source>&amp;Recent</source>
        <oldsource>Recent</oldsource>
        <translation>S&amp;enaste</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="949"/>
        <source>&apos;&amp;st&apos; cookies</source>
        <translation>&apos;&amp;st&apos; cookies</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="978"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="987"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="854"/>
        <source>Check for updates at program start</source>
        <translation>Sök efter uppdateringar när programmet startar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="94"/>
        <source>https://</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="228"/>
        <location filename="../newprg.ui" line="1245"/>
        <source>Download all on the list</source>
        <translation>Ladda ner alla på listan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="602"/>
        <source>Allows given quality to differ by an amount. 300 usually works well. (Bit rate +/- 300).</source>
        <translation>Tillåter given kvalitet att skilja sig åt med en mängd. 300 brukar fungera bra. (Bithastighet +/- 300).</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1014"/>
        <source>Exit</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1017"/>
        <source>Exits the program.</source>
        <translation>Avsluta programmet.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1020"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1086"/>
        <source>License streamCapture2...</source>
        <translation>Licens streamCapture2...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1162"/>
        <source>Italian</source>
        <translation>Italienska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1180"/>
        <source>Create new user</source>
        <translation>Skapa ny användare</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1256"/>
        <source>Create folder &quot;method_quality_amount_resolution&quot;</source>
        <translation>Skapa folder &quot;metod_kvalitet_belopp_upplösning&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1285"/>
        <source>View more information from svtplay-dl. Appears in purple text color.</source>
        <translation>Visa mer information från svtplay-dl. Visas med lila text.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1363"/>
        <source>Copy to Selected Location</source>
        <translation>Kopiera till vald plats</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1366"/>
        <source>Direct copy to the default copy location.</source>
        <translation>Direkt kopiering till förvald folder.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1381"/>
        <source>Save the location where the finished video file is copied. If you use &quot;Direct download of all...&quot; no files are ever copied.</source>
        <translation>Spara platsen ditr den färdiga videofilen kopieras. Om du använder &quot;Direktnedladdning av alla...&quot; kopieras inga filer.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1412"/>
        <source>Add all Episodes to Download List</source>
        <translation>Lägg till alla avsnitt till nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1415"/>
        <source>Adds all episodes to the download list.</source>
        <translation>Lägger till alla avsnitt till nedladdningslistan.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1467"/>
        <source>Use svtplay-dl beta</source>
        <translation>Använd svtplay-dl beta</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1481"/>
        <source>Use svtplay-dl stable</source>
        <translation>Använd stabila svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1492"/>
        <source>Use svtplay-dl from the system path</source>
        <translation>Använd svtplay-dl från systemsökvägen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1507"/>
        <source>Select svtplay-dl that you have in your computer.</source>
        <translation>Välj svtplay-dl som du har i din dator.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1515"/>
        <source>Use the selected svtplay-dl</source>
        <translation>Använd vald svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1518"/>
        <source>Use svtplay-dl that you selected in your computer.</source>
        <translation>Använd den svtplay-dl som du valt i din dator.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1526"/>
        <source>Do not show notifications</source>
        <translation>Visa inte aviseringar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1529"/>
        <source>Do not show notifications when the download is complete.</source>
        <translation>Visa inte aviseringar när nedladdningen är klar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1541"/>
        <source>Create a shortcut</source>
        <translation>Skapa en genväg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1552"/>
        <source>Desktop Shortcut</source>
        <translation>Skrivbordsgenväg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1555"/>
        <source>Create a shortcut to streamCapture2 on the desktop.</source>
        <translation>Skapa genväg på skrivbordet till streamCapture2.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1566"/>
        <source>Create a shortcut to streamCapture2 in the operating system menu.</source>
        <translation>Skapa genväg till streamCapture2 i operativsystemets meny.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1574"/>
        <source>Useful when testing your own translation.</source>
        <translation>Användbart när du testar din egen översättning.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1582"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Ange ny &apos;st&apos; cookie</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1698"/>
        <source>streamCapture2 settings</source>
        <translation>Inställningar för streamCapture2</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1703"/>
        <source>download svtplay-dl settings</source>
        <translation>Inställningar för ladda ner svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1712"/>
        <source>NFO info</source>
        <translation>NFO information</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1715"/>
        <source>NFO files contain media release information. Available at svtplay.se, among other places.</source>
        <translation>NFO-filer innehåller releaseinformation om media. Tillgänglig bland annat på svtplay.se.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1723"/>
        <source>Select file name</source>
        <translation>Välj filnamn</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1726"/>
        <source>You choose the name of the downloaded video file.</source>
        <translation>Du väljer namnet på den nedladdade videofilen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="861"/>
        <source>Edit settings (Advanced)</source>
        <translation>Redigera inställningarna (avancerat)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="565"/>
        <source>Video resolution: 480p=640x480, 720p=1280x720, 1080p=1920x1080</source>
        <translation>Videoupplösning: 480p=640x480, 720p=1280x720, 1080p=1920x1080</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1591"/>
        <source>Download svtplay-dl...</source>
        <translation>Ladda ner svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1594"/>
        <source>Download and decompress svtplay-dl from bin.ceicer.com</source>
        <translation>Ladda ner och dekomprimera svtplay-dl från bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1606"/>
        <source>Zoom In</source>
        <translation>Zooma in</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1609"/>
        <source>Increase the font size.</source>
        <translation>Öka teckenstorleken.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1612"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1617"/>
        <source>Zoom Default</source>
        <translation>Zooma som standard</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1620"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1629"/>
        <source>Zoom Out</source>
        <translation>Zooma ut</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1632"/>
        <source>Decrease the font size.</source>
        <translation>Minska teckenstorleken.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1635"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1644"/>
        <source>Check for the latest svtplay-dl from bin.ceicer.com...</source>
        <translation>Sök efter senaste svtplay-dl från bin.ceicer.com...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1653"/>
        <location filename="../newprg.ui" line="1656"/>
        <location filename="../newprg.ui" line="1659"/>
        <source>Download Microsoft runtime (required for svtplay-dl)...</source>
        <translation>Ladda ner Microsoft runtime (krävs för svtplay-dl)...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1662"/>
        <source>Download the runtime file from bin.ceicer.com.</source>
        <translation>Ladda ner runtimefilen från bin.ceicer.com.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1674"/>
        <source>License 7zip...</source>
        <translation>Licens 7zip...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1685"/>
        <source>Check streamCapture2 for updates at start</source>
        <translation>Kolla om det finns uppdateringar till streamCapture2 vid start</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1693"/>
        <source>Check for new versions of svtplay-dl at start</source>
        <translation>Kolla efter nya versioner av svtplay-dl vid start</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1563"/>
        <source>Applications menu Shortcut</source>
        <translation>Genväg i programmenyn</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1571"/>
        <source>Load external language file...</source>
        <translation>Ladda extern språkfil...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1390"/>
        <source>Select Default Download Location...</source>
        <translation>Välj standard nedladdningsplats...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1319"/>
        <source>Direct Download of all Episodes</source>
        <translation>Direkt nedladdning av alla avsnitt</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1322"/>
        <source>Trying to immediately download all episodes. Unable to create folders or select quality.</source>
        <translation>Försöker omedelbart att ladda ner alla avsnitt. Det går inte att skapa mappar eller välja kvalitet.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1344"/>
        <source>List all Episodes</source>
        <translation>Lista alla avsnitt</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1347"/>
        <source>Looking for video streams for all episodes.</source>
        <translation>Letar efter videoströmmar för alla avsnitt.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1393"/>
        <source>Save the location for direct download.</source>
        <translation>Spara platsen för direkt nedladdning.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1404"/>
        <source>Direct download to the default location.</source>
        <translation>Direktnedladdning till standardplatsen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1424"/>
        <source>Select font...</source>
        <translation>Välj teckensnitt...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1433"/>
        <source>Visit svtplay-dl forum for issues...</source>
        <translation>Besök svtplay-dl forum...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1445"/>
        <source>Maintenance Tool...</source>
        <translation>Underhållsverktyg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1495"/>
        <source>Uses (if available) svtplay-dl in the system path.</source>
        <translation>Använder (om den finns) svtplay-dl i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1504"/>
        <source>Select svtplay-dl...</source>
        <translation>Välj svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1448"/>
        <source>Starts the Maintenance Tool. To update or uninstall.</source>
        <translation>Startar Underhållsverktyget. För att uppdatera eller avinstallera.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1378"/>
        <source>Select Copy Location...</source>
        <translation>Välj kopieringsplats...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1459"/>
        <source>All saved searches, download list and settings are deleted.</source>
        <translation>Alla sparade sökningar, nedladdningslistan och alla inställningar tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1352"/>
        <location filename="../newprg.ui" line="1456"/>
        <source>Delete all settings and Exit</source>
        <translation>Ta bort alla inställningsfiler och avsluta</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1355"/>
        <source>All saved searches and the list of streams to be downloaded will be deleted.</source>
        <translation>Alla sparade sökningar och listan med filer som ska laddas ner tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1401"/>
        <source>Download to Default Location</source>
        <translation>Ladda ner till standardplatsen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="120"/>
        <location filename="../newprg.ui" line="1050"/>
        <source>Search for video streams.</source>
        <oldsource>Search for video files.</oldsource>
        <translation>Sök efter videoströmmar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="190"/>
        <source>Add current video to the list of files that will be downloaded.</source>
        <translation>Lägg till denna videoström till listan med videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="337"/>
        <source>The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..</source>
        <translation>Antalet bitar som transporteras eller behandlas per tidsenhet. Högre siffror ger bättre kvalitet och större fil ..</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="708"/>
        <source>Searches for the subtitle and downloads it at the same time as the video stream.</source>
        <translation>Söker efter undertexten och laddar ner den samtidigt som videoströmmen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="923"/>
        <source>&amp;Download List</source>
        <translation>&amp;Nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="933"/>
        <source>L&amp;ogin</source>
        <translation>L&amp;ogga in</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="941"/>
        <source>&amp;All Episodes</source>
        <translation>&amp;Alla avsnitt</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="954"/>
        <source>&amp;View</source>
        <translation>&amp;Visa</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1062"/>
        <source>Paste the link to the page where the video is displayed.</source>
        <translation>Klistra in länken till webbsidan där videon visas.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1077"/>
        <source>Download the stream you just searched for.</source>
        <oldsource>Download the file you just searched for.</oldsource>
        <translation>Ladda ner videoströmmen du just sökt efter.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1136"/>
        <source>Look at the list of all the streams to download.</source>
        <oldsource>Look at the list of all the files to download.</oldsource>
        <translation>Titta på listan över alla videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1183"/>
        <source>Save the name of a video stream provider, your username and, if you want, your password.</source>
        <translation>Spara namnet på en leverantör av videoströmmar, ditt användarnamn, och om du vill, ditt lösenord.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1308"/>
        <source>Uninstall streamCapture</source>
        <translation>Avinstallera streamCapture</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1311"/>
        <source>Uninstall and remove all components</source>
        <translation>Avinstallera och ta bort alla komponenter</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1327"/>
        <source>Download after Date...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1336"/>
        <source>Stop all downloads</source>
        <translation>Stoppa alla nedladdningar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1339"/>
        <source>Trying to stop svtplay-dl.</source>
        <translation>Försöker stoppa svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="676"/>
        <location filename="../newprg.ui" line="1195"/>
        <location filename="../newprg.ui" line="1303"/>
        <source>If no saved password is found, click here.</source>
        <translation>Om inget lösenord är sparat, klicka här.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1210"/>
        <source>Searching for and downloading subtitles.</source>
        <translation>Söker efter och laddar ner undertexter.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1233"/>
        <source>Add current video to the list of streams that will be downloaded.</source>
        <oldsource>Add current video to the list of files that will be downloaded.</oldsource>
        <translation>Lägg till aktuell video till listan på videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1248"/>
        <source>Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</source>
        <oldsource>Download all the files in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</oldsource>
        <translation>Ladda ner alla videoströmmar i listan. Om det är samma videoström i olika kvaliteter kommer foldrar för varje videoström att skapas automatiskt.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1271"/>
        <source>Edit Download List (Advanced)</source>
        <translation>Redigera nedladdningslistan (avancerat)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="996"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1005"/>
        <source>Check for updates...</source>
        <translation>Sök efter uppdateringar...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1029"/>
        <source>About svtplay-dl...</source>
        <translation>Om svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1038"/>
        <source>About FFmpeg...</source>
        <translation>Om FFmpeg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1095"/>
        <source>License svtplay-dl...</source>
        <translation>Licens svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1104"/>
        <source>License FFmpeg...</source>
        <translation>Licens FFmpeg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1121"/>
        <source>Help...</source>
        <translation>Hjälp...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1133"/>
        <source>View Download List</source>
        <translation>Titta på nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1145"/>
        <source>Delete Download List</source>
        <translation>Radera nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1171"/>
        <source>Version history...</source>
        <translation>Versionshistorik...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1218"/>
        <source>Explain what is going on</source>
        <translation>Förklar vad som händer</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="193"/>
        <location filename="../newprg.ui" line="1230"/>
        <source>Add to Download List</source>
        <translation>Lägg till på nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1259"/>
        <source>Create folder &quot;method_quality_amount&quot;</source>
        <translation>Skapa folder &quot;metod_kvalitet_belopp&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1262"/>
        <source>Automatically creates a folder for each downloaded video stream. If you use &quot;Direct download of all...&quot; no folders are ever created.</source>
        <translation>Skapar automatiskt en mapp för varje nedladdad videoström. Om du använder &quot;Direktnedladdning av alla...&quot; skapas inga mappar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1274"/>
        <source>Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.</source>
        <translation>Ändra metod eller kvalitet. Ta bort en fil från nedladdning. OBS! Om du gör felaktiga ändringar kommer det inte att fungera.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1282"/>
        <source>Show more</source>
        <translation>Visa mer</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1109"/>
        <source>Recent files</source>
        <translation>Senaste filerna</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1153"/>
        <source>Delete download list</source>
        <translation>Ta bort nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1148"/>
        <source>All saved streams in the download list are deleted.</source>
        <oldsource>All saved items in the download list are deleted.</oldsource>
        <translation>Alla sparade videoströmmar i nedladdningslistan tas bort.</translation>
    </message>
</context>
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../checkupdate.cpp" line="35"/>
        <location filename="../checkupdate.cpp" line="51"/>
        <location filename="../checkupdate.cpp" line="75"/>
        <location filename="../checkupdate.cpp" line="93"/>
        <location filename="../checkupdate.cpp" line="102"/>
        <location filename="../checkupdate.cpp" line="111"/>
        <location filename="../checkupdate.cpp" line="143"/>
        <location filename="../checkupdate.cpp" line="178"/>
        <location filename="../checkupdate.cpp" line="251"/>
        <location filename="../checkupdate.cpp" line="260"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="53"/>
        <location filename="../checkupdate.cpp" line="74"/>
        <location filename="../checkupdate.cpp" line="145"/>
        <location filename="../checkupdate.cpp" line="259"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittades.
Kontrollera dina Internetinställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="91"/>
        <source>Your version of </source>
        <translation>Din version av </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="92"/>
        <source> is newer than the latest official version. </source>
        <translation> är nyare än den senaste officiella versionen. </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="101"/>
        <source>You have the latest version of </source>
        <translation>Du har senaste versonen av </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="110"/>
        <source>There was an error when the version was checked.</source>
        <translation>Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="177"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="222"/>
        <source>Updates:</source>
        <translation>Uppdateringar:</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="245"/>
        <source>There is a new version of </source>
        <translation>Det finns en ny version av </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="247"/>
        <source>Latest version: </source>
        <translation>Senaste versionen: </translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="178"/>
        <source>No error.</source>
        <translation>Inga fel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="182"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Fjärrservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="186"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Fjärrvärdnamnet hittades inte (ogiltigt värdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="190"/>
        <source>The connection to the remote server timed out.</source>
        <translation>Anslutningen till fjärrservern tog för lång tid.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="194"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>Operationen avbröts via anrop till abort() eller close() innan den avslutades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="198"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>SSL/TLS-handskakningen misslyckades och den krypterade kanalen kunde inte upprättas. Signalen sslErrors() borde ha sänts ut.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="202"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket, men systemet har initierat roaming till en annan åtkomstpunkt. Begäran bör skickas in på nytt och kommer att behandlas så snart anslutningen återupprättats.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="206"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket eller misslyckande med att starta nätverket.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="210"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>Bakgrundsbegäran är för närvarande inte tillåten på grund av plattformspolicy.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="214"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>Omdirigeringarna nådde maxgränsen. Gränsen är som standard inställd på 50 eller enligt QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="218"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Vid omdirigeringar upptäckte API:et för nätverksåtkomst en omdirigering från ett krypterat protokoll (https) till ett okrypterat (http).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="222"/>
        <source>An unknown network-related error was detected.</source>
        <translation>Ett okänt nätverksrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="226"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>Anslutningen till proxyservern nekades (proxyservern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="230"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Proxyservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="234"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Proxyvärdnamnet hittades inte (ogiltigt proxyvärdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="238"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>Anslutningen till proxyn tog för läng tid eller så svarade proxyn inte i tid på den skickade begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="242"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Proxyn kräver autentisering för att uppfylla begäran men accepterade inte erbjudna användarnamn och lösenord (eller så har inga användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="246"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation>Ett okänt proxyrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="250"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>Åtkomsten till fjärrinnehållet nekades (liknande HTTP-fel 403).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="254"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>Åtgärden som begärs på fjärrinnehållet är inte tillåten.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="258"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Fjärrinnehållet hittades inte på servern (liknande HTTP-fel 404).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="262"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Fjärrservern kräver autentisering för att visa innehållet men angivna användarnamn och lösenord accepterades inte (eller så har inget användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="266"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>Begäran behövde skickas igen, men detta misslyckades till exempel eftersom uppladdningsdata inte kunde läsas en andra gång.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="270"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Begäran kunde inte slutföras på grund av en konflikt med resursens nuvarande tillstånd.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="274"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation>Den begärda resursen är inte längre tillgänglig på servern.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="278"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>Ett okänt fel relaterat till fjärrinnehållet upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="282"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>Network Access API kan inte uppfylla begäran eftersom protokollet inte är känt.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="286"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>Den begärda åtgärden är ogiltig för detta protokoll.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="290"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>Ett haveri i protokollet upptäcktes (analysfel, ogiltiga eller oväntade svar, etc.).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="294"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Servern stötte på ett oväntat tillstånd som hindrade den från att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="298"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Servern stöder inte den funktionalitet som krävs för att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="302"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation>Servern kan inte hantera begäran just nu.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="306"/>
        <source>Unknown Server Error.</source>
        <translation>Okänt serverfel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="310"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Fjärrservern vägrade anslutningen (servern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="314"/>
        <source>Unknown Error.</source>
        <translation>Okänt fel.</translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="../update.cpp" line="34"/>
        <location filename="../update.cpp" line="67"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="35"/>
        <source>zsync cannot be found in path:
</source>
        <translation>zsync kan inte hittas i sökvägen:
</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="36"/>
        <source>Unable to update.</source>
        <translation>Kan inte uppdatera.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="68"/>
        <source>An unexpected error occurred. Error message:</source>
        <translation>Ett oväntat fel uppstod. Felmeddelande:</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="78"/>
        <source>is updated.</source>
        <translation>är uppdaterad.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="79"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../updatedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="46"/>
        <source>Updating, please wait...</source>
        <translation>Uppdaterar, vänta...</translation>
    </message>
</context>
<context>
    <name>DownloadUnpack</name>
    <message>
        <location filename="../download.cpp" line="61"/>
        <source>Is not writable. Cancels</source>
        <translation>Är inte skrivbar. Avbryter</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="65"/>
        <source>Starting to download...
</source>
        <translation>Börjar att ladda ner...
</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="91"/>
        <location filename="../download.cpp" line="218"/>
        <location filename="../download.cpp" line="220"/>
        <source>to</source>
        <translation>till</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="91"/>
        <location filename="../download.cpp" line="93"/>
        <source>Check your file permissions and antivirus software.</source>
        <translation>Kontrollera dina filbehörigheter och antivirusprogram.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="91"/>
        <source>Can not save</source>
        <translation>Kan inte spara</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="93"/>
        <source>Can not save to</source>
        <translation>Kan inte spara till</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="95"/>
        <source>is downloaded to</source>
        <translation>är nedladdad till</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="108"/>
        <source>Starting to decompress</source>
        <translation>Börjar dekomprimera</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="125"/>
        <source>is decompressed.</source>
        <translation>är dekomprimerad.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="131"/>
        <source>Removed</source>
        <translation>Tog bort</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="143"/>
        <source>Could not delete</source>
        <translation>Kunde inte ta bort</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="169"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Linux operating systems. It will not work with Windows operating system.</source>
        <translation>Varning! Den version av svtplay-dl som du laddade ner är avsedd för Linux operativsystem. Det kommer inte att fungera med Windows operativsystem.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="177"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Windows operating systems. It will not work with Linux operating system.</source>
        <translation>Varning! Den version av svtplay-dl som du laddade ner är avsedd för Windows operativsystem. Det kommer inte att fungera med Linux operativsystem.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="210"/>
        <source>Download failed, please try again.</source>
        <translation>Nedladdningen misslyckades, försök igen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="218"/>
        <location filename="../download.cpp" line="220"/>
        <source>Changed name from</source>
        <translation>Ändrade namn från</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="236"/>
        <source>Select &quot;Tools&quot;, &quot;Use svtplay-dl beta&quot; to use this version.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Använd svtpay-dl beta&quot; för att använda den här versionen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="238"/>
        <source>Select &quot;Tools&quot;, &quot;Use svtplay-dl stable&quot; to use this version.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Använd svtpay-dl stabila&quot; för att använda den här versionen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="268"/>
        <source>Failed to decompress.</source>
        <translation>Misslyckades med att dekomprimera.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="32"/>
        <source>Download svtplay-dl from bin.ceicer.com</source>
        <translation>Ladda ner svtplay-dl från bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="70"/>
        <source>Search and Exit</source>
        <translation>Sök och Avsluta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="81"/>
        <source>Select svtplay-dl and download to the &quot;stable&quot; folder and to the &quot;beta&quot; folder.</source>
        <translation>Välj svtplay-dl och ladda ner till  mappen &quot;stable&quot; och till mappen &quot;beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="93"/>
        <source>Direct download of stable to the &quot;stable&quot; folder and beta to the &quot;beta&quot; folder.</source>
        <translation>Direkt nedladdning av stable till &quot;stable&quot;-mappen och beta till &quot;beta&quot;-mappen.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="186"/>
        <source>Current location for stable:</source>
        <translation>Nuvarande plats för stabila:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="186"/>
        <source>Current location for beta:</source>
        <translation>Nuvarande plats för beta:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="195"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Ta en skärmdump (5 sekunders fördröjning)</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="207"/>
        <location filename="../downloadunpack.cpp" line="210"/>
        <source>For</source>
        <translation>För</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="242"/>
        <location filename="../downloadunpack.cpp" line="274"/>
        <location filename="../downloadunpack.cpp" line="334"/>
        <location filename="../downloadunpack.cpp" line="357"/>
        <source>will be downloaded to</source>
        <translation>Kommer att laddas ner till</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="242"/>
        <source>The stable version of svtplay-dl</source>
        <translation>Den stabila versionen av svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="242"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use stable svtplay-dl&quot;.</source>
        <translation>För att använda denna svtplay-dl, klicka
&quot;Verktyg&quot;, &quot;Använd stabila svtplay-dl&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="274"/>
        <source>The beta version of svtplay-dl</source>
        <translation>Betaversionen av svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="274"/>
        <location filename="../downloadunpack.cpp" line="357"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation>För att använda denn version, Klicka
&quot;Verktyg&quot;, &quot;Använd svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="285"/>
        <location filename="../downloadunpack.cpp" line="334"/>
        <location filename="../downloadunpack.cpp" line="357"/>
        <source>Selected svtplay-dl</source>
        <translation>Vald svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="285"/>
        <location filename="../downloadunpack.cpp" line="302"/>
        <location filename="../downloadunpack.cpp" line="319"/>
        <source>will be downloaded.</source>
        <translation>kommer att laddas ner.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="302"/>
        <source>stable svtplay-dl</source>
        <translation>stabila svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="319"/>
        <source>svtplay-dl beta</source>
        <translation>svtplay-dl beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="334"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation>För att använda denna version, klicka
&quot;Verktyg&quot;, Använd stabila svtplay-dl&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="386"/>
        <location filename="../downloadunpack.cpp" line="409"/>
        <location filename="../filedialog.cpp" line="35"/>
        <source>Choose</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="387"/>
        <location filename="../downloadunpack.cpp" line="410"/>
        <location filename="../filedialog.cpp" line="36"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="388"/>
        <location filename="../downloadunpack.cpp" line="411"/>
        <location filename="../filedialog.cpp" line="37"/>
        <source>Save svtplay-dl in directory</source>
        <translation>Spara svtplay-dl i mappen</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="478"/>
        <source>files</source>
        <translation>filer</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="480"/>
        <source>file</source>
        <translation>fil</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="37"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="198"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="215"/>
        <source>V&amp;isit</source>
        <translation>&amp;Besök</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="222"/>
        <source>&amp;View</source>
        <translation>&amp;Visa</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="232"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="236"/>
        <source>Choose location for</source>
        <translation>Välj platsen för</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="257"/>
        <source>Search beta</source>
        <translation>Sök beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="260"/>
        <location filename="../downloadunpack.ui" line="420"/>
        <location filename="../downloadunpack.ui" line="448"/>
        <location filename="../downloadunpack.ui" line="502"/>
        <source>&quot;&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="272"/>
        <location filename="../downloadunpack.ui" line="275"/>
        <source>Download...</source>
        <translation>Ladda ner...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="278"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="289"/>
        <source>Try to decompress</source>
        <translation>Försöker att dekomprimera</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="298"/>
        <source>Exit</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="301"/>
        <source>F4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="310"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="319"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="324"/>
        <source>Load external language file...</source>
        <translation>Ladda extern språkfil...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="333"/>
        <source>Italian</source>
        <translation>Italienska</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="338"/>
        <source>Source code...</source>
        <translation>Källkod...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="346"/>
        <source>Binary files...</source>
        <translation>Binära filer...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="358"/>
        <location filename="../downloadunpack.ui" line="361"/>
        <source>Download beta...</source>
        <translation>Ladda ner beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="364"/>
        <source>Download latest</source>
        <translation>Ladda ner senaste</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="373"/>
        <source>Zoom In</source>
        <translation>Zooma in</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="376"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="381"/>
        <source>Zoom Default</source>
        <translation>Zooma som standard</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="384"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="393"/>
        <source>Zoom Out</source>
        <translation>Zooma ut</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="396"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="401"/>
        <source>Show Toolbar</source>
        <translation>Visa verktygsfält</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="413"/>
        <source>Download to directory &quot;beta&quot;</source>
        <translation>Ladda ner till mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="416"/>
        <source>Download to
directory &quot;beta&quot;</source>
        <translation>Ladda ner till
mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="441"/>
        <source>Download to directory &quot;stable&quot;</source>
        <translation>Ladda ner till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="444"/>
        <source>Download to
directory &quot;stable&quot;</source>
        <translation>Ladda ner till
mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="466"/>
        <location filename="../downloadunpack.ui" line="469"/>
        <location filename="../downloadunpack.ui" line="472"/>
        <source>stable...</source>
        <translation>stabila...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="481"/>
        <location filename="../downloadunpack.ui" line="484"/>
        <location filename="../downloadunpack.ui" line="487"/>
        <source>beta...</source>
        <translation>beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="496"/>
        <location filename="../downloadunpack.ui" line="499"/>
        <source>Search stable</source>
        <translation>Sök stabila</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="511"/>
        <source>Download stable...</source>
        <translation>Ladda ner stabila...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="514"/>
        <source>Download
stable...</source>
        <translation>Ladda ner
stabila...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="518"/>
        <source>Download stable</source>
        <translation>Ladda ner stabila</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="527"/>
        <source>Download stable to directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="530"/>
        <source>Download stable to
directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila till
mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="534"/>
        <source>Download stable
to directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila
till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="544"/>
        <source>Download beta to directory &quot;beta&quot;</source>
        <translation>Ladda ner beta till mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="547"/>
        <location filename="../downloadunpack.ui" line="551"/>
        <source>Download beta to
directory &quot;beta&quot;</source>
        <translation>ladda ner beta till
mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="37"/>
        <source>The font size changes to the selected font size</source>
        <translation>Teckenstorleken ändras till vald teckenstorlek</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="40"/>
        <source>Save a screenshot</source>
        <translation>Spara en skärmdump</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="42"/>
        <source>Images (*.png)</source>
        <translation>bildfiler (*.png)</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="178"/>
        <source>No error.</source>
        <translation>Inga fel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="182"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Fjärrservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="186"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Fjärrvärdnamnet hittades inte (ogiltigt värdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="190"/>
        <source>The connection to the remote server timed out.</source>
        <translation>Anslutningen till fjärrservern tog för lång tid.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="194"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>Operationen avbröts via anrop till abort() eller close() innan den avslutades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="198"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>SSL/TLS-handskakningen misslyckades och den krypterade kanalen kunde inte upprättas. Signalen sslErrors() borde ha sänts ut.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="202"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket, men systemet har initierat roaming till en annan åtkomstpunkt. Begäran bör skickas in på nytt och kommer att behandlas så snart anslutningen återupprättats.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="206"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket eller misslyckande med att starta nätverket.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="210"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>Bakgrundsbegäran är för närvarande inte tillåten på grund av plattformspolicy.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="214"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>Omdirigeringarna nådde maxgränsen. Gränsen är som standard inställd på 50 eller enligt QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="218"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Vid omdirigeringar upptäckte API:et för nätverksåtkomst en omdirigering från ett krypterat protokoll (https) till ett okrypterat (http).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="222"/>
        <source>An unknown network-related error was detected.</source>
        <translation>Ett okänt nätverksrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="226"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>Anslutningen till proxyservern nekades (proxyservern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="230"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Proxyservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="234"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Proxyvärdnamnet hittades inte (ogiltigt proxyvärdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="238"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>Anslutningen till proxyn tog för läng tid eller så svarade proxyn inte i tid på den skickade begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="242"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Proxyn kräver autentisering för att uppfylla begäran men accepterade inte erbjudna användarnamn och lösenord (eller så har inga användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="246"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation>Ett okänt proxyrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="250"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>Åtkomsten till fjärrinnehållet nekades (liknande HTTP-fel 403).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="254"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>Åtgärden som begärs på fjärrinnehållet är inte tillåten.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="258"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Fjärrinnehållet hittades inte på servern (liknande HTTP-fel 404).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="262"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Fjärrservern kräver autentisering för att visa innehållet men angivna användarnamn och lösenord accepterades inte (eller så har inget användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="266"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>Begäran behövde skickas igen, men detta misslyckades till exempel eftersom uppladdningsdata inte kunde läsas en andra gång.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="270"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Begäran kunde inte slutföras på grund av en konflikt med resursens nuvarande tillstånd.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="274"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation>Den begärda resursen är inte längre tillgänglig på servern.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="278"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>Ett okänt fel relaterat till fjärrinnehållet upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="282"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>Network Access API kan inte uppfylla begäran eftersom protokollet inte är känt.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="286"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>Den begärda åtgärden är ogiltig för detta protokoll.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="290"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>Ett haveri i protokollet upptäcktes (analysfel, ogiltiga eller oväntade svar, etc.).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="294"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Servern stötte på ett oväntat tillstånd som hindrade den från att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="298"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Servern stöder inte den funktionalitet som krävs för att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="302"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation>Servern kan inte hantera begäran just nu.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="306"/>
        <source>Unknown Server Error.</source>
        <translation>Okänt serverfel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="310"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Fjärrservern vägrade anslutningen (servern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="314"/>
        <source>Unknown Error.</source>
        <translation>Okänt fel.</translation>
    </message>
</context>
<context>
    <name>SelectFont</name>
    <message>
        <location filename="../selectfont.ui" line="26"/>
        <location filename="../selectfont.cpp" line="26"/>
        <source>Select font</source>
        <translation>Välj teckensnitt</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="45"/>
        <source>Choose the type of font:&lt;br&gt;&quot;Monospace fonts&quot;, fonts where all characters are the same width. &lt;br&gt;&quot;Proportional fonts&quot;, fonts where the width varies depending on the character.</source>
        <translation>Välj teckensnitt:&lt;br&gt;&quot;Teckensnitt med fast bredsteg&quot;, teckensnitt där alla tecken har samma bredd.&lt;br&gt;&quot;Proportionellt teckensnitt&quot;, teckensnitt som varierar i bredd beroende på tecken.</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="139"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="160"/>
        <source>Bold</source>
        <translation>Fet</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="178"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="197"/>
        <source>Bold and italic</source>
        <translation>Fet och kursiv</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="282"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="314"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="28"/>
        <source>Select font size.</source>
        <translation>Välj fontstorlek.</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="83"/>
        <source>All fonts</source>
        <translation>Alla teckensnitt</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="83"/>
        <source>Monospaced fonts</source>
        <translation>Teckensnitt med fast breddsteg</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="84"/>
        <source>Proportional fonts</source>
        <translation>Proportionella teckensnitt</translation>
    </message>
</context>
<context>
    <name>DownloadInstall</name>
    <message>
        <location filename="../download_install.ui" line="19"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../download_install.ui" line="53"/>
        <source>Downloading...</source>
        <translation>Laddar ner...</translation>
    </message>
    <message>
        <location filename="../download_install.ui" line="69"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../download_install.ui" line="85"/>
        <source>Install</source>
        <translation>Installera</translation>
    </message>
    <message>
        <location filename="../download_install.cpp" line="9"/>
        <source>Download </source>
        <translation>Ladda ner </translation>
    </message>
    <message>
        <location filename="../download_install.cpp" line="28"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../download_install.cpp" line="44"/>
        <source>The download is complete.&lt;br&gt;The current version will be uninstalled before the new one is installed.</source>
        <translation>Nedladdningen är klar.&lt;br&gt;Den nuvarande versionen kommer att avinstalleras innan den nya installeras.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="28"/>
        <source>No error.</source>
        <translation>Inget fel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="32"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Fjärrservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="36"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Fjärrvärdnamnet hittades inte (ogiltigt värdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="40"/>
        <source>The connection to the remote server timed out.</source>
        <translation>Anslutningen till fjärrservern tog för lång tid.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="44"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>Operationen avbröts via anrop till abort() eller close() innan den avslutades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="48"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>SSL/TLS-handskakningen misslyckades och den krypterade kanalen kunde inte upprättas. Signalen sslErrors() borde ha sänts ut.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="52"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket, men systemet har initierat roaming till en annan åtkomstpunkt. Begäran bör skickas in på nytt och kommer att behandlas så snart anslutningen återupprättats.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="56"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket eller misslyckande med att starta nätverket.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="60"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>Bakgrundsbegäran är för närvarande inte tillåten på grund av plattformspolicy.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="64"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>Omdirigeringarna nådde maxgränsen. Gränsen är som standard inställd på 50 eller enligt QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="68"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Vid omdirigeringar upptäckte API:et för nätverksåtkomst en omdirigering från ett krypterat protokoll (https) till ett okrypterat (http).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="72"/>
        <source>An unknown network-related error was detected.</source>
        <translation>Ett okänt nätverksrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="76"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>Anslutningen till proxyservern nekades (proxyservern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="80"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Proxyservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="84"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Proxyvärdnamnet hittades inte (ogiltigt proxyvärdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="88"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>Anslutningen till proxyn tog för läng tid eller så svarade proxyn inte i tid på den skickade begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="92"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Proxyn kräver autentisering för att uppfylla begäran men accepterade inte erbjudna användarnamn och lösenord (eller så har inga användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="96"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation>Ett okänt proxyrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="100"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>Åtkomsten till fjärrinnehållet nekades (liknande HTTP-fel 403).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="104"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>Åtgärden som begärs på fjärrinnehållet är inte tillåten.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="108"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Fjärrinnehållet hittades inte på servern (liknande HTTP-fel 404).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="112"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Fjärrservern kräver autentisering för att visa innehållet men angivna användarnamn och lösenord accepterades inte (eller så har inget användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="116"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>Begäran behövde skickas igen, men detta misslyckades till exempel eftersom uppladdningsdata inte kunde läsas en andra gång.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="120"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Begäran kunde inte slutföras på grund av en konflikt med resursens nuvarande tillstånd.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="124"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation>Den begärda resursen är inte längre tillgänglig på servern.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="128"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>Ett okänt fel relaterat till fjärrinnehållet upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="132"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>Network Access API kan inte uppfylla begäran eftersom protokollet inte är känt.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="136"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>Den begärda åtgärden är ogiltig för detta protokoll.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="140"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>Ett haveri i protokollet upptäcktes (analysfel, ogiltiga eller oväntade svar, etc.).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="144"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Servern stötte på ett oväntat tillstånd som hindrade den från att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="148"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Servern stöder inte den funktionalitet som krävs för att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="152"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation>Servern kan inte hantera begäran just nu.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="156"/>
        <source>Unknown Server Error.</source>
        <translation>Okänt serverfel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="160"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Fjärrservern vägrade anslutningen (servern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="164"/>
        <source>Unknown Error.</source>
        <translation>Okänt fel.</translation>
    </message>
</context>
</TS>
