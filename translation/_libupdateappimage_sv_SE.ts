<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Update</name>
    <message>
        <location filename="../update.cpp" line="34"/>
        <location filename="../update.cpp" line="67"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="35"/>
        <source>zsync cannot be found in path:
</source>
        <translation>zsync kan inte hittas i sökvägen:
</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="36"/>
        <source>Unable to update.</source>
        <translation>Kan inte uppdatera.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="68"/>
        <source>An unexpected error occurred. Error message:</source>
        <translation>Ett oväntat fel uppstod. Felmeddelande:</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="78"/>
        <source>is updated.</source>
        <translation>är uppdaterad.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="79"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../updatedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="46"/>
        <source>Updating, please wait...</source>
        <translation>Uppdaterar, vänta...</translation>
    </message>
</context>
</TS>
