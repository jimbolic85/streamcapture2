<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="it_IT">
<context>
    <name>Update</name>
    <message>
        <location filename="../update.cpp" line="34"/>
        <location filename="../update.cpp" line="67"/>
        <source>Update</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="35"/>
        <source>zsync cannot be found in path:
</source>
        <translation>zsync non trovato nel percorso:
</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="36"/>
        <source>Unable to update.</source>
        <translation>Impossibile effettuare aggiornamento.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="68"/>
        <source>An unexpected error occurred. Error message:</source>
        <translation>Si è verificato un errore inaspettato. Messaggio errore:</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="78"/>
        <source>is updated.</source>
        <translation>è aggiornato.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="79"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../updatedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Finestra</translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="46"/>
        <source>Updating, please wait...</source>
        <translation>Aggiornamento...</translation>
    </message>
</context>
</TS>
