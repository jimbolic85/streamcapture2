<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>SelectFont</name>
    <message>
        <location filename="../selectfont.ui" line="26"/>
        <location filename="../selectfont.cpp" line="26"/>
        <source>Select font</source>
        <translation>Välj teckensnitt</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="28"/>
        <source>Select font size.</source>
        <translation>Välj fontstorlek.</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="139"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="160"/>
        <source>Bold</source>
        <translation>Fet</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="45"/>
        <source>Choose the type of font:&lt;br&gt;&quot;Monospace fonts&quot;, fonts where all characters are the same width. &lt;br&gt;&quot;Proportional fonts&quot;, fonts where the width varies depending on the character.</source>
        <translation>Välj teckensnitt:&lt;br&gt;&quot;Teckensnitt med fast bredsteg&quot;, teckensnitt där alla tecken har samma bredd.&lt;br&gt;&quot;Proportionellt teckensnitt&quot;, teckensnitt som varierar i bredd beroende på tecken.</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="178"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="197"/>
        <source>Bold and italic</source>
        <translation>Fet och kursiv</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="282"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="314"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="83"/>
        <source>All fonts</source>
        <translation>Alla teckensnitt</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="83"/>
        <source>Monospaced fonts</source>
        <translation>Teckensnitt med fast breddsteg</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="84"/>
        <source>Proportional fonts</source>
        <translation>Proportionella teckensnitt</translation>
    </message>
</context>
</TS>
