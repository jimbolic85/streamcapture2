<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>AuthenticationRequiredException</name>
    <message>
        <location filename="../../libs/installer/downloadfiletask.cpp" line="+330"/>
        <source>%1 at %2</source>
        <translation>%1 a %2</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Proxy requires authentication.</source>
        <translation>Il proxy richiede l&apos;autenticazione.</translation>
    </message>
</context>
<context>
    <name>BinaryContent</name>
    <message>
        <location filename="../../libs/installer/binarycontent.cpp" line="+201"/>
        <source>Cannot seek to %1 to read the operation data.</source>
        <translation>Impossibile cercare in %1 per leggere i dati dell&apos;operazione.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Cannot seek to %1 to read the resource collection block.</source>
        <translation>Impossibile cercare in %1 per leggere il blocco raccolta risorse.</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Cannot open meta resource %1.</source>
        <translation>Impossibile aprire risorsa metadati %1.</translation>
    </message>
</context>
<context>
    <name>BinaryLayout</name>
    <message>
        <location line="-142"/>
        <source>Cannot seek to %1 to read the embedded meta data count.</source>
        <translation>Impossibile cercare in %1 per leggere il conteggio metadati incorporato.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Cannot seek to %1 to read the resource collection segment.</source>
        <translation>Impossibile cercare in %1 per leggere il segmento raccolta risorse.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unexpected mismatch of meta resources. Read %1, expected: %2.</source>
        <translation>Imprevista incongruenza delle risorse metadati. Letto %1, previsto: %2.</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../../libs/kdtools/authenticationdialog.ui"/>
        <source>Http authentication required</source>
        <translation>Richiesta autenticazione http</translation>
    </message>
    <message>
        <location/>
        <source>You need to supply a Username and Password to access this site.</source>
        <translation>È necessario fornire un nome utente e una password per accedere a questo sito.</translation>
    </message>
    <message>
        <location/>
        <source>Username:</source>
        <translation>Nome Utente:</translation>
    </message>
    <message>
        <location/>
        <source>Password:</source>
        <translation>Password:</translation>
    </message>
    <message>
        <location/>
        <source>%1 at %2</source>
        <translation>%1 a %2</translation>
    </message>
</context>
<context>
    <name>DirectoryGuard</name>
    <message>
        <location filename="../../libs/installer/lib7z_facade.cpp" line="+332"/>
        <source>Path &quot;%1&quot; exists but is not a directory.</source>
        <translation>Il percorso &quot;%1&quot; esiste ma non è una cartella.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cannot create directory &quot;%1&quot;.</source>
        <translation>Impossibile creare la cartella &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>ExtractCallbackImpl</name>
    <message>
        <location line="+340"/>
        <location line="+67"/>
        <source>Cannot retrieve path of archive item %1.</source>
        <translation>Impossibile recuperare il percorso dell&apos;elemento dell’archivio %1.</translation>
    </message>
    <message>
        <location line="-39"/>
        <source>Cannot remove already existing symlink %1.</source>
        <translation>Impossibile rimuovere collegamento simbolico già esistente %1.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot open file &quot;%1&quot; for writing: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la scrittura: %2</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Cannot create symlink at &quot;%1&quot;. Another one is already existing.</source>
        <translation>Impossibile creare collegamento simbolico in &quot;%1&quot;. Un altro è già presente.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot read symlink target from file &quot;%1&quot;.</source>
        <translation>Impossibile leggere la destinazione del collegamento simbolico dal file &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cannot create symlink at %1: %2</source>
        <translation>Impossibile creare collegamento simbolico in %1: %2</translation>
    </message>
</context>
<context>
    <name>InstallerBase</name>
    <message>
        <location filename="../installerbase.cpp" line="+65"/>
        <source>Unable to start installer</source>
        <translation>Impossibile avviare l&apos;installazione</translation>
    </message>
</context>
<context>
    <name>InstallerCalculator</name>
    <message>
        <location filename="../../libs/installer/installercalculator.cpp" line="+78"/>
        <source>Components added as automatic dependencies:</source>
        <translation>Componenti aggiunti come dipendenze automatiche:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Components added as dependency for &quot;%1&quot;:</source>
        <translation>Componenti aggiunti come dipendenza per &quot;%1&quot;:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Components that have resolved dependencies:</source>
        <translation>Componenti che hanno dipendenza risolte:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Selected components without dependencies:</source>
        <translation>Componenti selezionati senza dipendenze:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Recursion detected, component &quot;%1&quot; already added with reason: &quot;%2&quot;</source>
        <translation>Rilevata ricorsione, componente &quot;%1&quot; già aggiunto con motivo: &quot;%2&quot;</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Cannot find missing dependency &quot;%1&quot; for &quot;%2&quot;.</source>
        <translation>Impossibile trovare la dipendenza mancante &quot;%1&quot; per &quot;%2&quot;.</translation>
    </message>
</context>
<context>
    <name>Job</name>
    <message>
        <location filename="../../libs/kdtools/job.cpp" line="+183"/>
        <source>Canceled</source>
        <translation>Annullato</translation>
    </message>
</context>
<context>
    <name>KDUpdater::AppendFileOperation</name>
    <message>
        <location filename="../../libs/kdtools/updateoperations.cpp" line="+646"/>
        <source>Cannot backup file &quot;%1&quot;: %2</source>
        <translation>Impossibile eseguire il backup del file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Cannot open file &quot;%1&quot; for writing: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la scrittura: %2</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Cannot find backup file for &quot;%1&quot;.</source>
        <translation>Impossibile trovare il file di backup per &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot restore backup file for &quot;%1&quot;.</source>
        <translation>Impossibile ripristinare il file di backup per &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Cannot restore backup file for &quot;%1&quot;: %2</source>
        <translation>Impossibile ripristinare il file di backup per &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>KDUpdater::CopyOperation</name>
    <message>
        <location line="-568"/>
        <source>Cannot backup file &quot;%1&quot;.</source>
        <translation>Impossibile eseguire il backup del file &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cannot copy a non-existent file: %1</source>
        <translation>Impossibile copiare un file non esistente: %1</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Cannot remove file &quot;%1&quot;: %2</source>
        <translation>Impossibile rimuovere il file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Cannot copy file &quot;%1&quot; to &quot;%2&quot;: %3</source>
        <translation>Impossibile copiare il file da &quot;%1&quot; a &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Cannot delete file &quot;%1&quot;: %2</source>
        <translation>Impossibile eliminare il file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cannot restore backup file into &quot;%1&quot;: %2</source>
        <translation>Impossibile ripristinare il file di backup in &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>KDUpdater::DeleteOperation</name>
    <message>
        <location line="+167"/>
        <source>Cannot create backup of file &quot;%1&quot;: %2</source>
        <translation>Impossibile creare il backup del file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Cannot restore backup file for &quot;%1&quot;: %2</source>
        <translation>Impossibile ripristinare il file di backup per &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>KDUpdater::FileDownloader</name>
    <message>
        <location filename="../../libs/kdtools/filedownloader.cpp" line="+336"/>
        <source>Download finished.</source>
        <translation>Download completato.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cryptographic hashes do not match.</source>
        <translation>Gli hash di crittografia non corrispondono.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Download canceled.</source>
        <translation>Download annullato.</translation>
    </message>
    <message>
        <location line="+242"/>
        <source>%1 of %2</source>
        <translation>%1 di %2</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>%1 downloaded.</source>
        <translation>%1 scaricato.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>(%1/sec)</source>
        <translation>(%1/sec)</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n day(s), </source>
        <translation>
            <numerusform>%n giorno, </numerusform>
            <numerusform>%n giorni, </numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s), </source>
        <translation>
            <numerusform>%n ora, </numerusform>
            <numerusform>%n ore, </numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n minuto</numerusform>
            <numerusform>%n minuti</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+5"/>
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n secondo</numerusform>
            <numerusform>%n secondi</numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source> - %1%2%3%4 remaining.</source>
        <translation> - rimangono %1%2%3%4.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source> - unknown time remaining.</source>
        <translation> - tempo rimanente sconosciuto.</translation>
    </message>
</context>
<context>
    <name>KDUpdater::HttpDownloader</name>
    <message>
        <location line="+676"/>
        <source>Cannot download %1. Writing to file &quot;%2&quot; failed: %3</source>
        <translation>Impossibile scaricare %1. Scrittura nel file &quot;%2&quot; non riuscita: %3</translation>
    </message>
    <message>
        <location line="+190"/>
        <source>Cannot download %1. Cannot create file &quot;%2&quot;: %3</source>
        <translation>Impossibile scaricare %1. Impossibile creare il file &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>%1 at %2</source>
        <translation>%1 a %2</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Authentication request canceled.</source>
        <translation>Richiesta di autenticazione annullata.</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Secure Connection Failed</source>
        <translation>Connessione protetta non riuscita</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error during connection to: %1.</source>
        <translation>Errore durante la connessione a: %1.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This could be a problem with the server&apos;s configuration, or it could be someone trying to impersonate the server.</source>
        <translation>Il problema verificatosi potrebbe essere relativo alla configurazione del server o a qualcuno che sta tentando di impersonare il server.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>If you have connected to this server successfully in the past or trust this server, the error may be temporary and you can try again.</source>
        <translation>Se la connessione al server è stata già stabilita in passato o se si considera il server attendibile, l&apos;errore può essere temporaneo ed è possibile riprovare.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Try again</source>
        <translation>Riprova</translation>
    </message>
</context>
<context>
    <name>KDUpdater::LocalFileDownloader</name>
    <message>
        <location line="-789"/>
        <source>Cannot open file &quot;%1&quot; for reading: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la lettura: %2</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cannot open file &quot;%1&quot; for writing: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la scrittura: %2</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Writing to file &quot;%1&quot; failed: %2</source>
        <translation>Scrittura nel file &quot;%1&quot; non riuscita: %2</translation>
    </message>
</context>
<context>
    <name>KDUpdater::MkdirOperation</name>
    <message>
        <location filename="../../libs/kdtools/updateoperations.cpp" line="+79"/>
        <source>Cannot create directory &quot;%1&quot;: %2</source>
        <translation>Impossibile creare la cartella &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown error.</source>
        <translation>Errore sconosciuto.</translation>
    </message>
    <message>
        <location line="+45"/>
        <location line="+3"/>
        <source>Cannot remove directory &quot;%1&quot;: %2</source>
        <translation>Impossibile rimuovere la cartella &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>KDUpdater::MoveOperation</name>
    <message>
        <location line="-253"/>
        <source>Cannot backup file &quot;%1&quot;.</source>
        <translation>Impossibile eseguire il backup del file &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Cannot remove file &quot;%1&quot;: %2</source>
        <translation>Impossibile rimuovere il file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+14"/>
        <source>Cannot copy file &quot;%1&quot; to &quot;%2&quot;: %3</source>
        <translation>Impossibile copiare il file da &quot;%1&quot; a &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot remove file &quot;%1&quot;.</source>
        <translation>Impossibile rimuovere il file &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot restore backup file for &quot;%1&quot;: %2</source>
        <translation>Impossibile ripristinare il file di backup per &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>KDUpdater::PrependFileOperation</name>
    <message>
        <location line="+413"/>
        <source>Cannot backup file &quot;%1&quot;: %2</source>
        <translation>Impossibile eseguire il backup del file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Cannot open file &quot;%1&quot; for reading: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la lettura: %2</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Cannot open file &quot;%1&quot; for writing: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la scrittura: %2</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Cannot find backup file for &quot;%1&quot;.</source>
        <translation>Impossibile trovare il file di backup per &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot restore backup file for &quot;%1&quot;.</source>
        <translation>Impossibile ripristinare il file di backup per &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Cannot restore backup file for &quot;%1&quot;: %2</source>
        <translation>Impossibile ripristinare il file di backup per &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>KDUpdater::ResourceFileDownloader</name>
    <message>
        <location filename="../../libs/kdtools/filedownloader.cpp" line="+190"/>
        <source>Cannot read resource file &quot;%1&quot;: %2</source>
        <translation>Impossibile leggere il file di risorse &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>KDUpdater::RmdirOperation</name>
    <message>
        <location filename="../../libs/kdtools/updateoperations.cpp" line="-253"/>
        <location line="+10"/>
        <source>Cannot remove directory &quot;%1&quot;: %2</source>
        <translation>Impossibile rimuovere la cartella &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>The directory does not exist.</source>
        <translation>La cartella non esiste.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Cannot recreate directory &quot;%1&quot;: %2</source>
        <translation>Impossibile ricreare la cartella &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>KDUpdater::Task</name>
    <message>
        <location filename="../../libs/kdtools/task.cpp" line="+192"/>
        <source>%1 started</source>
        <translation>%1 avviato</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>%1 cannot be stopped</source>
        <translation>%1 non può essere arrestato</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Cannot stop task %1</source>
        <translation>Impossibile arrestare l&apos;attività %1</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>%1 cannot be paused</source>
        <translation>%1 non può essere sospeso</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Cannot pause task %1</source>
        <translation>Impossibile sospendere l&apos;attività %1</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Cannot resume task %1</source>
        <translation>Impossibile riprendere l&apos;attività %1</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>%1 done</source>
        <translation>%1 completato</translation>
    </message>
</context>
<context>
    <name>KDUpdater::UpdateFinder</name>
    <message>
        <location filename="../../libs/kdtools/updatefinder.cpp" line="+191"/>
        <source>Cannot access the package information of this application.</source>
        <translation>Impossibile accedere alle informazioni sul pacchetto di questa applicazione.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>No package sources set for this application.</source>
        <translation>Nessuna origine pacchetto impostata per questa applicazione.</translation>
    </message>
    <message numerus="yes">
        <location line="+19"/>
        <source>%n update(s) found.</source>
        <translation>
            <numerusform>Trovato %n aggiornamento.</numerusform>
            <numerusform>Trovati %n aggiornamenti.</numerusform>
        </translation>
    </message>
    <message>
        <location line="+76"/>
        <location line="+266"/>
        <source>Downloading Updates.xml from update sources.</source>
        <translation>Download di Updates.xml dalle origini di aggiornamento.</translation>
    </message>
    <message>
        <location line="-258"/>
        <source>Cannot download package source %1 from &quot;%2&quot;.</source>
        <translation>Impossibile scaricare origine pacchetto %1 da &quot;%2&quot;.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Updates.xml file(s) downloaded from update sources.</source>
        <translation>File Updates.xml scaricati dalle origini di aggiornamento.</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Computing applicable updates.</source>
        <translation>Calcolo degli aggiornamenti applicabili.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Application updates computed.</source>
        <translation>Aggiornamenti applicazione calcolati.</translation>
    </message>
</context>
<context>
    <name>KDUpdater::UpdatesInfoData</name>
    <message>
        <location filename="../../libs/kdtools/updatesinfo.cpp" line="+53"/>
        <source>Updates.xml contains invalid content: %1</source>
        <translation>Updates.xml contiene contenuto non valido: %1</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot read &quot;%1&quot;</source>
        <translation>Impossibile leggere &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Parse error in %1 at %2, %3: %4</source>
        <translation>Errore di analisi in %1 a %2, %3: %4</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Root element %1 unexpected, should be &quot;Updates&quot;.</source>
        <translation>Elemento radice %1 imprevisto, dovrebbe essere &quot;Updates&quot;.</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>ApplicationName element is missing.</source>
        <translation>Elemento ApplicationName mancante.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>ApplicationVersion element is missing.</source>
        <translation>Elemento ApplicationVersion mancante.</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>PackageUpdate element without Name</source>
        <translation>Elemento PackageUpdate senza Name</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>PackageUpdate element without Version</source>
        <translation>Elemento PackageUpdate senza Version</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>PackageUpdate element without ReleaseDate</source>
        <translation>Elemento PackageUpdate senza ReleaseDate</translation>
    </message>
</context>
<context>
    <name>Lib7z</name>
    <message>
        <location filename="../../libs/installer/lib7z_facade.cpp" line="-532"/>
        <source>internal code: %1</source>
        <translation>codice interno: %1</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>not enough memory</source>
        <translation>memoria insufficiente</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Error: %1</source>
        <translation>Errore: %1</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Cannot retrieve property %1 for item %2.</source>
        <translation>Impossibile recuperare la proprietà %1 per l&apos;elemento %2.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Property %1 for item %2 not of type VT_FILETIME but %3.</source>
        <translation>La proprietà %1 per l&apos;elemento %2 non è di tipo VT_FILETIME ma %3.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cannot convert UTC file time to system time.</source>
        <translation>Impossibile convertire l&apos;ora del file UTC nell&apos;ora di sistema.</translation>
    </message>
    <message>
        <location line="+170"/>
        <location line="+526"/>
        <location line="+79"/>
        <location line="+57"/>
        <source>Cannot load codecs.</source>
        <translation>Impossibile caricare i codec.</translation>
    </message>
    <message>
        <location line="-643"/>
        <location line="+605"/>
        <source>Cannot open archive &quot;%1&quot;.</source>
        <translation>Impossibile aprire l&apos;archivio &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="-596"/>
        <source>Cannot retrieve number of items in archive.</source>
        <translation>Impossibile recuperare il numero di elementi nell&apos;archivio.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot retrieve path of archive item &quot;%1&quot;.</source>
        <translation>Impossibile recuperare il percorso dell&apos;elemento dell’archivio &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+583"/>
        <location line="+50"/>
        <source>Unknown exception caught (%1).</source>
        <translation>Rilevata eccezione sconosciuta (%1).</translation>
    </message>
    <message>
        <location line="-252"/>
        <source>Cannot create temporary file: %1</source>
        <translation>Impossibile creare file temporaneo: %1</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Unsupported archive type.</source>
        <translation>Tipo di archivio non supportato.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Cannot create archive &quot;%1&quot;</source>
        <translation>Impossibile creare l&apos;archivio &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot create archive &quot;%1&quot;: %2</source>
        <translation>Impossibile creare l&apos;archivio &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Cannot remove old archive &quot;%1&quot;: %2</source>
        <translation>Impossibile rimuovere il vecchio archivio &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot rename temporary archive &quot;%1&quot; to &quot;%2&quot;: %3</source>
        <translation>Impossibile rinominare l&apos;archivio temporaneo &quot;%1&quot; &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unknown exception caught (%1)</source>
        <translation>Rilevata eccezione sconosciuta (%1)</translation>
    </message>
</context>
<context>
    <name>LocalPackageHub</name>
    <message>
        <location filename="../../libs/kdtools/localpackagehub.cpp" line="+95"/>
        <source>%1 contains invalid content: %2</source>
        <translation>%1 contiene contenuto non valido: %2</translation>
    </message>
    <message>
        <location line="+154"/>
        <source>The file %1 does not exist.</source>
        <translation>Il file %1 non esiste.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot open %1.</source>
        <translation>Impossibile aprire %1.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Parse error in %1 at %2, %3: %4</source>
        <translation>Errore di analisi in %1 a %2, %3: %4</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Root element %1 unexpected, should be &apos;Packages&apos;.</source>
        <translation>Elemento radice %1 imprevisto, dovrebbe essere &apos;Packages&apos;.</translation>
    </message>
</context>
<context>
    <name>LockFile</name>
    <message>
        <location filename="../../libs/kdtools/lockfile_unix.cpp" line="+50"/>
        <location filename="../../libs/kdtools/lockfile_win.cpp" line="+52"/>
        <source>Cannot create lock file &quot;%1&quot;: %2</source>
        <translation>Impossibile creare il file di blocco &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+11"/>
        <location filename="../../libs/kdtools/lockfile_win.cpp" line="+8"/>
        <source>Cannot write PID to lock file &quot;%1&quot;: %2</source>
        <translation>Impossibile scrivere PID nel file di blocco &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+9"/>
        <location filename="../../libs/kdtools/lockfile_win.cpp" line="+7"/>
        <source>Cannot obtain the lock for file &quot;%1&quot;: %2</source>
        <translation>Impossibile ottenere il blocco per il file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+15"/>
        <location filename="../../libs/kdtools/lockfile_win.cpp" line="+15"/>
        <source>Cannot release the lock for file &quot;%1&quot;: %2</source>
        <translation>Impossibile rilasciare il blocco per il file &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>QInstaller</name>
    <message>
        <location filename="../../libs/installer/binarycontent.cpp" line="-42"/>
        <source>No marker found, stopped after %1.</source>
        <translation>Nessun marcatore trovato, operazione interrotta dopo %1.</translation>
    </message>
    <message>
        <location filename="../../libs/installer/fileio.cpp" line="+138"/>
        <source>Cannot open file &quot;%1&quot; for reading: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la lettura: %2</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+13"/>
        <source>Cannot open file &quot;%1&quot; for writing: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la scrittura: %2</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Read failed after %1 bytes: %2</source>
        <translation>Lettura non riuscita dopo %1 byte: %2</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Copy failed: %1</source>
        <translation>Copia non riuscita: %1</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Write failed after %1 bytes: %2</source>
        <translation>Scrittura non riuscita dopo %1 byte: %2</translation>
    </message>
    <message>
        <location filename="../../libs/installer/fileutils.cpp" line="+134"/>
        <source>bytes</source>
        <translation>byte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>EB</source>
        <translation>EB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZB</source>
        <translation>ZB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>YB</source>
        <translation>YB</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Cannot remove file &quot;%1&quot;: %2</source>
        <translation>Impossibile rimuovere il file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Cannot remove directory &quot;%1&quot;: %2</source>
        <translation>Impossibile rimuovere la cartella &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+118"/>
        <location line="+31"/>
        <location line="+344"/>
        <source>Cannot create directory &quot;%1&quot;.</source>
        <translation>Impossibile creare la cartella &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="-362"/>
        <source>Cannot copy file from &quot;%1&quot; to &quot;%2&quot;: %3</source>
        <translation>Impossibile copiare il file da &quot;%1&quot; a &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Cannot move file from &quot;%1&quot; to &quot;%2&quot;: %3</source>
        <translation>Impossibile spostare il file da &quot;%1&quot; a &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+12"/>
        <source>Cannot create directory &quot;%1&quot;: %2</source>
        <translation>Impossibile creare la cartella &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cannot open temporary file: %1</source>
        <translation>Impossibile aprire il file temporaneo: %1</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Cannot open temporary file for template %1: %2</source>
        <translation>Impossibile aprire il file temporaneo per il modello %1: %2</translation>
    </message>
    <message>
        <location line="+271"/>
        <source>Cannot copy file &quot;%1&quot; to &quot;%2&quot;: %3</source>
        <translation>Impossibile copiare il file &quot;%1&quot; in &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Cannot copy file &quot;%1&quot; to &quot;%2&quot;.</source>
        <translation>Impossibile copiare il file &quot;%1&quot; in &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../../libs/installer/utils.cpp" line="+395"/>
        <source>The specified module could not be found.</source>
        <translation>Impossibile trovare il modulo specificato.</translation>
    </message>
    <message>
        <location filename="../../libs/ifwtools/repositorygen.cpp" line="+569"/>
        <location line="+365"/>
        <source>Invalid content in &quot;%1&quot;.</source>
        <translation>Contenuto non valido in &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>QInstaller::Component</name>
    <message>
        <location filename="../../libs/installer/component.cpp" line="+472"/>
        <source>Components cannot have children in updater mode.</source>
        <translation>I componenti non possono avere figli in modalità updater.</translation>
    </message>
    <message>
        <location line="+181"/>
        <source>Cannot open the requested UI file &quot;%1&quot;: %2</source>
        <translation>Impossibile aprire il file di interfaccia utente richiesto &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Cannot load the requested UI file &quot;%1&quot;: %2</source>
        <translation>Impossibile caricare il file di interfaccia utente richiesto &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Cannot open the requested license file &quot;%1&quot;: %2</source>
        <translation>Impossibile aprire il file di licenza richiesto &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+403"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Error: Operation %1 does not exist.</source>
        <translation>Errore: l&apos;operazione %1 non esiste.</translation>
    </message>
    <message>
        <location line="+300"/>
        <source>Cannot resolve isDefault in %1</source>
        <translation>Impossibile risolvere isDefault in %1</translation>
    </message>
    <message>
        <location line="+225"/>
        <source>There was an error loading the selected component. This component can not be installed.</source>
        <translation>Si è verificato un errore durante il caricamento del componente selezionato. 
Questo componente non può essere installato.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update Info: </source>
        <translation>Informazioni aggiornamento: </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>There was an error loading the selected component. This component can not be updated.</source>
        <translation>Si è verificato un errore durante il caricamento del componente selezionato. 
Questo componente non può essere aggiornato.</translation>
    </message>
</context>
<context>
    <name>QInstaller::ComponentModel</name>
    <message>
        <location filename="../../libs/installer/componentmodel.cpp" line="+208"/>
        <source>Component is marked for installation.</source>
        <translation>Il componente è contrassegnato per l&apos;installazione.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Component is marked for uninstallation.</source>
        <translation>Il componente è contrassegnato per la disinstallazione.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Component is installed.</source>
        <translation>Il componente è installato.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Component is not installed.</source>
        <translation>Il componente non è installato.</translation>
    </message>
    <message>
        <location filename="../../libs/installer/packagemanagercore.cpp" line="+3962"/>
        <source>Component Name</source>
        <translation>Nome componente</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Action</source>
        <translation>Azione</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Installed Version</source>
        <translation>Versione installata</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New Version</source>
        <translation>Nuova versione</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Release Date</source>
        <translation>Data di pubblicazione</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Size</source>
        <translation>Dimensioni</translation>
    </message>
</context>
<context>
    <name>QInstaller::ComponentSelectionPage</name>
    <message>
        <location filename="../../libs/installer/componentselectionpage_p.cpp" line="+102"/>
        <source>Alt+A</source>
        <comment>select default components</comment>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Def&amp;ault</source>
        <translation>&amp;Predefiniti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select default components in the tree view.</source>
        <translation>Seleziona nella struttura i componenti predefiniti.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Alt+R</source>
        <comment>reset to already installed components</comment>
        <translation>Alt+F</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Reset</source>
        <translation>&amp;Ripriristina</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset all components to their original selection state in the tree view.</source>
        <translation>Ripristina nella struttura tutti i componenti al loro stato di selezione originale.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Alt+S</source>
        <comment>select all components</comment>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Select All</source>
        <translation>&amp;Seleziona tutto</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select all components in the tree view.</source>
        <translation>Seleziona tutti i componenti nella struttura.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Alt+D</source>
        <comment>deselect all components</comment>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Deselect All</source>
        <translation>&amp;Deseleziona tutto</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Deselect all components in the tree view.</source>
        <translation>Deseleziona tutti i componenti nella struttura.</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>&amp;Browse QBSP files</source>
        <translation>&amp;Sfoglia file QBSP</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Select a Qt Board Support Package file to install additional content that is not directly available from the online repositories.</source>
        <translation>Per installare contenuti aggiuntivi che non sono direttamente disponibili dai repository online seleziona un file Qt Board Support Package.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Filter the enabled repository categories to selection.</source>
        <translation>Filtra le categorie repository abilitate per la selezione.</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>This component will occupy approximately %1 on your hard disk drive.</source>
        <translation>Questo componente occuperà circa %1 sul disco rigido.</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Open File</source>
        <translation>Apri file</translation>
    </message>
    <message>
        <location filename="../../libs/installer/packagemanagergui.cpp" line="+2085"/>
        <source>Select Components</source>
        <translation>Selezione componenti</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Please select the components you want to update.</source>
        <translation>Seleziona i componenti che vuoi aggiornare.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Please select the components you want to install.</source>
        <translation>Seleziona i componenti che vuoi installare.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Please select the components you want to uninstall.</source>
        <translation>Seleziona i componenti che vuoi disinstallare.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select the components to install. Deselect installed components to uninstall them. Any components already installed will not be updated.</source>
        <translation>Seleziona i componenti da installare. 
Deseleziona i componenti installati per disinstallarli. 
I componenti già installati non verranno aggiornati.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mandatory components need to be updated first before you can select other components to update.</source>
        <translation>Prima di poter selezionare altri componenti da aggiornare i componenti obbligatori devono essere aggiornati .</translation>
    </message>
</context>
<context>
    <name>QInstaller::ConsumeOutputOperation</name>
    <message>
        <location filename="../../libs/installer/consumeoutputoperation.cpp" line="+65"/>
        <source>&lt;to be saved installer key name&gt; &lt;executable&gt; [argument1] [argument2] [...]</source>
        <translation>&lt;to be saved installer key name&gt; &lt;executable&gt; [argument1] [argument2] [...]</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Needed installer object in %1 operation is empty.</source>
        <translation>L&apos;oggetto programma di installazione necessario nell&apos;operazione %1 è vuoto.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot save the output of &quot;%1&quot; to an empty installer key value.</source>
        <translation>Impossibile salvare l&apos;output di &quot;%1” in un valore chiave del programma di installazione vuoto.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>File &quot;%1&quot; does not exist or is not an executable binary.</source>
        <translation>Il file &quot;%1&quot; non esiste o non è un file binario eseguibile.</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Running &quot;%1&quot; resulted in a crash.</source>
        <translation>L&apos;esecuzione di &quot;%1&quot; ha causato un arresto anomalo.</translation>
    </message>
</context>
<context>
    <name>QInstaller::CopyDirectoryOperation</name>
    <message>
        <location filename="../../libs/installer/copydirectoryoperation.cpp" line="+67"/>
        <source>&lt;source&gt; &lt;target&gt; [&quot;forceOverwrite&quot;]</source>
        <translation>&lt;source&gt; &lt;target&gt; [&quot;forceOverwrite&quot;]</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Invalid argument in %1: Third argument needs to be forceOverwrite, if specified.</source>
        <translation>Argomento non valido in %1: il terzo argomento deve essere forceOverwrite, se specificato.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Invalid argument in %1: Directory &quot;%2&quot; is invalid.</source>
        <translation>Argomento non valido in %1: la cartella &quot;%2&quot; non è valida.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Cannot create directory &quot;%1&quot;.</source>
        <translation>Impossibile creare la cartella &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Failed to overwrite &quot;%1&quot;.</source>
        <translation>Impossibile sovrascrivere &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot copy file &quot;%1&quot; to &quot;%2&quot;: %3</source>
        <translation>Impossibile copiare il file da &quot;%1&quot; a &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Cannot remove file &quot;%1&quot;.</source>
        <translation>Impossibile rimuovere il file &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>QInstaller::CopyFileTask</name>
    <message>
        <location filename="../../libs/installer/copyfiletask.cpp" line="+64"/>
        <source>Invalid task item count.</source>
        <translation>Conteggio elementi attività non valido.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Cannot open file &quot;%1&quot; for reading: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la lettura: %2</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cannot open file &quot;%1&quot; for writing: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la scrittura: %2</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Writing to file &quot;%1&quot; failed: %2</source>
        <translation>Scrittura nel file &quot;%1&quot; non riuscita: %2</translation>
    </message>
</context>
<context>
    <name>QInstaller::CreateDesktopEntryOperation</name>
    <message>
        <location filename="../../libs/installer/createdesktopentryoperation.cpp" line="+128"/>
        <source>Cannot backup file &quot;%1&quot;: %2</source>
        <translation>Impossibile eseguire il backup del file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Failed to overwrite file &quot;%1&quot;.</source>
        <translation>Impossibile sovrascrivere il file &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot write desktop entry to &quot;%1&quot;.</source>
        <translation>Impossibile scrivere la voce desktop su &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>QInstaller::CreateLinkOperation</name>
    <message>
        <location filename="../../libs/installer/createlinkoperation.cpp" line="+65"/>
        <source>Cannot create link from &quot;%1&quot; to &quot;%2&quot;.</source>
        <translation>Impossibile creare collegamento da &quot;%1&quot; a &quot;%2&quot;.</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Cannot remove link from &quot;%1&quot; to &quot;%2&quot;.</source>
        <translation>Impossibile rimuovere collegamento da &quot;%1&quot; a &quot;%2&quot;.</translation>
    </message>
</context>
<context>
    <name>QInstaller::CreateLocalRepositoryOperation</name>
    <message>
        <location filename="../../libs/installer/createlocalrepositoryoperation.cpp" line="+92"/>
        <source>Cannot set permissions for file &quot;%1&quot;.</source>
        <translation>Impossibile impostare le autorizzazioni file &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Cannot remove file &quot;%1&quot;: %2</source>
        <translation>Impossibile rimuovere il file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Cannot move file &quot;%1&quot; to &quot;%2&quot;: %3</source>
        <translation>Impossibile spostare il file da &quot;%1&quot; a &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Installer at &quot;%1&quot; needs to be an offline one.</source>
        <translation>Il programma di installazione su &quot;%1” deve essere in versione offline.</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Cannot create path &quot;%1&quot;.</source>
        <translation>Impossibile creare il percorso &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Cannot remove directory &quot;%1&quot;.</source>
        <translation>Impossibile rimuovere la cartella &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Cannot open file &quot;%1&quot; for reading.</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la lettura.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot read file &quot;%1&quot;: %2</source>
        <translation>Impossibile leggere il file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Cannot open file &quot;%1&quot; for reading: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la lettura: %2</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Cannot create target directory: &quot;%1&quot;.</source>
        <translation>Impossibile creare la cartella destinazione: &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Unknown exception caught: %1.</source>
        <translation>Rilevata eccezione sconosciuta: %1.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Removing file &quot;%1&quot;.</source>
        <translation>Rimozione file &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot remove file &quot;%1&quot;.</source>
        <translation>Impossibile rimuovere il file &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+19"/>
        <location line="+4"/>
        <source>Cannot remove directory &quot;%1&quot;: %2</source>
        <translation>Impossibile rimuovere la cartella &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>QInstaller::CreateShortcutOperation</name>
    <message>
        <location filename="../../libs/installer/createshortcutoperation.cpp" line="+230"/>
        <source>&lt;target&gt; &lt;link location&gt; [target arguments] [&quot;workingDirectory=...&quot;] [&quot;iconPath=...&quot;] [&quot;iconId=...&quot;] [&quot;description=...&quot;]</source>
        <translation>&lt;target&gt; &lt;link location&gt; [target arguments] [&quot;workingDirectory=...&quot;] [&quot;iconPath=...&quot;] [&quot;iconId=...&quot;] [&quot;description=...&quot;]</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+4"/>
        <source>Cannot create directory &quot;%1&quot;: %2</source>
        <translation>Impossibile creare la cartella &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Failed to overwrite &quot;%1&quot;: %2</source>
        <translation>Impossibile sovrascrivere &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Cannot create link &quot;%1&quot;: %2</source>
        <translation>Impossibile creare il collegamento &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>QInstaller::DownloadArchivesJob</name>
    <message>
        <location filename="../../libs/installer/downloadarchivesjob.cpp" line="+104"/>
        <location line="+46"/>
        <source>Canceled</source>
        <translation>Annullato</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Downloading hash signature failed.</source>
        <translation>Download firma hash non riuscito.</translation>
    </message>
    <message>
        <location line="+74"/>
        <location line="+38"/>
        <source>Download Error</source>
        <translation>Errore download</translation>
    </message>
    <message>
        <location line="-38"/>
        <source>Hash verification while downloading failed. This is a temporary error, please retry.</source>
        <translation>Verifica hash durante il download non riuscita. Errore temporaneo, riprovare.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot verify Hash</source>
        <translation>Impossibile verificare hash</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Cannot download archive %1: %2</source>
        <translation>Impossibile scaricare l&apos;archivio %1: %2</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cannot fetch archives: %1
Error while loading %2</source>
        <translation>Impossibile recuperare gli archivi: %1
Errore durante il caricamento di %2</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Downloading archive &quot;%1&quot; for component %2.</source>
        <translation>Download archivio &quot;%1&quot; per il componente %2.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scheme %1 not supported (URL: %2).</source>
        <translation>Schema %1 non supportato (URL: %2).</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot find component for %1.</source>
        <translation>Impossibile trovare il componente per %1.</translation>
    </message>
</context>
<context>
    <name>QInstaller::Downloader</name>
    <message>
        <location filename="../../libs/installer/downloadfiletask.cpp" line="-196"/>
        <source>Target file &quot;%1&quot; already exists but is not a file.</source>
        <translation>Il file destinazione &quot;%1&quot; esiste già ma non è un file.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot open file &quot;%1&quot; for writing: %2</source>
        <extracomment>%2 is a sentence describing the error</extracomment>
        <translation>Impossibile aprire il file &quot;%1&quot; per la scrittura: %2</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>File &quot;%1&quot; not open for writing: %2</source>
        <extracomment>%2 is a sentence describing the error.</extracomment>
        <translation>File &quot;%1&quot; non aperto per la scrittura: %2</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Writing to file &quot;%1&quot; failed: %2</source>
        <extracomment>%2 is a sentence describing the error.</extracomment>
        <translation>Scrittura nel file &quot;%1&quot; non riuscita: %2</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Redirect loop detected for &quot;%1&quot;.</source>
        <translation>Rilevato ciclo di reindirizzamento per &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Network error while downloading &apos;%1&apos;: %2.</source>
        <translation>Errore di rete durante il download di &apos;%1&apos;: %2.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unknown network error while downloading &quot;%1&quot;.</source>
        <extracomment>%1 is a sentence describing the error</extracomment>
        <translation>Errore di rete sconosciuto durante il download di &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Network transfers canceled.</source>
        <translation>Trasferimenti rete annullati.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Pause and resume not supported by network transfers.</source>
        <translation>I trasferimenti rete non supportano le operazioni di pausa e ripresa.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Invalid source URL &quot;%1&quot;: %2</source>
        <extracomment>%2 is a sentence describing the error</extracomment>
        <translation>URL sorgente non valida &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>QInstaller::ElevatedExecuteOperation</name>
    <message>
        <location filename="../../libs/installer/elevatedexecuteoperation.cpp" line="+159"/>
        <source>Cannot start detached: &quot;%1&quot;</source>
        <translation>Impossibile avvio scollegato: &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Cannot start: &quot;%1&quot;: %2</source>
        <translation>Impossibile avviare: &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Program crashed: &quot;%1&quot;</source>
        <translation>Arresto del programma: &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Execution failed (Unexpected exit code: %1): &quot;%2&quot;</source>
        <translation>Esecuzione non riuscita (codice di uscita imprevisto: %1): &quot;%2&quot;</translation>
    </message>
</context>
<context>
    <name>QInstaller::ExtractArchiveOperation::Runnable</name>
    <message>
        <location filename="../../libs/installer/extractarchiveoperation_p.h" line="+183"/>
        <source>Cannot open archive &quot;%1&quot; for reading: %2</source>
        <translation>Impossibile aprire l’archivio &quot;%1&quot; per la lettura: %2</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Error while extracting archive &quot;%1&quot;: %2</source>
        <translation>Errore durante l&apos;estrazione dell&apos;archivio &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown exception caught while extracting &quot;%1&quot;.</source>
        <translation>Rilevata eccezione sconosciuta durante l&apos;estrazione di &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>QInstaller::FakeStopProcessForUpdateOperation</name>
    <message>
        <location filename="../../libs/installer/fakestopprocessforupdateoperation.cpp" line="+66"/>
        <source>Cannot get package manager core.</source>
        <translation>Impossibile ottenere core di gestione pacchetti.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>This process should be stopped before continuing: %1</source>
        <translation>Questo processo deve essere arrestato prima di continuare: %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>These processes should be stopped before continuing: %1</source>
        <translation>Questi processi devono essere arrestati prima di continuare: %1</translation>
    </message>
</context>
<context>
    <name>QInstaller::FileTaskObserver</name>
    <message>
        <location filename="../../libs/installer/observer.cpp" line="+77"/>
        <source>%1 of %2</source>
        <translation>%1 di %2</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>%1 received.</source>
        <translation>%1 ricevuto.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>(%1/sec)</source>
        <translation>(%1/sec)</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n day(s), </source>
        <translation>
            <numerusform>%n giorno, </numerusform>
            <numerusform>%n giorni, </numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s), </source>
        <translation>
            <numerusform>%n ora, </numerusform>
            <numerusform>%n ore, </numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n minuto</numerusform>
            <numerusform>%n minuti</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+5"/>
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n secondo</numerusform>
            <numerusform>%n secondi</numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source> - %1%2%3%4 remaining.</source>
        <translation> - %1%2%3%4 rimanente.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source> - unknown time remaining.</source>
        <translation> - tempo rimanente sconosciuto.</translation>
    </message>
</context>
<context>
    <name>QInstaller::FinishedPage</name>
    <message>
        <location filename="../../libs/installer/packagemanagergui.cpp" line="+758"/>
        <source>Completing the %1 Wizard</source>
        <translation>Completamento procedura guidata %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Finished</source>
        <translation>Completato</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Click %1 to exit the %2 Wizard.</source>
        <translation>Faiclic su %1 per uscire dalla procedura guidata %2.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Restart</source>
        <translation>Riavvia</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Run %1 now.</source>
        <translation>Esegui %1 ora.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The %1 Wizard failed.</source>
        <translation>Procedura guidata %1 non riuscita.</translation>
    </message>
</context>
<context>
    <name>QInstaller::GlobalSettingsOperation</name>
    <message>
        <location filename="../../libs/installer/globalsettingsoperation.cpp" line="+60"/>
        <source>Settings are not writable.</source>
        <translation>Le impostazioni non sono scrivibili.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Failed to write settings.</source>
        <translation>Impossibile scrivere le impostazioni.</translation>
    </message>
</context>
<context>
    <name>QInstaller::InstallIconsOperation</name>
    <message>
        <location filename="../../libs/installer/installiconsoperation.cpp" line="+107"/>
        <source>&lt;source path&gt; [vendor prefix]</source>
        <translation>&lt;source path&gt; [vendor prefix]</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid Argument: source directory must not be empty.</source>
        <translation>Argomento non valido: la cartella sorgente non deve essere vuota.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Cannot backup file &quot;%1&quot;: %2</source>
        <translation>Impossibile eseguire il backup del file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Failed to overwrite &quot;%1&quot;: %2</source>
        <translation>Impossibile sovrascrivere &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Failed to copy file &quot;%1&quot;: %2</source>
        <translation>Impossibile copiare il file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Cannot create directory &quot;%1&quot;: %2</source>
        <translation>Impossibile creare la cartella &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>QInstaller::IntroductionPage</name>
    <message>
        <location filename="../../libs/installer/packagemanagergui.cpp" line="-1487"/>
        <source>Setup - %1</source>
        <translation>Installazione</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Welcome to the %1 Setup Wizard.</source>
        <translation>Installazione guidata di %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Add or remove components</source>
        <translation>&amp;Aggiungi o rimuovi componenti</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Update components</source>
        <translation>A&amp;ggiorna componenti</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Remove all components</source>
        <translation>&amp;Rimuovi tutti i componenti</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Retrieving information from remote installation sources...</source>
        <translation>Recupero informazioni dalle origini di installazione remote in corso...</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>At least one valid and enabled repository required for this action to succeed.</source>
        <translation>Richiesto almeno un repository valido e abilitato per il completamento di questa azione.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>No updates available.</source>
        <translation>Nessun aggiornamento disponibile.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source> Only local package management available.</source>
        <translation> Disponibile solo gestione pacchetto locale.</translation>
    </message>
    <message>
        <location line="+224"/>
        <source>&amp;Quit</source>
        <translation>&amp;Esci</translation>
    </message>
</context>
<context>
    <name>QInstaller::LicenseAgreementPage</name>
    <message>
        <location line="+91"/>
        <source>License Agreement</source>
        <translation>Contratto licenza</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Alt+A</source>
        <comment>agree license</comment>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Please read the following license agreement. You must accept the terms contained in this agreement before continuing with the installation.</source>
        <translation>Leggi il seguente contratto di licenza. 
Prima di continuare l&apos;installazione è necessario accettare i termini contenuti in questo contratto.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>I accept the license.</source>
        <translation>Accetto la licenza.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please read the following license agreements. You must accept the terms contained in these agreements before continuing with the installation.</source>
        <translation>Leggi i seguenti contratti di licenza. 
Prima di continuare l&apos;installazione è necessario accettare i termini contenuti in questi contratti.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>I accept the licenses.</source>
        <translation>Accetto le licenze.</translation>
    </message>
</context>
<context>
    <name>QInstaller::LicenseOperation</name>
    <message>
        <location filename="../../libs/installer/licenseoperation.cpp" line="+62"/>
        <source>No license files found to copy.</source>
        <translation>Nessun file di licenza da copiare trovato.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Needed installer object in %1 operation is empty.</source>
        <translation>L&apos;oggetto programma di installazione necessario nell&apos;operazione %1 è vuoto.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Can not write license file &quot;%1&quot;.</source>
        <translation>Impossibile scrivere il file di licenza &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>No license files found to delete.</source>
        <translation>Nessun file di licenza da eliminare trovato.</translation>
    </message>
</context>
<context>
    <name>QInstaller::LineReplaceOperation</name>
    <message>
        <location filename="../../libs/installer/linereplaceoperation.cpp" line="+69"/>
        <source>Invalid argument in %1: Empty search argument is not supported.</source>
        <translation>Argomento non valido in %1: l&apos;argomento di ricerca vuoto non è supportato.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Cannot open file &quot;%1&quot; for reading: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la lettura: %2</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Cannot open file &quot;%1&quot; for writing: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la scrittura: %2</translation>
    </message>
</context>
<context>
    <name>QInstaller::MetadataJob</name>
    <message>
        <location filename="../../libs/installer/metadatajob.cpp" line="+155"/>
        <source>Missing package manager core engine.</source>
        <translation>Motore core di gestione pacchetti mancante.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Preparing meta information download...</source>
        <translation>Preparazione del download delle informazioni sui metadati...</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Unpacking compressed repositories. This may take a while...</source>
        <translation>Decompressione repository compressi. Potrebbe volerci qualche momento...</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Meta data download canceled.</source>
        <translation>Download dei metadati annullato.</translation>
    </message>
    <message>
        <location line="+87"/>
        <location line="+110"/>
        <source>Unknown exception during extracting.</source>
        <translation>Eccezione sconosciuta durante l&apos;estrazione.</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>Missing proxy credentials.</source>
        <translation>Credenziali proxy mancanti.</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Authentication failed.</source>
        <translation>Autenticazione non riuscita.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+97"/>
        <source>Unknown exception during download.</source>
        <translation>Eccezione sconosciuta durante il download.</translation>
    </message>
    <message>
        <location line="-80"/>
        <source>Failure to fetch repositories.</source>
        <translation>Impossibile recuperare repository.</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>Extracting meta information...</source>
        <translation>Estrazione informazioni sui metadati in corso...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Checksum mismatch detected for &quot;%1&quot;.</source>
        <translation>Rilevata mancata corrispondenza checksum per &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Retrieving meta information from remote repository... %1/%2 </source>
        <translation>Recupero delle informazioni sui metadati dal repository remoto in corso... %1/%2 </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Retrieving meta information from remote repository... </source>
        <translation>Recupero delle informazioni sui metadati dal repository remoto in corso... </translation>
    </message>
    <message>
        <location filename="../../libs/installer/metadatajob_p.h" line="+84"/>
        <source>Error while extracting archive &quot;%1&quot;: %2</source>
        <translation>Errore durante l&apos;estrazione dell&apos;archivio &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown exception caught while extracting archive &quot;%1&quot;.</source>
        <translation>Rilevata eccezione sconosciuta durante l&apos;estrazione dell&apos;archivio &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cannot open file &quot;%1&quot; for reading: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la lettura: %2</translation>
    </message>
</context>
<context>
    <name>QInstaller::PackageManagerCore</name>
    <message>
        <location filename="../../libs/installer/packagemanagercore.cpp" line="-3485"/>
        <source>Error writing Maintenance Tool</source>
        <translation>Errore durante la scrittura dello strumento di manutenzione</translation>
    </message>
    <message>
        <location line="+294"/>
        <source>
Downloading packages...</source>
        <translation>
Download pacchetti in corso...</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Installation canceled by user.</source>
        <translation>Installazione annullata dall&apos;utente.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>All downloads finished.</source>
        <translation>Completati tutti i download.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Cancelling the Installer</source>
        <translation>Annullamento del programma di installazione</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Authentication Error</source>
        <translation>Errore di autenticazione</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Some components could not be removed completely because administrative rights could not be acquired: %1.</source>
        <translation>Alcuni componenti non possono essere rimossi completamente poiché non è possibile acquisire diritti di amministrazione: %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown error.</source>
        <translation>Errore sconosciuto.</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Some components could not be removed completely because an unknown error happened.</source>
        <translation>Alcuni componenti non possono essere rimossi completamente a causa di un errore sconosciuto.</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>User input is required but the output device is not associated with a terminal.</source>
        <translation>È richiesto l&apos;input dell&apos;utente ma il dispositivo di output non è associato a un terminale.</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>The directory you selected already exists and contains an installation. Choose a different target for installation.</source>
        <translation>La cartella selezionata esiste già e contiene un&apos;installazione. 
Scegli una destinazione diversa per l&apos;installazione.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Warning</source>
        <translation>Avviso</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>You have selected an existing, non-empty directory for installation.
Note that it will be completely wiped on uninstallation of this application.
It is not advisable to install into this directory as installation might fail.
Do you want to continue?</source>
        <translation>È stata selezionata una cartella esistente non vuota per l&apos;installazione.
Nota che verrà eliminata completamente in caso di disinstallazione di questa applicazione.
Non è consigliabile eseguire l&apos;installazione in questa cartella poiché l&apos;installazione potrebbe avere esito negativo.
Vuoi continuare?</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You have selected an existing file or symlink, please choose a different target for installation.</source>
        <translation>È stato selezionato un file o un collegamento simbolico esistente, scegli una destinazione diversa per l&apos;installazione.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>The installation path cannot be empty, please specify a valid directory.</source>
        <translation>Il percorso di installazione non può essere vuoto, specifica una cartella valida.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The installation path cannot be relative, please specify an absolute path.</source>
        <translation>Il percorso di installazione non può essere relativo, specifica un percorso assoluto.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The path or installation directory contains non ASCII characters. This is currently not supported! Please choose a different path or installation directory.</source>
        <translation>Il percorso o la cartella di installazione contengono caratteri non ASCII. 
Questo non è al momento supportato. 
Scegli un percorso diverso o una cartella di installazione diversa.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>As the install directory is completely deleted, installing in %1 is forbidden.</source>
        <translation>Dal momento che la cartella di installazione viene eliminata completamente, l&apos;installazione in %1 non è consentita.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The path you have entered is too long, please make sure to specify a valid path.</source>
        <translation>Il percorso immesso è troppo lungo, assicurati di specificare un percorso valido.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>The path you have entered is not valid, please make sure to specify a valid target.</source>
        <translation>Il percorso immesso non è valido, assicurati di specificare una destinazione valida.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>The path you have entered is not valid, please make sure to specify a valid drive.</source>
        <translation>Il percorso immesso non è valido, assicurati di specificare un&apos;unità valida.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The installation path must not end with &apos;.&apos;, please specify a valid directory.</source>
        <translation>Il percorso di installazione non deve terminare con &apos;.&apos;, specifica una cartella valida.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>The installation path must not contain &quot;%1&quot;, please specify a valid directory.</source>
        <translation>Il percorso di installazione non deve contenere &quot;%1&quot;, specifica una cartella valida.</translation>
    </message>
    <message>
        <location line="+225"/>
        <source>Application not running in Package Manager mode.</source>
        <translation>L&apos;applicazione non è in esecuzione in modalità Gestione pacchetti.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>No installed packages found.</source>
        <translation>Nessun pacchetto installato trovato.</translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+2179"/>
        <source>Cannot register component! Component with identifier %1 already exists.</source>
        <translation>Impossibile registrare il componente! 
Il componente con identificatore %1 esiste già.</translation>
    </message>
    <message>
        <location line="-2056"/>
        <source>Application running in Uninstaller mode.</source>
        <translation>L&apos;applicazione è in esecuzione in modalità Programma di disinstallazione.</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>There is an important update available, please run the updater first.</source>
        <translation>È disponibile un aggiornamento importante, eseguire innanzitutto l&apos;updater.</translation>
    </message>
    <message>
        <location line="+458"/>
        <source>Cannot resolve all dependencies.</source>
        <translation>Impossibile risolvere tutte le dipendenze.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Components about to be removed.</source>
        <translation>Componenti che si sta per rimuovere.</translation>
    </message>
    <message>
        <location line="+231"/>
        <source>Cannot install component %1. Component is installed only as automatic dependency to %2.
</source>
        <translation>Impossibile installare il componente %1. 
Il componente è installato solo come dipendenza automatica da %2.
</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot install component %1. Component is not checkable meaning you have to select one of the subcomponents.
</source>
        <translation>Impossibile installare il componente %1. 
Il componente non è controllabile, il che significa che devi selezionare uno dei sottocomponenti.
</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Component %1 already installed
</source>
        <translation>Componente %1 già installato
</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot install %1. Component is virtual.
</source>
        <translation>Impossibile installare %1. Il componente è virtuale.
</translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+23"/>
        <source>Cannot install %1. Component not found.
</source>
        <translation>Impossibile installare %1. Componente non trovato.
</translation>
    </message>
    <message>
        <location line="+40"/>
        <location line="+117"/>
        <location line="+41"/>
        <location line="+51"/>
        <source>Running processes found.</source>
        <translation>Rilevati processi in esecuzione.</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>Cannot elevate access rights while running from command line. Please restart the application as administrator.</source>
        <translation>Impossibile elevare i diritti di accesso durante l&apos;esecuzione dalla riga di comando. 
Riavvia l&apos;applicazione come amministratore.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error while elevating access rights.</source>
        <translation>Errore durante l&apos;elevazione dei diritti di accesso.</translation>
    </message>
    <message>
        <location line="-1590"/>
        <location line="+13"/>
        <location line="+2856"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location line="-1196"/>
        <source>Not enough disk space to store temporary files and the installation. %1 are available, while %2 are at least required.</source>
        <translation>Spazio su disco insufficiente per memorizzare i file temporanei e l&apos;installazione. 
Spazio disponibile %1, spazio minimo richiesto %2.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Not enough disk space to store all selected components! %1 are available while %2 are at least required.</source>
        <translation>Spazio su disco insufficiente per memorizzare tutti i componenti selezionati! 
Spazio disponibile %1, spazio minimo richiesto %2.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Not enough disk space to store temporary files! %1 are available while %2 are at least required.</source>
        <translation>Spazio su disco insufficiente per memorizzare i file temporanei! 
Spazio disponibile %1, spazio minimo richiesto %2.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The volume you selected for installation seems to have sufficient space for installation, but there will be less than 1% of the volume&apos;s space available afterwards.</source>
        <translation>Il volume selezionato per l&apos;installazione sembra avere spazio sufficiente per l&apos;installazione, ma in seguito sarà disponibile meno dell&apos;1% dello spazio del volume.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The volume you selected for installation seems to have sufficient space for installation, but there will be less than 100 MB available afterwards.</source>
        <translation>Il volume selezionato per l&apos;installazione sembra avere spazio sufficiente per l&apos;installazione, ma in seguito saranno disponibili meno di 100 MB.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The estimated installer size %1 would exceed the supported executable size limit of %2. The application may not be able to run.</source>
        <translation>La dimensione stimata del programma di installazione %1 supererebbe il limite di dimensione dell&apos;eseguibile supportato di %2. 
L&apos;applicazione potrebbe non essere in grado di funzionare.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Installation will use %1 of disk space.</source>
        <translation>L&apos;installazione utilizzerà %1 di spazio su disco.</translation>
    </message>
    <message>
        <location line="+1190"/>
        <source>invalid</source>
        <translation>non valido</translation>
    </message>
</context>
<context>
    <name>QInstaller::PackageManagerCorePrivate</name>
    <message>
        <location filename="../../libs/installer/packagemanagercore_p.cpp" line="+464"/>
        <source>Unresolved dependencies</source>
        <translation>Dipendenze non risolte</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+1244"/>
        <location line="+27"/>
        <location line="+179"/>
        <location line="+71"/>
        <location line="+121"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location line="-1256"/>
        <source>Access error</source>
        <translation>Errore di accesso</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Format error</source>
        <translation>Errore di formato</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot write installer configuration to %1: %2</source>
        <translation>Impossibile scrivere la configurazione del programma di installazione in %1: %2</translation>
    </message>
    <message>
        <location line="+124"/>
        <source>Stop Processes</source>
        <translation>Arresta processi</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>These processes should be stopped to continue:

%1</source>
        <translation>Questi processi devono essere arrestati per continuare:

%1</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+1166"/>
        <location line="+245"/>
        <source>Installation canceled by user</source>
        <translation>Installazione annullata dall&apos;utente</translation>
    </message>
    <message>
        <location line="-1404"/>
        <source>Retry count exceeded</source>
        <translation>Numero ritentativi eccessivo</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>Writing maintenance tool.</source>
        <translation>Scrittura dello strumento di manutenzione.</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+421"/>
        <source>Failed to seek in file %1: %2</source>
        <translation>Impossibile cercare nel file %1: %2</translation>
    </message>
    <message>
        <location line="-413"/>
        <source>Maintenance tool is not a bundle</source>
        <translation>Lo strumento di manutenzione non è un bundle</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+15"/>
        <location line="+305"/>
        <source>Cannot remove data file &quot;%1&quot;: %2</source>
        <translation>Impossibile rimuovere il file di dati &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="-314"/>
        <source>Cannot write maintenance tool data to %1: %2</source>
        <translation>Impossibile scrivere i dati dello strumento di manutenzione in %1: %2</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Cannot write maintenance tool to &quot;%1&quot;: %2</source>
        <translation>Impossibile scrivere lo strumento di manutenzione in &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Cannot remove temporary data file &quot;%1&quot;: %2</source>
        <translation>Impossibile rimuovere il file dati temporaneo &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+293"/>
        <source>Cannot write maintenance tool binary data to %1: %2</source>
        <translation>Impossibile scrivere i dati binari dello strumento di manutenzione in %1: %2</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Writing offline base binary.</source>
        <translation>Scrittura binaria base offline.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cannot remove file &quot;%1&quot;: %2</source>
        <translation>Impossibile rimuovere il file &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot create directory &quot;%1&quot;.</source>
        <translation>Impossibile creare la cartella &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cannot write offline binary to &quot;%1&quot;: %2</source>
        <translation>Impossibile scrivere file binario offline su &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+569"/>
        <source>Cannot remove temporary file &quot;%1&quot;: %2</source>
        <translation>Impossibile rimuovere file temporaneo &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="-532"/>
        <location line="+441"/>
        <source>Variable &apos;TargetDir&apos; not set.</source>
        <translation>Variabile &apos;TargetDir&apos; non impostata.</translation>
    </message>
    <message>
        <location line="-402"/>
        <location line="+256"/>
        <source>Preparing the installation...</source>
        <translation>Preparazione dell&apos;installazione in corso...</translation>
    </message>
    <message>
        <location line="-246"/>
        <source>It is not possible to install from network location</source>
        <translation>Impossibile installare dal percorso di rete</translation>
    </message>
    <message>
        <location line="+40"/>
        <location line="+2"/>
        <source>Creating local repository</source>
        <translation>Creazione repository locale</translation>
    </message>
    <message>
        <location line="+43"/>
        <location line="+178"/>
        <source>Creating Maintenance Tool</source>
        <translation>Creazione strumento di manutenzione</translation>
    </message>
    <message>
        <location line="-167"/>
        <source>
Installation finished!</source>
        <translation>
Installazione completata!</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>
Installation aborted!</source>
        <translation>
Installazione annullata!</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>It is not possible to run that operation from a network location</source>
        <translation>Impossibile eseguire l&apos;operazione da una posizione di rete</translation>
    </message>
    <message>
        <location line="+104"/>
        <source>Removing deselected components...</source>
        <translation>Rimozione dei componenti deselezionati in corso...</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>
Update finished!</source>
        <translation>
Aggiornamento completato!</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>
Update aborted!</source>
        <translation>
Aggiornamento annullato!</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Uninstallation completed successfully.</source>
        <translation>Disinstallazione completata.</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Uninstallation aborted.</source>
        <translation>Disinstallazione interrotta.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Cannot create target directory for installer.</source>
        <translation>Impossibile creare la cartella destinazione per il programma di installazione.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Preparing offline generation...</source>
        <translation>Preparazione generazione offline...</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Preparing installer configuration...</source>
        <translation>Preparazione configurazione installazione...</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Creating the installer...</source>
        <translation>Creazione programma installazione...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Failed to create offline installer. %1</source>
        <translation>Impossibile creare il programma di installazione offline. %1</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Cannot remove temporary directory &quot;%1&quot;.</source>
        <translation>Impossibile rimuovere la cartella temporanea &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Offline generation completed successfully.</source>
        <translation>Generazione offline completata correttamente.</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Offline generation aborted!</source>
        <translation>Generazione offline annullata!</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>
Installing component %1</source>
        <translation>
Installazione componente %1</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+240"/>
        <source>Installer Error</source>
        <translation>Errore del programma di installazione</translation>
    </message>
    <message>
        <location line="-239"/>
        <source>Error during installation process (%1):
%2</source>
        <translation>Errore durante il processo di installazione (%1):
%2</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Done</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Cannot prepare uninstall</source>
        <translation>Impossibile preparare la disinstallazione</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Cannot start uninstall</source>
        <translation>Impossibile avviare la disinstallazione</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Error during uninstallation process:
%1</source>
        <translation>Errore durante il processo di disinstallazione:
%1</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Unknown error</source>
        <translation>Errore sconosciuto</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+24"/>
        <source>Cannot retrieve remote tree %1.</source>
        <translation>Impossibile recuperare l&apos;albero remoto %1.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Failure to read packages from %1.</source>
        <translation>Impossibile leggere pacchetti da %1.</translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+34"/>
        <source>Cannot retrieve meta information: %1</source>
        <translation>Impossibile recuperare le meta informazioni: %1</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+12"/>
        <source>Cannot add temporary update source information.</source>
        <translation>Impossibile aggiungere informazioni sull&apos;origine di aggiornamento temporanea.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Cannot find any update source information.</source>
        <translation>Impossibile trovare informazioni sulla sorgente aggiornamento.</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Dependency cycle between components &quot;%1&quot; and &quot;%2&quot; detected.</source>
        <translation>Rilevato ciclo di dipendenza tra i componenti &quot;%1&quot; e &quot;%2&quot;.</translation>
    </message>
</context>
<context>
    <name>QInstaller::PackageManagerGui</name>
    <message>
        <location filename="../../libs/installer/packagemanagergui.cpp" line="-1761"/>
        <source>%1 Setup</source>
        <translation>Installazione di %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Maintain %1</source>
        <translation>Aggiornamento di %1</translation>
    </message>
    <message>
        <location line="+647"/>
        <source>Do you want to cancel the installation process?</source>
        <translation>Vuoi annullare il processo di installazione?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to cancel the uninstallation process?</source>
        <translation>Vuoi annullare il processo di disinstallazione?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to quit the installer application?</source>
        <translation>Vuoi uscire dal programma di installazione?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to quit the uninstaller application?</source>
        <translation>Vuoi uscire dal programma di disinstallazione?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to quit the maintenance application?</source>
        <translation>Vuoi uscire dall&apos;applicazione di manutenzione?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 Question</source>
        <translation>Domanda su %1</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Specify proxy settings and configure repositories for add-on components.</source>
        <translation>Specifica le impostazioni del proxy e configura i repository per i componenti aggiuntivi.</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>It is not possible to install from network location.
Please copy the installer to a local drive</source>
        <translation>Impossibile effettuare la disinstallazione da un percorso di rete.
Copia il programma di installazione in un&apos;unità locale</translation>
    </message>
</context>
<context>
    <name>QInstaller::PerformInstallationForm</name>
    <message>
        <location filename="../../libs/installer/performinstallationform.cpp" line="+127"/>
        <location line="+91"/>
        <location line="+21"/>
        <source>&amp;Show Details</source>
        <translation>Vi&amp;sualizza dettagli</translation>
    </message>
    <message>
        <location line="-21"/>
        <source>&amp;Hide Details</source>
        <translation>&amp;Nascondi dettagli</translation>
    </message>
</context>
<context>
    <name>QInstaller::PerformInstallationPage</name>
    <message>
        <location filename="../../libs/installer/packagemanagergui.cpp" line="+1648"/>
        <source>U&amp;ninstall</source>
        <translation>Disi&amp;nstalla</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uninstalling %1</source>
        <translation>Disinstallazione di %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Update</source>
        <translation>&amp;Aggiorna</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Updating components of %1</source>
        <translation>Aggiornamento componenti di %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Install</source>
        <translation>&amp;Installa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Installing %1</source>
        <translation>Installazione di %1</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Installing</source>
        <translation>Installazione</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Updating</source>
        <translation>Aggiornamento</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Uninstalling</source>
        <translation>Disinstallazione</translation>
    </message>
</context>
<context>
    <name>QInstaller::ProxyCredentialsDialog</name>
    <message>
        <location filename="../../libs/installer/proxycredentialsdialog.ui"/>
        <source>Dialog</source>
        <translation>Finestra di dialogo</translation>
    </message>
    <message>
        <location/>
        <source>The proxy %1 requires a username and password.</source>
        <translation>Il proxy %1 richiede un nome utente e una password.</translation>
    </message>
    <message>
        <location/>
        <source>Username:</source>
        <translation>Nome Utente:</translation>
    </message>
    <message>
        <location/>
        <source>Username</source>
        <translation>Nome utente</translation>
    </message>
    <message>
        <location/>
        <source>Password:</source>
        <translation>Password:</translation>
    </message>
    <message>
        <location/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../../libs/installer/proxycredentialsdialog.cpp" line="+51"/>
        <source>Proxy Credentials</source>
        <translation>Credenziali proxy</translation>
    </message>
</context>
<context>
    <name>QInstaller::ReadyForInstallationPage</name>
    <message>
        <location filename="../../libs/installer/packagemanagergui.cpp" line="-208"/>
        <source>U&amp;ninstall</source>
        <translation>Disi&amp;nstalla</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+56"/>
        <source>Ready to Uninstall</source>
        <translation>Avvia disinstallazione</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Setup is now ready to begin removing %1 from your computer.&lt;br&gt;&lt;font color=&quot;red&quot;&gt;The program directory %2 will be deleted completely&lt;/font&gt;, including all content in that directory!</source>
        <translation>Ora è possibile iniziare la rimozione di %1 dal computer.&lt;br&gt;&lt;font color=&quot;red&quot;&gt;La cartella del programma %2 verrà eliminata completamente&lt;/font&gt;, incluso tutto il contenuto di tale cartella!</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>U&amp;pdate</source>
        <translation>A&amp;ggiorna</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ready to Update Packages</source>
        <translation>Aggiornamento pacchetti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Setup is now ready to begin updating your installation.</source>
        <translation>Ora è possibile iniziare l&apos;installazione.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Install</source>
        <translation>&amp;Installa</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+36"/>
        <source>Ready to Install</source>
        <translation>Avvia installazione</translation>
    </message>
    <message>
        <location line="-35"/>
        <source>Setup is now ready to begin installing %1 on your computer.</source>
        <translation>Ora è possibile iniziare l&apos;installazione di %1 nel computer.</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Ready to Update</source>
        <translation>Avvia aggiornamento</translation>
    </message>
</context>
<context>
    <name>QInstaller::RegisterFileTypeOperation</name>
    <message>
        <location filename="../../libs/installer/registerfiletypeoperation.cpp" line="+147"/>
        <location line="+60"/>
        <source>Registering file types is only supported on Windows.</source>
        <translation>La registrazione dei tipi di file è supportata solo in Windows.</translation>
    </message>
    <message>
        <location line="-47"/>
        <source>Register File Type: Invalid arguments</source>
        <translation>Tipo di file di registro: argomenti non validi</translation>
    </message>
</context>
<context>
    <name>QInstaller::RemoteObject</name>
    <message>
        <location filename="../../libs/installer/remoteobject.h" line="+96"/>
        <source>Cannot read all data after sending command: %1. Bytes expected: %2, Bytes received: %3. Error: %4</source>
        <translation>Impossibile leggere tutti i dati dopo l&apos;invio del comando: %1. 
Byte previsti: %2; byte ricevuti: %3. Errore: %4</translation>
    </message>
</context>
<context>
    <name>QInstaller::ReplaceOperation</name>
    <message>
        <location filename="../../libs/installer/replaceoperation.cpp" line="+81"/>
        <source>Current search argument calling &quot;%1&quot; with empty search argument is not supported.</source>
        <translation>L&apos;argomento di ricerca attuale che chiama &quot;%1&quot; con argomento di ricerca vuoto non è supportato.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Current mode argument calling &quot;%1&quot; with arguments &quot;%2&quot; is not supported. Please use string or regex.</source>
        <translation>L&apos;argomento della modalità attuale che chiama &quot;%1&quot; con argomenti &quot;%2&quot; non è supportato. 
Usa string o regex.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cannot open file &quot;%1&quot; for reading: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la lettura: %2</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Cannot open file &quot;%1&quot; for writing: %2</source>
        <translation>Impossibile aprire il file &quot;%1&quot; per la scrittura: %2</translation>
    </message>
</context>
<context>
    <name>QInstaller::Resource</name>
    <message>
        <location filename="../../libs/installer/binaryformat.cpp" line="+167"/>
        <source>Cannot open resource %1 for reading.</source>
        <translation>Impossibile aprire la risorsa %1 per la lettura.</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Read failed after %1 bytes: %2</source>
        <translation>Lettura non riuscita dopo %1 byte: %2</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Write failed after %1 bytes: %2</source>
        <translation>Scrittura non riuscita dopo %1 byte: %2</translation>
    </message>
</context>
<context>
    <name>QInstaller::RestartPage</name>
    <message>
        <location filename="../../libs/installer/packagemanagergui.cpp" line="+414"/>
        <source>Completing the %1 Setup Wizard</source>
        <translation>Completamento dell&apos;installazione guidata di %1</translation>
    </message>
</context>
<context>
    <name>QInstaller::ScriptEngine</name>
    <message>
        <location filename="../../libs/installer/scriptengine.cpp" line="+507"/>
        <source>Cannot open script file at %1: %2</source>
        <translation>Impossibile aprire il file di script in %1: %2</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Exception while loading the component script &quot;%1&quot;: %2</source>
        <translation>Eccezione durante il caricamento dello script del componente &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown error.</source>
        <translation>Errore sconosciuto.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>on line number: </source>
        <translation>Numero online: </translation>
    </message>
</context>
<context>
    <name>QInstaller::SelfRestartOperation</name>
    <message>
        <location filename="../../libs/installer/selfrestartoperation.cpp" line="+58"/>
        <source>Installer object needed in operation %1 is empty.</source>
        <translation>L&apos;oggetto programma di installazione necessario nell&apos;operazione %1 è vuoto.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Self Restart: Only valid within updater or packagemanager mode.</source>
        <translation>Riavvio automatico: valido solo in modalità aggiornamento o gestione pacchetti.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Self Restart: Invalid arguments</source>
        <translation>Riavvio automatico: argomenti non validi</translation>
    </message>
</context>
<context>
    <name>QInstaller::ServerAuthenticationDialog</name>
    <message>
        <location filename="../../libs/installer/serverauthenticationdialog.ui"/>
        <source>Server Requires Authentication</source>
        <translation>Il server richiede l&apos;autenticazione</translation>
    </message>
    <message>
        <location/>
        <source>You need to supply a username and password to access this site.</source>
        <translation>Per accedere a questo sito è necessario fornire un nome utente e una password .</translation>
    </message>
    <message>
        <location/>
        <source>Username:</source>
        <translation>Nome utente:</translation>
    </message>
    <message>
        <location/>
        <source>Password:</source>
        <translation>Password:</translation>
    </message>
    <message>
        <location/>
        <source>%1 at %2</source>
        <translation>%1 a %2</translation>
    </message>
</context>
<context>
    <name>QInstaller::SettingsOperation</name>
    <message>
        <location filename="../../libs/installer/settingsoperation.cpp" line="+74"/>
        <source>Missing argument(s) &quot;%1&quot; calling %2 with arguments &quot;%3&quot;.</source>
        <translation>Argomento(i) mancante(i) &quot;%1&quot; richiamando %2 con argomenti &quot;%3&quot;.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Current method argument calling &quot;%1&quot; with arguments &quot;%2&quot; is not supported. Please use set, remove, add_array_value or remove_array_value.</source>
        <translation>La chiamata all’argomento metodo attuale &quot;%1&quot; con argomenti &quot;%2&quot; non è supportata. 
Usa set, remove, add_array_value o remove_array_value.</translation>
    </message>
</context>
<context>
    <name>QInstaller::SimpleMoveFileOperation</name>
    <message>
        <location filename="../../libs/installer/simplemovefileoperation.cpp" line="+63"/>
        <source>None of the arguments can be empty: source &quot;%1&quot;, target &quot;%2&quot;.</source>
        <translation>Nessuno degli argomenti può essere vuoto: origine &quot;%1&quot;, destinazione &quot;%2&quot;.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Cannot move file from &quot;%1&quot; to &quot;%2&quot;, because the target path exists and is not removable.</source>
        <translation>Impossibile spostare il file da &quot;%1&quot; a &quot;%2&quot;, poiché la destinazione esiste e non è rimovibile.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Cannot move file &quot;%1&quot; to &quot;%2&quot;: %3</source>
        <translation>Impossibile spostare il file da &quot;%1&quot; a &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+13"/>
        <source>Moving file &quot;%1&quot; to &quot;%2&quot;.</source>
        <translation>Spostamento del file &quot;%1&quot; su &quot;%2&quot;.</translation>
    </message>
</context>
<context>
    <name>QInstaller::StartMenuDirectoryPage</name>
    <message>
        <location filename="../../libs/installer/packagemanagergui.cpp" line="-596"/>
        <source>Start Menu shortcuts</source>
        <translation>Collegamenti menu Start</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select the Start Menu in which you would like to create the program&apos;s shortcuts. You can also enter a name to create a new directory.</source>
        <translation>Seleziona il menu Start in cui vuoi creare i collegamenti del programma. 
È anche possibile inserire un nome per creare una nuova cartella.</translation>
    </message>
</context>
<context>
    <name>QInstaller::TargetDirectoryPage</name>
    <message>
        <location line="-161"/>
        <source>Installation Folder</source>
        <translation>Cartella installazione</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Please specify the directory where %1 will be installed.</source>
        <translation>Specifica la cartella in cui verrà installato %1.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Alt+R</source>
        <comment>browse file system to choose a file</comment>
        <translation>Alt+F</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B&amp;rowse...</source>
        <translation>S&amp;foglia...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse file system to choose the installation directory.</source>
        <translation>Sfoglia il file system per scegliere la cartella di installazione.</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Select Installation Folder</source>
        <translation>Seleziona cartella installazione</translation>
    </message>
</context>
<context>
    <name>QInstaller::TestRepository</name>
    <message>
        <location filename="../../libs/installer/testrepository.cpp" line="+77"/>
        <source>Missing package manager core engine.</source>
        <translation>Motore core di gestione pacchetti mancante.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Empty repository URL.</source>
        <translation>URL repository vuoto.</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Download canceled.</source>
        <translation>Download annullato.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Timeout while testing repository &quot;%1&quot;.</source>
        <translation>Timeout durante il test del repository &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Cannot parse Updates.xml: %1</source>
        <translation>Impossibile analizzare Updates.xml: %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot open Updates.xml for reading: %1</source>
        <translation>Impossibile aprire Updates.xml per la lettura: %1</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Authentication failed.</source>
        <translation>Autenticazione non riuscita.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Unknown error while testing repository &quot;%1&quot;.</source>
        <translation>Errore sconosciuto durante il test del repository &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../libs/installer/adminauthorization_x11.cpp" line="+79"/>
        <location line="+5"/>
        <source>Authorization required</source>
        <translation>Necessaria autorizzazione</translation>
    </message>
    <message>
        <location line="-4"/>
        <location line="+5"/>
        <source>Enter your password to authorize for sudo:</source>
        <translation>Inserisci la password per autorizzare sudo:</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error acquiring admin rights</source>
        <translation>Errore durante l&apos;acquisizione di diritti di amministrazione</translation>
    </message>
    <message>
        <location filename="../sdkapp.h" line="+101"/>
        <source>Another %1 instance is already running. Wait until it finishes, close it, or restart your system.</source>
        <translation>Un&apos;altra istanza di %1 è già in esecuzione. 
Attendi fino al termine, chiudila o riavvia il sistema.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Please make sure that the current user has reading access to file &quot;%1&quot; or try running %2 as an administrator.</source>
        <translation>Assicurati che l&apos;utente attuale abbia accesso in lettura al file &quot;%1&quot; o prova a eseguire %2 come amministratore.</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Cannot start installer binary as updater.</source>
        <translation>Impossibile avviare il file del programma di installazione come aggiornamento.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot start installer binary as package manager.</source>
        <translation>Impossibile avviare il programma di installazione come gestore di pacchetti.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot start installer binary as uninstaller.</source>
        <translation>Impossibile avviare il programma di installazione come disinstallazione.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Empty repository list for option &apos;addRepository&apos;.</source>
        <translation>Elenco repository vuoto per l&apos;opzione &apos;addRepository&apos;.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Empty repository list for option &apos;addTempRepository&apos;.</source>
        <translation>Elenco repository vuoto per l&apos;opzione &apos;addTempRepository&apos;.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Empty repository list for option &apos;setTempRepository&apos;.</source>
        <translation>Elenco repository vuoto per l&apos;opzione &apos;setTempRepository&apos;.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Empty repository list for option &apos;installCompressedRepository&apos;.</source>
        <translation>Elenco repository vuoto per l&apos;opzione &apos;installCompressedRepository&apos;.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The file %1 does not exist.</source>
        <translation>Il file %1 non esiste.</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Arguments missing for option %1</source>
        <translation>Argomenti mancanti per l&apos;opzione %1</translation>
    </message>
    <message>
        <location line="+149"/>
        <source>Invalid button value %1 </source>
        <translation>Valore pulsante non valido %1 </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Incorrect arguments for %1</source>
        <translation>Argomenti errati per %1</translation>
    </message>
</context>
<context>
    <name>RemoteClient</name>
    <message>
        <location filename="../../libs/installer/remoteclient_p.h" line="+133"/>
        <location line="+18"/>
        <source>Cannot get authorization.</source>
        <translation>Impossibile ottenere l’autorizzazione.</translation>
    </message>
    <message>
        <location line="-17"/>
        <source>Cannot get authorization that is needed for continuing the installation.

Please start the setup program as a user with the appropriate rights.
Or accept the elevation of access rights if being asked.</source>
        <translation>Impossibile ottenere l&apos;autorizzazione necessaria per continuare l&apos;installazione.

Avvia il programma di installazione come utente con i diritti appropriati.
O accetta l&apos;elevazione dei diritti di accesso, ove richiesto.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Cannot get authorization that is needed for continuing the installation.
 Either abort the installation or use the fallback solution by running

%1

as a user with the appropriate rights and then clicking OK.</source>
        <translation>Impossibile ottenere l&apos;autorizzazione necessaria per continuare l&apos;installazione.
Annulla l&apos;installazione o usa la soluzione di fallback eseguendo

%1

come utente con i diritti appropriati, quindi fai clic su &apos;OK&apos;.</translation>
    </message>
</context>
<context>
    <name>ResourceCollectionManager</name>
    <message>
        <location filename="../../libs/installer/binaryformat.cpp" line="+154"/>
        <source>Cannot open resource %1: %2</source>
        <translation>Impossibile aprire la risorsa %1: %2</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../../libs/installer/settings.cpp" line="+282"/>
        <source>Cannot open settings file %1 for reading: %2</source>
        <translation>Impossibile aprire il file impostazioni %1 in lettura: %2</translation>
    </message>
    <message>
        <location line="+650"/>
        <source>Select Categories</source>
        <translation>Seleziona categorie</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.ui"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location/>
        <source>Network</source>
        <translation>Rete</translation>
    </message>
    <message>
        <location/>
        <source>No proxy</source>
        <translation>Nessun proxy</translation>
    </message>
    <message>
        <location/>
        <source>System proxy settings</source>
        <translation>Impostazioni proxy di sistema</translation>
    </message>
    <message>
        <location/>
        <source>Manual proxy configuration</source>
        <translation>Configurazione manuale proxy</translation>
    </message>
    <message>
        <location/>
        <source>HTTP proxy:</source>
        <translation>Proxy HTTP:</translation>
    </message>
    <message>
        <location/>
        <source>Port:</source>
        <translation>Porta:</translation>
    </message>
    <message>
        <location/>
        <source>FTP proxy:</source>
        <translation>Proxy FTP:</translation>
    </message>
    <message>
        <location/>
        <source>Repositories</source>
        <translation>Repository</translation>
    </message>
    <message>
        <location/>
        <source>Add Username and Password for authentication if needed.</source>
        <translation>Aggiungi, se necessario, nome utente e password per l&apos;autenticazione.</translation>
    </message>
    <message>
        <location/>
        <source>Use temporary repositories only</source>
        <translation>Usa solo repository temporanei</translation>
    </message>
    <message>
        <location/>
        <source>Add</source>
        <translation>Aggiungi</translation>
    </message>
    <message>
        <location/>
        <source>Remove</source>
        <translation>Rimuovi</translation>
    </message>
    <message>
        <location/>
        <source>Test</source>
        <translation>Test</translation>
    </message>
    <message>
        <location/>
        <location filename="../settingsdialog.cpp" line="+371"/>
        <source>Show Passwords</source>
        <translation>Visualizza password</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="-242"/>
        <source>Check this to use repository during fetch.</source>
        <translation>Seleziona questa opzione per usare il repository durante il recupero.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add the username to authenticate on the server.</source>
        <translation>Per eseguire l&apos;autenticazione sul server aggiungi il nome.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add the password to authenticate on the server.</source>
        <translation>Per eseguire l&apos;autenticazione sul server aggiungi la password.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The servers URL that contains a valid repository.</source>
        <translation>URL del server che contiene un repository valido.</translation>
    </message>
    <message>
        <location line="+211"/>
        <source>An error occurred while testing this repository.</source>
        <translation>Si è verificato un errore durante il test del repository.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The repository was tested successfully.</source>
        <translation>Repository testato correttamente.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Do you want to disable the repository?</source>
        <translation>Vuoi disabilitare il repository?</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do you want to enable the repository?</source>
        <translation>Vuoi abilitare il repository?</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Hide Passwords</source>
        <translation>Nascondi password</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Use</source>
        <translation>Usare</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Username</source>
        <translation>Nome utente</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Repository</source>
        <translation>Repository</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Default repositories</source>
        <translation>Repository predefiniti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Temporary repositories</source>
        <translation>Repository temporanei</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User defined repositories</source>
        <translation>Repository definiti dall&apos;utente</translation>
    </message>
    <message>
        <location filename="../settingsdialog.ui"/>
        <source>Select All</source>
        <translation>Seleziona tutto</translation>
    </message>
    <message>
        <location/>
        <source>Deselect All</source>
        <translation>Deseleziona tutto</translation>
    </message>
</context>
<context>
    <name>UpdateOperation</name>
    <message>
        <location filename="../../libs/installer/environmentvariablesoperation.cpp" line="+108"/>
        <location line="+35"/>
        <source>Cannot write to registry path %1.</source>
        <translation>Impossibile scrivere nel percorso del registro di sistema %1.</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>Registry path %1 is not writable.</source>
        <translation>Il percorso registro di sistema %1 non è scrivibile.</translation>
    </message>
    <message>
        <location filename="../../libs/kdtools/updateoperation.cpp" line="+211"/>
        <source>exactly %1</source>
        <translation>esattamente %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>at least %1</source>
        <translation>almeno %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not more than %1</source>
        <translation>Non più di %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 or %2</source>
        <translation>%1 o %2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 to %2</source>
        <translation>%1 a %2</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>Invalid arguments in %1: %n arguments given, %2 arguments expected.</source>
        <translation>
            <numerusform>Argomento non valido in %1: %n argomento fornito, %2 argomento previsto.</numerusform>
            <numerusform>Argomenti non validi in %1: %n argomenti forniti, %2 argomenti previsti.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>Invalid arguments in %1: %n arguments given, %2 arguments expected in the form: %3.</source>
        <translation>
            <numerusform>Argomento non valido in %1: %n argomenti fornito, %2 argomento previsto nel modulo: %3.</numerusform>
            <numerusform>Argomenti non validi in %1: %n argomenti forniti, %2 argomenti previsti nel modulo: %3.</numerusform>
        </translation>
    </message>
    <message>
        <location line="+197"/>
        <source>Renaming file &quot;%1&quot; to &quot;%2&quot; failed: %3</source>
        <translation>Impossibile rinominare il file &quot;%1&quot; &quot;%2&quot;: %3</translation>
    </message>
</context>
<context>
    <name>QInstaller::ComponentSelectionPagePrivate</name>
    <message>
        <location filename="../../libs/installer/componentselectionpage_p.cpp" line="-189"/>
        <source>Filter</source>
        <translation>Filtro</translation>
    </message>
    <message>
        <location line="+178"/>
        <location line="+25"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
</context>
<context>
    <name>QInstaller::ExtractArchiveOperation</name>
    <message>
        <location filename="../../libs/installer/extractarchiveoperation.cpp" line="+100"/>
        <source>Extracting &quot;%1&quot;</source>
        <translation>Estrazione di &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QInstaller::QFileDialogProxy</name>
    <message>
        <location filename="../../libs/installer/scriptengine.cpp" line="-171"/>
        <source>User input is required but the output device is not associated with a terminal.</source>
        <translation>È richiesto l&apos;input dell&apos;utente ma il dispositivo di output non è associato a un terminale.</translation>
    </message>
</context>
</TS>
