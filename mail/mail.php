<?php
session_start();
$_SESSION["name"] = $_POST["name"];
$_SESSION["email"] = $_POST["email"];
$_SESSION["subject"] = $_POST["subject"];
$_SESSION["body"] = $_POST["body"];
$recipient = "ic_streamcapture2@ceicer.com";
$ready_send = true;
unset($_SESSION["mission_done"]);

if ($_SERVER["REQUEST_METHOD"] !== "POST") {
    exit("Direct access not supported.");
}

if (isset($_POST["required"]) && !empty($_POST["required"])) {
    $missing_required_fields = false;
    $required = explode(",", $_POST["required"]);
    foreach ($required as $req) {
        if (!isset($_POST[$req]) || empty($_POST[$req])) {
            $_SESSION["mission_done"] = "failure";
            $ready_send = false;
            break;
        }
    }
    // check if e-mail address is well-formed

    if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
        $_SESSION["mission_done"] = "failure";
        $ready_send = false;

        $_SESSION["failure_email"] = "Incorrect email address";
    }

    if (!preg_match("/^[a-zA-Z-' ]*$/", $_POST["name"])) {
        $_SESSION["mission_done"] = "failure";
        $ready_send = false;

        $_SESSION["failure_email"] = "Incorrect name";
    }

    if (trim($_POST["body"]) == "") {
        $_SESSION["mission_done"] = "failure";
        $ready_send = false;

        $_SESSION["failure_email"] =
            "You must enter something in the message field";
    }
}

if (isset($_POST["name"]) && !empty($_POST["name"])) {
    $mail_from_name = "streamCapture2: " . $_POST["name"];
} else {
    $mail_from_name = "streamCapture2: unknown";
}

if (isset($_POST["email"]) && !empty($_POST["email"])) {
    $mail_from = $_POST["email"];
} else {
    $mail_from = "";
}

if (!empty($mail_from) and !empty($mail_from_name)) {
    $mail_sender ="=?UTF-8?B?" . base64_encode($mail_from_name) . "?= <" . $mail_from . ">";
} elseif (!empty($mail_from)) {
    $mail_sender = $mail_from;
} else {
    $mail_sender = "=?UTF-8?B?" . base64_encode($mail_from_name) . "?=";
}

if (isset($_POST["subject"]) && !empty($_POST["subject"])) {
    $mail_subject = $_POST["subject"];
} else {
    $mail_subject = "streamCapture2 Form Submission";
}

$now = date("Y-m-d H:i:s");
$message = "Below is the result of your feedback from streamCapture2. It was submitted by $mail_from_name Time: $now.";
$message .=
    "\n---------------------------------------------------------------------------\n\n";

foreach ($_POST as $key => $value) {
    if (
        !in_array($key, [
            "recipient",
            "subject",
            "redirect",
            "required",
            "missing_fields_redirect",
        ])
    ) {
        $message .= $key . ": " . $value . "\n\n";
    }
}

$message .=
    "---------------------------------------------------------------------------";

// $headers = "From: $mail_sender\r\n";
$headers = "From: ic_streamcapture2@ceicer.com\r\n";
$headers .= "Reply-To: $mail_sender\r\n";
// $headers .= "Reply-To: ic_streamcapture2@ceicer.com\r\n";
$headers .= "MIME-Version: 1.0\r\n";
// $headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
$headers .= "Content-type: text/plain; charset=utf-8\r\n";
$headers .= "X-Priority: 3\r\n";
$headers .= "X-MSMail-Priority: Normal\r\n";
$headers .= "X-Mailer: php\r\n";
$headers .= "Content-Transfer-Encoding: base64";
$mail_subject = "=?UTF-8?B?" . base64_encode($mail_subject) . "?=";
// $mail_subject .= $mail_from_name;
if ($ready_send) {
    $mail_sent = mail(
        $recipient,
        $mail_subject,
        base64_encode($message),
        $headers
    );

    $_SESSION["mission_done"] = "success";

    unset($_SESSION["name"]);
    unset($_SESSION["email"]);
    unset($_SESSION["subject"]);
    unset($_SESSION["body"]);
}

if (isset($_POST["redirect"]) && !empty($_POST["redirect"])) {
    header("Location: " . $_POST["redirect"]);
}

?>
