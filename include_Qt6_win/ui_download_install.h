/********************************************************************************
** Form generated from reading UI file 'download_install.ui'
**
** Created by: Qt User Interface Compiler version 6.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DOWNLOAD_INSTALL_H
#define UI_DOWNLOAD_INSTALL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_DownloadInstall
{
public:
    QProgressBar *progressBar;
    QLabel *lblDownloading;
    QPushButton *pbCancel;
    QPushButton *pbInstall;

    void setupUi(QDialog *DownloadInstall)
    {
        if (DownloadInstall->objectName().isEmpty())
            DownloadInstall->setObjectName(QString::fromUtf8("DownloadInstall"));
        DownloadInstall->resize(386, 200);
        DownloadInstall->setMinimumSize(QSize(0, 0));
        progressBar = new QProgressBar(DownloadInstall);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(10, 130, 365, 23));
        progressBar->setMaximum(200);
        progressBar->setValue(100);
        progressBar->setTextVisible(false);
        progressBar->setInvertedAppearance(false);
        lblDownloading = new QLabel(DownloadInstall);
        lblDownloading->setObjectName(QString::fromUtf8("lblDownloading"));
        lblDownloading->setGeometry(QRect(10, 10, 341, 101));
        lblDownloading->setWordWrap(true);
        pbCancel = new QPushButton(DownloadInstall);
        pbCancel->setObjectName(QString::fromUtf8("pbCancel"));
        pbCancel->setGeometry(QRect(10, 160, 93, 29));
        pbInstall = new QPushButton(DownloadInstall);
        pbInstall->setObjectName(QString::fromUtf8("pbInstall"));
        pbInstall->setEnabled(false);
        pbInstall->setGeometry(QRect(110, 160, 93, 29));

        retranslateUi(DownloadInstall);

        QMetaObject::connectSlotsByName(DownloadInstall);
    } // setupUi

    void retranslateUi(QDialog *DownloadInstall)
    {
        DownloadInstall->setWindowTitle(QCoreApplication::translate("DownloadInstall", "Dialog", nullptr));
        lblDownloading->setText(QCoreApplication::translate("DownloadInstall", "Downloading...", nullptr));
        pbCancel->setText(QCoreApplication::translate("DownloadInstall", "Cancel", nullptr));
        pbInstall->setText(QCoreApplication::translate("DownloadInstall", "Install", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DownloadInstall: public Ui_DownloadInstall {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DOWNLOAD_INSTALL_H
